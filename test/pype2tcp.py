#!/usr/bin/env python3
#
# Read stdout from a sub-program and pass the traffic out over
# TCP/IP to a given host and port.
#
# Called as "pype2tcp.py HOST PORT prog args..."
#
# From: Rick van Rein <rick@openfortress.nl>


import sys
from sys import argv, stdin, stdout, stderr, exit

import subprocess

from socket import socket, AF_INET6, AF_INET, SOCK_STREAM

if len (argv) < 4:
	stderr.write ('Usage: %s host port wrapped_command [args...]\n' % argv [0])
	exit (1);

host = argv [1]
port = int (argv [2])
subcmd = argv [3:]

try:
	sox = socket (AF_INET6, SOCK_STREAM, 0)
	sox.connect ( (host,port) )
except:
	sox = socket (AF_INET,  SOCK_STREAM, 0)
	sox.connect ( (host,port) )

# Start wrapped subprocess
print ('Running %r' % subcmd)
wrapped = subprocess.Popen (subcmd,
		stdin=None,
		stdout=subprocess.PIPE,
		stderr=None)
		#IMPLIES:TEXT# universal_newlines=True)

# Pypeline synchronisation
stdout.write ('--\n')
stdout.flush ()

# Copy content from stdout
nextln = None
while nextln != b'':
	nextln = wrapped.stdout.readline ()
	print ('nextln = %r' % nextln)
	sox.send (nextln)

sox.close ()

while wrapped.returncode is None:
	wrapped.wait ()

exit (wrapped.returncode)

