#!/usr/bin/env python3
#
# Read test input and produce readable information.
# Compare with known-good output when given.
#
# Output lines use "stream=data" or, for the default stream, "data".
#
# From: Rick van Rein <rick@openfortress.nl>


from sys import argv, stdin, stdout, stderr, exit

from socket import socket, AF_INET6, AF_INET, SOCK_STREAM


if not 1 <= len (argv) <= 4:
	stderr.write ('Usage: %s [compare] [host port]\n' % argv [0])
	exit (1);

if len (argv) in [2,4]:
	verify = open (argv [1]).read ()
else:
	verify = None

if len (argv) > 2:
	host = argv [-2]
	port = int (argv [-1])
	try:
		sox = socket (AF_INET6, SOCK_STREAM, 0)
		sox.bind ( (host,port) )
	except:
		sox = socket (AF_INET,  SOCK_STREAM, 0)
		sox.bind ( (host,port) )
	sox.listen (5)
else:
	sox = None

# Pypeline synchronisation
stdout.write ('--\n')
stdout.flush ()

if sox is None:
	data = stdin.read ()
else:
	(cnx,_) = sox.accept ()
	data = ''
	nextln = cnx.recv (1024)
	while len (nextln) != 0:
		data += nextln.decode ('ascii')
		nextln = cnx.recv (1024)
	cnx.close ()
	sox.close ()

def parse_indata (data):
	#PEDANTIC# cur = None
	cur = 'stdout'
	first = True
	all = { }
	for sohseq in data.split ('\x01'):
		soseqs = sohseq.split ('\x0e')
		if not first:
			cur = soseqs [0]
			if cur == '':
				cur = 'stdout'
			soseqs = soseqs [1:]
		first = False
		for soseq in soseqs:
			if soseq != '':
				if cur not in all:
					all [cur] = ''
				all [cur] += soseq
			if cur is not None:
				vfyline = '%s=%s' % (cur,soseq)
				#PEDANTIC# cur = None
				cur = 'stdout'
			else:
				vfyline = soseq
			stdout.write (vfyline)
	return all

def parse_verifier (verifier):
	all = { }
	stream = None
	escapable = '\x01\x02\x03\x04\x0e\x0f\x10\x11\x12\x13\x14\x19\x1a\x1c\x1d\x1e\x1f\x00\xff'
	def escape (s):
		return ''.join ([
			( c if not c in escapable else '\x10' + chr (ord (c) ^ 0x40) )
			for c in s
		])
	for arg in verifier.splitlines (keepends=True):
		print (arg, end='')
		if '=' in arg:
			(stream,value) = arg.split ('=', 1)
			escval = escape (value)
		else:
			escval = escape (arg)
		if escval != '':
			if stream not in all:
				all [stream] = ''
			all [stream] += escval
	return all

stdout.write ('>> INPUT:\n')
indata = parse_indata (data)

if verify is not None:
	stdout.write ('\n>> EXPECTED:\n')
	vfydata = parse_verifier (verify)
	if indata != vfydata:
		stderr.write ('Input does not match expectation:\ninput = %r\nexpectation = %r\n' % (indata,vfydata))
		exit (1)
	else:
		exit (0)

