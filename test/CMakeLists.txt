include_directories (${CMAKE_SOURCE_DIR}/include)

#
# test programs
#

#TODO# add_executable (plaintest plaintest.c)



#
# test settings
#

set (pypeline ${CMAKE_CURRENT_SOURCE_DIR}/pypeline)
set (pype2tcp ${CMAKE_CURRENT_SOURCE_DIR}/pype2tcp.py)
set (pype2file ${CMAKE_CURRENT_SOURCE_DIR}/pype2file.py)
set (ctrl2asc ${CMAKE_BINARY_DIR}/src/ctrl2ascii)
set (parsedump ${CMAKE_BINARY_DIR}/lib/parsedump)
#TODO# set (genstreams ${CMAKE_CURRENT_SOURCE_DIR}/genstreams.py)
#TODO# set (vfystreams ${CMAKE_CURRENT_SOURCE_DIR}/vfystreams.py)
#TODO# set (pype2tcp ${CMAKE_CURRENT_SOURCE_DIR}/pype2tcp.py)
#TODO# set (plaintest ${CMAKE_CURRENT_BINARY_DIR}/plaintest)
set (dotty ${CMAKE_BINARY_DIR}/src/dotty)

set (TESTBIN ${CMAKE_CURRENT_SOURCE_DIR}/bin)

#
# Run the parser-dumper to see what it brings
#
add_test (t-parsedump-sillyworld ${pypeline}
	PYPE:FORK:$ diff ${CMAKE_CURRENT_SOURCE_DIR}/sillyworld.txt FILE:actual.txt
	--
	PYPE:FORK:$ ${pype2file} FILE:actual.txt ${parsedump} FILE:actual1.asc FILE:actual2.asc FILE:actual3.asc
	--
	PYPE:FORK:$ ${pype2file} FILE:actual3.asc ${ctrl2asc} "yasd98as<SOH>x<US>y<SO>"
	--
	PYPE:FORK:$ ${pype2file} FILE:actual2.asc ${ctrl2asc} "Cup<SI>asdasx"
	--
	PYPE:FORK:$ ${pype2file} FILE:actual1.asc ${ctrl2asc} "jabba dabba d00<SOH>hello<US>Silly World"
)

#
# Run the parser-dumper on incomplete grammar
#
add_test (t-parsedump-SOH-dangling ${pypeline}
	PYPE:EXIT:1 PYPE:FORK:$ ${parsedump} "jabba dabba d00<SOH>hello<US>Silly World" "Cup<SI>asdasx" "yasd98as<SOH>x"
)

#
# Run the parser-dumper on incomplete grammar
#
add_test (t-parsedump-SOH-US-dangling ${pypeline}
	PYPE:EXIT:1 PYPE:FORK:$ ${parsedump} "jabba dabba d00<SOH>hello<US>Silly World" "Cup<SI>asdasx" "yasd98as<SOH>x<US>y"
)

#
# basic test: Pypeline generate.py --> verify.py
#
#TODO# add_test (t_basic ${pypeline}
#TODO# 	${genstreams} ${TESTBIN}/plain.test IP:HERE TCP:HERE
#TODO# 	--
#TODO# 	${vfystreams} ${TESTBIN}/plain.test IP:HERE TCP:HERE
#TODO# )

#
# basic dotty: Print on stdout/stderr and dotty-map to streams
#
#TODO# add_test (t_dotty_outflows ${pypeline}
#TODO# 	${pype2tcp} IP:HERE TCP:HERE ${dotty} ${plaintest}
#TODO# 	--
#TODO# 	${vfystreams} ${TESTBIN}/plain.test IP:HERE TCP:HERE
#TODO# )

