#!/usr/bin/env python3
#
# Generate simulated output over streams (for test purposes).
# Input lines use "stream=data" or, for the default stream, "data".
#
# This assumes the atomic model, which returns to standard after
# every change to a stream, as part of the same write.
#
# From: Rick van Rein <rick@openfortress.nl>


import sys
from sys import argv, stdin, stdout, stderr, exit

from socket import socket, AF_INET6, AF_INET, SOCK_STREAM


escapable = '\x01\x02\x03\x04\x0e\x0f\x10\x11\x12\x13\x14\x19\x1a\x1c\x1d\x1e\x1f\x00\xff'


if not 1 <= len (argv) <= 4:
	stderr.write ('Usage: %s [input] [host port]\n' % argv [0])
	exit (1);

if len (argv) in [2, 4]:
	stdin = open (argv [1], 'r')

if len (argv) > 2:
	host = argv [-2]
	port = int (argv [-1])
	try:
		sox = socket (AF_INET6, SOCK_STREAM, 0)
		sox.connect ( (host,port) )
	except:
		sox = socket (AF_INET,  SOCK_STREAM, 0)
		sox.connect ( (host,port) )
else:
	sox = None

# Pypeline synchronisation
stdout.write ('--\n')
stdout.flush ()

def escape (s):
	return ''.join ([
		( c if not c in escapable else '\x10' + chr (ord (c) ^ 0x40) )
		for c in s
	])

for arg in stdin.readlines ():
	if arg [:1] == '=':
		escval = escape (arg [1:])
		stdout.write (escval)
		if sox is not None:
			sox.send (escval.encode ('ascii'))
	elif '=' in arg:
		(stream,value) = arg.split ('=', 1)
		escval = escape (value)
		stdout.write ('\x01' + stream + '\x0e' + escval + '\x0e')
		if sox is not None:
			sox.send (('\x01' + stream + '\x0e' + escval + '\x0e').encode ('ascii'))
	else:
		escval = escape (arg)
		stdout.write (escval.encode ('ascii'))
		if sox is not None:
			sox.send (escval.encode ('ascii'))

if sox is not None:
	sox.close ()

