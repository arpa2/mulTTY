#!/usr/bin/env python3
#
# Read stdout from a sub-program and pass the traffic to a file.
#
# Called as "pype2file.py FILE prog args..."
#
# From: Rick van Rein <rick@openfortress.nl>


import sys
from sys import argv, stdin, stdout, stderr, exit

import subprocess

from socket import socket, AF_INET6, AF_INET, SOCK_STREAM

if len (argv) < 3:
	stderr.write ('Usage: %s filename wrapped_command [args...]\n' % argv [0])
	exit (1);

filename = argv [1]
subcmd = argv [2:]

file = open (filename, 'wb')

# Start wrapped subprocess
print ('Running %r' % subcmd)
wrapped = subprocess.Popen (subcmd,
		stdin=None,
		stdout=subprocess.PIPE,
		stderr=None)
		#IMPLIES:TEXT# universal_newlines=True)

# Copy content from stdout
nextln = None
while nextln != b'':
	nextln = wrapped.stdout.readline ()
	print ('nextln = %r' % nextln)
	file.write (nextln)

file.close ()

while wrapped.returncode is None:
	wrapped.wait ()

# Pypeline synchronisation
stdout.write ('--\n')
stdout.flush ()

exit (wrapped.returncode)

