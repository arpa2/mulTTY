# mulTTY - Makefile for multiplexed TTY streams
#
# This Makefile is just a stub: it invokes CMake, which in turn
# generates Makefiles, and then uses those to make the project. 
#
# Useful Make parameters at this level are:
#	PREFIX=/usr/local
#
# For anything else, do this:
#
#	make configure                 # Basic configure, remember PREFIX!
#	( cd cbuild ; ccmake )          # CMake GUI for build configuration
#	( cd cbuild ; make install )    # Build and install
#
#

PREFIX ?= /usr/local

all: compile

cbuild-dir:
	@mkdir -p cbuild

configure: _configure cbuild-dir cbuild/CMakeCache.txt

_configure:
	@rm -f cbuild/CMakeCache.txt

cbuild/CMakeCache.txt:
	( cd cbuild && cmake .. -DCMAKE_INSTALL_PREFIX=$(PREFIX) -DCMAKE_PREFIX_PATH=$(PREFIX) )

compile: cbuild-dir cbuild/CMakeCache.txt
	( cd cbuild && $(MAKE) )
	
install: cbuild-dir
	( cd cbuild && $(MAKE) install )
	
test: cbuild-dir
	( cd cbuild && $(MAKE) test )
	
uninstall: cbuild-dir
	( cd cbuild && $(MAKE) uninstall )

clean:
	rm -rf cbuild/

package: compile
	( cd cbuild && $(MAKE) package )

