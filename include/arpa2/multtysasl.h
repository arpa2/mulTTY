/* Additional definitions for mulTTY SASL.
 *
 * SASL compilation is optional for mulTTY, and this file reflects
 * the additional dependencies that make that work.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#ifndef ARPA2_MULTTY_SASL_H
#define ARPA2_MULTTY_SASL_H


#include <arpa2/multty.h>
#include <arpa2/quick-mem.h>
#include <arpa2/quick-diasasl.h>
 
 
 
/**********   SASL DEFINITIONS   **********/



/* The state for the SASL server-side connection.
 *
 * We only use MSS_CLEAN when no resources are allocated.
 * This happens to be the default after clearing memory.
 *
 * The TTY may have been altered in MSS_WAIT4xxx and will
 * be returned to its old setup in all other states.
 *
 */
enum multtysasl_server_state {
        MSS_ERROR = -1,
        MSS_CLEAN = 0,
        MSS_WAIT4CLIENT,
        MSS_WAIT4BACKEND,
        MSS_REJECTED,
        MSS_AUTHENTICATED
};


/* The mulTTY stream for "sasl" has an attachment that
 * allows the callbacks to collect data while SASL text
 * poors in.  This is part of the multtysasl_server
 * structure below, and it gets added to the program
 * while it is in active use.
 */
struct multty_ext4sasl {
        struct multty mty;
        void *fragpool;
        membuf fragbuf;
        uint32_t fraglen;
        char *c2s_mechchoice;
#ifdef OLD_SIGNAL_MODE
        char *c2s_tokenbuf;
        uint32_t c2s_tokenlen;
        bool c2s_send;
        bool c2s_teardown;
#endif
};


/* The data structure for a SASL server-side session.
 *
 * This must have a callback to insert a <NUL> terminated
 * string into the "sasl" stream in the caller.  When a
 * response arrives, the mulTTY callback TODO
 * must be triggered.  Furthermore, the diasasl_node is
 * considered to run under the caller's control, causing
 * its own callbacks when data arrives from the backend.
 *
 * The caller allocates this using mem_open() so it forms
 * a memory pool that can fixate data from the session.
 * Until the pool is cleared, the session data remains in
 * memory, so the caller can copy to its own structures.
 *
 * It is advised to finalise and thereby clear this data
 * soon after the session is over, and before engaging in
 * remote communication.
 */
struct multtysasl_server {
        //
        // Build around a Quick-DiaSASL session
        struct diasasl_session diasasl;
        struct diasasl_node *dianode;
        //
        // Configuration information
        char *config_hostname;
        char *config_service;
        //
        // The state of progress of this session
        enum multtysasl_server_state state;
        //
        // The caller_data field can be arbitrarily
        // filled by the caller.  This may be useful
        // during a callback to the caller.
        void *caller_data;
        //
        // Callback for reporting a state change
        // to a final state.  This can be used in
        // the caller to stop the authentication
        // process.
        void (*finalstate_cb) (struct multtysasl_server *this);
        //
        // Callback for printing a string on the
        // "session" stream of the caller.  Returns
        // false of failure, which triggers error.
        // The string is <NUL> terminated and has
        // been <DLE> escaped and otherwise follows
        // the format for the format of mulTTY SASL.
        bool (*send2client_cb) (struct multtysasl_server *this, const char *multtysasl_string);
        //
        // The SASL mechanism selected by the client
        membuf mechanism;
        //
        // Optional data that came along with a success
        membuf additional_data;
        //
        // The mulTTY stream for "sasl", with extensions
        struct multty_ext4sasl mtysasl;
        //
        // Terminal I/O settings (echo and such)
        struct termios _termios_original;
        int _tty_in;
        bool _termios_changed;
        bool _tty_sasl_active;
        //
        // Data passed in during a Quick-DiaSASL callback
#ifdef OLD_AND_INDIRECT
        membuf _mechlist;
        membuf _s2c_token;
#endif
        bool _success;
        bool _failure;
        //
};



/* Open a mulTTY SASL session for one authentication exchange.  Return success.
 * When successful, this needs to be reversed with multtysasl_close().
 *
 * The tty_in handle is optional.  If it is not used, set it <0.  When provided
 * and if it is a tty, echo will be suppressed and similar sanity will be used.
 * When later closing the server, this is reversed to the original state.
 *
 * The printing callback is used to print <DLE>-escaped binary content, as
 * it may occur in the "sasl" stream.  The callback asks for the current
 * stream to be set to "sasl" and then print the string without further
 * escaping.
 *
 * The node is the Quick-DiaSASL node against which this session runs.
 * It will be setup with the server_domain and svcproto and communication
 * starts immediately (reply traffic will not be processed before the caller
 * invokes diasasl_process() on the dianode, but errors may fire immediately).
 *
 * The newserver pointer must be initialised to NULL before the call.
 *
 * When a program and/or flow is provided, then their values are setup for
 * the lifetime of this object.
 *
 * After this call, it is possible to setup channel binding information,
 *      (*newserver)->diasasl.chanbind     = ...
 *      (*newserver)->diasasl.chanbind_len = ...
 * This is new territory for ASCII connections! and unknown how.
 * Ideas: 1. Use the (EC)DH key used in SSH
 *        2. Derive a key from a TLS or SSH session key
 *        3. Use the inode of the terminal
 *
 */
bool multtysasl_open (struct multtysasl_server **newserver,
                                int tty_in,
                                bool (*send2client_cb) (struct multtysasl_server *this, const char *multtysasl_string),
                                void (*finalstate_cb) (struct multtysasl_server *this),
                                struct diasasl_node *dianode,
				char *hostname,
                                char *server_domain,
                                char *svcproto,
                                struct multtyprog *prog, struct multtyflow *flow);


/* Close a mulTTY SASL session after to cleanup an authentication exchange
 * that was opened with a successful call to multtysasl_open().  This is good
 * to do as soon as possible, but any data should first be taken out of it.
 * It is a good aim to close before the next remote communication.
 *
 * The server pointer will be reset to NULL and all memory is cleared
 * (inasfar as compiler optimisation does not zealously damage that code).
 */
void multtysasl_close (struct multtysasl_server **oldserver);


#endif /* ARPA2_MULTTY_SASL_H */
