/* mulTTY -> include file
 *
 * Functions for handling mulTTY functionality without pain.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#ifndef ARPA2_MULTTY_H
#define ARPA2_MULTTY_H


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <limits.h>
#include <unistd.h>
#include <sys/uio.h>


/**********   GENERAL DEFINITIONS   **********/



/* A number of ASCII definitions that we use in mulTTY.
 * The c_XXX forms are characters, s_XXX are strings.
 * The DCx names are so generic that we add our own,
 * all starting with letter 'P' for program.
 */
#define c_NUL '\x00'
#define c_SOH '\x01'  /* mulTTY name prefix */
#define c_STX '\x02'
#define c_ETX '\x03'
#define c_EOT '\x04'
#define c_ENQ '\x05'
#define c_ACK '\x06'
#define c_SO  '\x0e'  /* mulTTY stream switch, shift out */
#define c_SI  '\x0f'  /* mulTTY stream switch, shift in  */
#define c_DLE '\x10'  /* mulTTY escape, with XOR 0x40 */
#define c_DC1 '\x11'
#define c_DC2 '\x12'
#define c_DC3 '\x13'
#define c_DC4 '\x14'
#define c_NAK '\x15'
#define c_SYN '\x16'
#define c_ETB '\x17'
#define c_CAN '\x18'
#define c_EM  '\x19'  /* mulTTY end of stream/program media */
#define c_SUB '\x1a'
#define c_FS  '\x1c'
#define c_GS  '\x1d'
#define c_RS  '\x1e'
#define c_US  '\x1f'  /* mulTTY machine/human name portion separator */
#define c_DEL '\x7f'
#define c_IAC '\xff'  /* for Telnet-aware escaping */
#define c_PUP  c_ETX  /* mulTTY program up     */
#define c_PRM  c_EOT  /* mulTTY program remove */
#define c_PDN  c_STX  /* mulTTY program down   */
#define c_PSW  c_SUB  /* mulTTY program switch */
/* strings */
#define s_NUL "\x00"
#define s_SOH "\x01"  /* mulTTY name prefix */
#define s_STX "\x02"
#define s_ETX "\x03"
#define s_SOT "\x04"
#define s_ENQ "\x05"
#define s_ACK "\x06"
#define s_SO  "\x0e"  /* mulTTY stream switch, shift out */
#define s_SI  "\x0f"  /* mulTTY stream switch, shift in  */
#define s_DLE "\x10"  /* mulTTY escape, with XOR 0x40 */
#define s_DC1 "\x11"
#define s_DC2 "\x12"
#define s_DC3 "\x13"
#define s_DC4 "\x14"
#define s_NAK "\x15"
#define s_SYN "\x16"
#define s_ETB "\x17"
#define s_CAN "\x18"
#define s_EM  "\x19"  /* mulTTY end of stream/program media */
#define s_SUB "\x1a"
#define s_FS  "\x1c"
#define s_GS  "\x1d"
#define s_RS  "\x1e"
#define s_US  "\x1f"  /* mulTTY machine/human name portion separator */
#define s_DEL "\x7f"
#define s_IAC "\xff"  /* for Telnet-aware escaping */
#define s_PUP  s_ETX  /* mulTTY program up     */
#define s_PRM  s_EOT  /* mulTTY program remove */
#define s_PDN  s_STX  /* mulTTY program down   */
#define s_PSW  s_SUB  /* mulTTY program switch */


/* Events as delivered by the Ragel parser and possibly used
 * over other communication interfaces.  These align with the
 * various parser events, but events are called with the
 * character range to process, so they are not too far off.
 *
 * Event numeric values matter:
 *  - 000..009 --> text handling (includes prename, description)
 *  - 010..019 --> stream handling
 *  - 020..029 --> multiplex handling
 *  - 030..099 --> reserved
 *  - 100..inf --> applications, in blocks of 100 each
 */
typedef enum {
	//
	/////   BASIC TEXT AND NAMING   /////
	//
	// No known state of parser progress
	MULTTY_RESET = 0,
	//
	// No command being processed, just (binary) data
	MULTTY_CONTENT,
	//
	// <SOH> is followed by a prefix name
	MULTTY_PREFIX_NAME,
	// <SOH>...<UL> is followed by a description
	MULTTY_DESCRIPTION,
	//
	/////   MULTTY STREAM HANDLING   /////
	//
	// <SI> or <SO> shifts to the prefix-named or default stream
	// (they are indistinguishable, except in the ASCII grammar)
	MULTTY_SHIFT_STREAM = 10,
	// <EM> ends the current stream, prename/descr can say why
	MULTTY_END_STREAM,
	//
	/////   MULTTY PROGRAM MULTIPLEXING   /////
	//
	// <STX> moves to the [current/named] child program
	MULTTY_PROGRAM_CHILD = 20,
	// <ETX> moves to the [current/named] program in the parent
	MULTTY_PROGRAM_PARENT,
	// <SUB> moves to the [former /named] sibling program
	MULTTY_PROGRAM_SIBLING,
	// <EOT> stops    the [current/named] program
	MULTTY_PROGRAM_STOP,
	//
	/////   SASL STREAMS   /////
	//
	// <ENQ><CAN>...<ETB> is a s2c login request with a few fields
	MULTTY_SASL_S2C_INITIATE_HOSTNAME = 100,
	MULTTY_SASL_S2C_INITIATE_SERVICE,
	MULTTY_SASL_S2C_INITIATE_MECHLIST,
	//      <CAN>...<ETB> is a c2s login attempt with a mechname
	MULTTY_SASL_C2S_MECHNAME,
	// <ENQ><CAN><ETB> is a s2c teardown instruction
	MULTTY_SASL_S2C_TEARDOWN,
	// <CAN><ETB>      is a c2s teardown instruction
	MULTTY_SASL_C2S_TEARDOWN,
	// <ACK>...<ETB> is an authentication success
	MULTTY_SASL_SUCCESS,
	// <NAK><ETB>     is an authentiction failure
	MULTTY_SASL_FAILURE,
	//      <US>...<ETB> is a c2s token,      <ETB> is no c2s token
	MULTTY_SASL_C2S_TOKEN,
	MULTTY_SASL_C2S_NO_TOKEN,
	// <ENQ><US>...<ETB> is a s2c token, <ENQ><ETB> is no s2c token
	MULTTY_SASL_S2C_TOKEN,
	MULTTY_SASL_S2C_NO_TOKEN,
	//
	/////   SHELL STREAMS   /////
	//
	// <US> separates words
	MULTTY_SHELL_WORD_SEP = 200,
	// <RS> ends or separates...?
	_MULTTY_SHELL_RESERVED_RS,
	// <GS> separates definitions and their use
	MULTTY_SHELL_DEFINE,
	// <FS> separates commands
	MULTTY_SHELL_COMMAND_SEP,
	// <ENQ> asks for completion suggestions
	MULTTY_SHELL_COMPLETION,
	//
	/////   MENU/TABLE STREAMS   /////
	//
	// <FS> separates (cacheable/named) files
	MULTTY_MENUTABLE_FILE = 300,
	// <RS> separates records
	MULTTY_MENUTABLE_RECORD_SEP,
	// <GS> separates alternatives
	MULTTY_MENUTABLE_ALTERNATIVE_SEP,
	// <US> separates list/record elements
	MULTTY_MENUTABLE_ELEMEMENT_SEP,
	// <ENQ> represents that an entry is incomplete
	MULTTY_MENUTABLE_INCOMPLETE,
	//
} multty_event_t;


struct multtyflow;
struct multtyprog;
struct multty;



/**********   FLOW DEFINITIONS   **********/



/* Callbacks with text, passed into the current flow.
 *
 * The txt and len point to character data, if applicable.
 * The len may be 0, txt is only NULL if an optional bit is
 * absent.
 *
 * The text may be delivered in multiple chunks, as a result
 * of the input blocks handed into the parser, or any other
 * concern local to the parser.  The first and last flag are
 * set to start a new text and reset to end the last for the
 * event.
 */
typedef void (*multtyflow_text_callback_t) (
		struct multtyflow *mtyf, multty_event_t evt,
		char *txt, unsigned len,
		bool first, bool last);


/* Callbacks with mulTTY events, based on availability of data.
 * textual parameters from <SOH>...<US>... --if any-- have been
 * delivered before, via callbacks for text events.
 *
 * The named flag is set if a prefix name was delivered for
 * this event.  The description flag is set if a description
 * was delivered for this event.  Such information will have
 * been delivered in prior text callbacks.  There is no grammar
 * for a description except following a name.
 */
typedef void (*multtyflow_event_callback_t) (
		struct multtyflow *mtyf, multty_event_t evt,
		bool named, bool described);


/* Callbacks when the parser encounters a bad character.
 */
typedef bool (*multtyflow_badchar_callback_t) (
		struct multtyflow *mtyf, multty_event_t evt,
		char *bad);


/* Two different escape profiles.  Binary content wants to send
 * no bytes that might be construed as ASCII control codes, and
 * it also encodes 0x00 and 0xff.  ASCII itself, when passed as
 * not-yet-mulTTY-compliant, should leave common ASCII codes but
 * escape those that are specifically interpreted by mulTTY (and
 * barely used in normal ASCII disciplines anyway).  Finally, if
 * content is aware of mulTTY already, it bypasses escaping.
 *
 * The use of ASCII is practical for readability, but it might
 * cause changes to binary codes, especially <NUL> which may be
 * removed at will, or added at will while not being recognised
 * as such, and <IAC> which would impair the ability to pass it
 * over Telnet, which is a source for a port number quit suitable
 * for mulTTY and already standardised and abandoned.  So we add
 * a MIXED escaping discipline that solves these problems, to
 * pass everyday ASCII as is, but protect <NUL> and <IAC> while
 * always escaping the codes that might be confused for mulTTY
 * control codes.  Where we use strings or string buffers within
 * mulTTY markup, we always use the MIXED discipline.
 *
 * The codes <DC1> through <DC4> are for device control and not
 * to be had by any ASCII application program.  For this reason,
 * mulTTY streams must always escape them, and mulTTY itself
 * will not use it (for 
 *
 * Escaping is NOT idempotent, because <DLE> gets escaped.  The
 * only algebraic nicety is the mulTTY escaping is a zero, both
 * left-sided and right-sided.  But even that is not guaranteed
 * to hold true in the future.  Do not escape upon escape unless
 * you plan to reverse it with unescape after unescape.
 *
 * TODO: No ASCII confusion from 2,3,4,5,6 and 21,22,23,24 and 28,29,30,31 until they are used in streams
 */
#define MULTTY_ESC_NOTHING ((uint32_t) 0x00000000)
#define MULTTY_ESC_MULTTY  ((uint32_t) 0x00000000)
#define MULTTY_ESC_BINARY  ((uint32_t) 0xffffffff)
#define MULTTY_ESC_ASCII   ((uint32_t) ( (1<< 1)|(1<< 2)| \
	(1<< 3)|(1<< 4)|(1<< 5)|(1<< 6)| \
	(1<<11)|(1<<12)|(1<<13)|(1<<14)| \
	(1<<15)|(1<<16)|(1<<21)|(1<<22)|(1<<23)| \
	(1<<24)|(1<<25)|(1<<26)|(1<<28)|(1<<29)|(1<<30)|(1<<31)) )
#define MULTTY_ESC_MIXED   ((uint32_t) ( (1<<0) | MULTTY_ESC_ASCII ))


/* Check whether escaping is useful for a character under the
 * given escape style.  The style exists to minimise traffic
 * and avoid confusion ("why is my LF escaped?")
 *
 * There are a few styles, which we all port back to the range
 * of control codes.
 *  - most characters are printable, and never escaped
 *  - classical control codes are escaped in binary data
 *  - character <DEL> 0x7f is escaped when <BS>  0x08 is
 *  - character <IAC> 0xff is escaped when <NUL> 0x00 is
 *
 * The <IAC> character is specific to Telnet, where it is the
 * only break with "8-bit clean" transport.  Since mulTTY may
 * be used on port 23, we should avoid this kind of confusion
 * and will escape when the assumption is binary, when <NUL>
 * should also be protected by escaping.
 *
 * Escaping is not done here.  It is simply a prefix <DLE> and
 * the character will be added XOR 0x40.
 */
bool mtyescapewish (uint32_t style, uint8_t ch);


/* Check that a character sequence is free from the wish to
 * escape any of its characters, under the given style.
 * This may be used to assure that inserts that end up in
 * mulTTY structures have no confusing characters.
 *
 * As an example, "<SOH>id<US>tralala<XXX>" strings might
 * be constrained to have no control characters in id, and
 * only ASCII including high-top-bit in tralala.  This
 * should not constrain proper use cases, but it could not
 * be abused to hijack mulTTY streams.  The difference
 * would be made with the escape style parameter, set to
 * MULTTY_ESC_BINARY for the id and MULTTY_ESC_ASCII for
 * tralala.  The latter could include <CR><LF> and so on,
 * which have no place in binary content, but neither has
 * a place for embedded <DLE> or <SOH> characters to avoid
 * accidentally or malicuously overtaking <US> or <XXX>.
 */
bool mtyescapefree (uint32_t style, const uint8_t *ptr, int len);


/* Programs are identified with a standard structure
 * holding an id name of up to 32 chars and optionally
 * a <US> appended to indicate the use of a description.
 */
typedef char MULTTY_PROGID [33];



/* Mixed input/output flows for mulTTY.
 *
 * Flows can be open as input and/or output, in any desired
 * combination.  They are both here because the mixed I/O
 * function often makes sense; applications may use
 * bidirectional streams: SASL, shell, Z-modem, ...
 *
 * Input flows hold parser input and state, plus the current
 * program and stream, so it is clear where to relay events.
 * Set the current in curin; the current program in curinplex.
 *
 * Output flows hold current output program and stream, so a
 * switch can be made if this is desired.  Change the current
 * stream in curout; change the current program in curoutplex.
 *
 * All flows can be found in a linked list starting at streams.
 * Maintaining this list enables incoming flows to be directed
 * properly.  Multiplexing is a relative operation, and chases
 * links relative to the current program.
 */
typedef struct multtyflow {
	//
	//===== INPUT SECTION =====//
	//
	// The event type currently being parsed
	multty_event_t progress;
	//
	// Whether the parser detected a prename, description
	bool named, described;
	//
	// The event handler for delivering parsed text
	multtyflow_text_callback_t text_cb;
	//
	// The event handler for delivering mulTTY operations
	multtyflow_event_callback_t event_cb;
	//
	// The event handler for bad characters
	multtyflow_badchar_callback_t badchar_cb;
	//
	// Parser internals to allow partial parsing
	int ragel_cs;
	//
	// The list of all streams, the first is default in/out
	//TODO//MULTTYPROG// struct multty_t *allin;
	//
	// The last-seen <SO> or <SI> variant
	char siso_in;
	//
	// The "physical" input file handle
	FILE *realin;
	//
	// Current input program/stream
	struct multty     *curin;
	struct multtyprog *curinprog;
	//
	//===== OUTPUT SECTION =====//
	//
	// Current output program/stream
	struct multty     *curout;
	struct multtyprog *curoutprog;
	//
	// The last-relayed <SO> or <SI> variant
	char siso_out;
	//
	// The "physical" output file handle
	FILE *realout;
} multtyflow_t;



/** Initialise a multtyflow structure.
 *
 * This structure must be allocated and freed by the caller.
 * After this call, the structure is ready for processing data
 * with multtyflow_process() calls, and final cleanup with
 * multtyflow_fini().
 *
 * The multtyplex flag can be set to prepare a parser for the
 * extended variant that includes multiplexing of programs.
 * When multtyplex flags false, the parser expects to process
 * a single program's flow, consisting of streams.
 *
 * You should complete this structure:
 *  - mtyf->text_cb    must be the text/label/descr callback
 *  - mtyf->event_cb   must be the mulTTY event callback
 *  - mtyf->badchar_cb must be the bad character callback
 */
void multtyflow_init (struct multtyflow *mtyf, bool multtyplex);


/** Cleanup a multtyflow structure.
 *
 * This structure was initialised by multtyflow_init() and
 * may since then have been called as multtyflow_process() or
 * multty_process_text().
 *
 * The structure is cleaned up.  The return value is only true
 * when the parser reached an end state, without half-done
 * structures open.
 */
bool multtyflow_fini (struct multtyflow *mtyf);


/** Process mulTTY ASCII input by parsing and delivering it.
 *
 * This will result in callbacks for delivering parts of the
 * ASCII flow to backends.  Any structures/strings broken at
 * the end of this part will be delivered in separate chunks,
 * with a flag marking that the last bit was not done yet.
 * This makes it possible to reuse the buffer after this call
 * returns.
 *
 * Parser errors are handled through callbacks, so this
 * function can be used in fire-and-forget mode.  The stx_cb
 * and stxerr_cb are expected to have been setup after having
 * called multtyflow_init().
 *
 * When integrated into a concurrent program, do not re-enter
 * it for the same inflow structure, as that would confuse
 * the order in which the events are delivered, and require
 * them to work well under re-entry.  This is more easily
 * achieved by using multtyflow_process() instead.
 */
void multtyflow_process_text (struct multtyflow *mty, char *txt, unsigned len);


#if 0
/** Read and process mulTTY ASCII input.
 *
 * This will read input from the current input flow and
 * deliver to the parser to call events, as described for
 * multtyflow_process_text().
 *
 * When the current input flow can block, then so can
 * this call.  Non-blocking inputs may fail with EAGAIN,
 * which is considered normal and acceptable to this call.
 * This should make the function useful as event handler.
 */
void multtyflow_process (struct multtyflow *mty);
#endif


/* The parser callback for SASL, which can be hung into the stream
 * receiving it.  This will be used in preference of any other
 * callback routines, it uses the grammar defined herein and
 * triggers textual and event callbacks specific to SASL.
 */
void multtysasl_parse_cb (struct multty *mty, multty_event_t _event,
			char *txt, unsigned len,
			bool first_up, bool last_up);




/**********   STREAM DEFINITIONS   **********/



/* Callbacks to an optional stream parser, defined by any
 * flow that needs to analyse its input.
 *
 * The text may be delivered in multiple chunks, as a result
 * of the input blocks handed into the parser, or any other
 * concern local to the parser.  The first and last flag are
 * set to start a new text and reset to end the last for the
 * event.
 *
 * When a parser is defined, it may use the text and event
 * callbacks for delivery of its findings.  If escapes
 * need to be removed, then this is either the task for
 * the parser or the software reading from it.
 */
typedef void (*multty_parser_callback_t) (
		struct multty *mty, multty_event_t evt,
		char *txt, unsigned len,
		bool first, bool last);


/* Callbacks with text, passed into the current stream.
 *
 * The txt and len point to character data, if applicable.
 * The len may be 0, txt is only NULL if an optional bit is
 * absent.
 *
 * The text may be delivered in multiple chunks, as a result
 * of the input blocks handed into the parser, or any other
 * concern local to the parser.  The first and last flag are
 * set to start a new text and reset to end the last for the
 * event.
 *
 * This function is invoked directly by a flow when it finds
 * no parser for this stream.  When a parser is defined, it
 * will call that and leave triggering of this event to the
 * parser.
 */
typedef void (*multty_text_callback_t) (
		struct multty *mty, multty_event_t evt,
		char *txt, unsigned len,
		bool first, bool last);


/* Callbacks with mulTTY events, based on availability of data.
 * textual parameters from <SOH>...<US>... --if any-- have been
 * delivered before, via callbacks for text events.
 *
 * The named flag is set if a prefix name was delivered for
 * this event.  The description flag is set if a description
 * was delivered for this event.  Such information will have
 * been delivered in prior text callbacks.  There is no grammar
 * for a description except following a name.
 *
 * This definition may be triggered by a parser, if one is
 * defined for this stream.
 */
typedef void (*multty_event_callback_t) (
		struct multty *mty, multty_event_t evt,
		bool named, bool described);


/** The mulTTY stream defines a program's named input and/or output.
 *
 * Streams are local to a flow, and offers the in/out directions
 * as they are supported by the flow.  Every flow has one default
 * stream, named "", and separately it has a current stream that
 * is the target for current input.
 *
 * Multiplexers would be aware of the program under which a stream
 * is defined.  The program serves like a naming scope for streams.
 *
 * Without a parser, literal text fragments are passed into the
 * text callback, after removing escapes to form a text fragment
 * that may hold arbitrary content, including UTF-8 and control
 * characters, including even <NUL> and <IAC> characters.  On
 * output, escapes are added to some characters/bytes.
 *
 * When a parser is attached to the stream, then the literal text
 * fragments pass into the parser without removal of escapes.  The
 * parser in turn may invoke text and event callbacks.  Any output
 * sent over this stream should also be in "wire format" and must
 * not hold annotations that could confuse remote-end parsing by
 * mulTTY or other software.  This may be verified/corrected.
 *
 * You can set these fields directly:
 *  - mty->description
 *  - mty->description_fresh
 *  - mty->text_cb
 *  - mty->event_cb
 *  - mty->parse_cb
 */
typedef struct multty {
	//
	//===== INPUT SECTION =====//
	//
	//
	// Links to related structures
	struct multty *next, *prev;
	struct multtyflow *flow;
	struct multtyprog *program;
	//
	// The stream name is up to 32 chars but 32+ differs from 32
	char name [33];
	//
	// The stream description is optional text that may be
	// sent after <SOH>...<US> when description_fresh is set.
	char *description;
	bool description_fresh;
	//
	// The actual input file descriptor
	//TODO//ASYMM// int infd;
	//
	// Callback for incoming text
	multty_text_callback_t text_cb;
	//
	// Callback for incoming events
	multty_event_callback_t event_cb;
	//
	// Literal input sent to a parser
	multty_parser_callback_t parse_cb;
	//
	// The event type currently being parsed
	multty_event_t progress;
	//
	// Parser internals to allow partial parsing
	int ragel_cs;
	//
	// When a parser is used, this can hold its intermediate state
	int parser_state;
	void *parser_store;
	//
	//===== OUTPUT SECTION =====//
	//
	// The actual output file descriptor
	//TODO//ASYMM// int outfd;
} multty_t;


#if 0
/** The "stdio" stream holds stdin for input and stdout for output.
 *
 * This stream is therefore bidirectional, and can be used in any
 * mulTTY program.  It is setup by default as the first entry,
 * making this the default stream.
 */
extern struct multty _multty_stdio;
#define multty_stdio (&_multty_stdio)
#endif


#if 0
/* The "errctl" stream holds stdctl for input and stderr for output.
 *
 * The "stdctl" stream is a new idea, to help passing management
 * information such as signals and possibly interrogate the
 * program for its running state and list interactive commands.
 *
 * The "errctl" stream is setup by default in any mulTTY program,
 * as the second entry after the "stdio" stream.
 */
extern struct multty _multty_errctl;
#define multty_errctl (&_multty_errctl)
#endif


/* The streams "stdin", "stdout" and "stderr" represent the usual
 * targets, except that they pass throug mulTTY parsing on input,
 * and multiplexing on output.
 *
 * The additional stream "stdctl" is meant for controlling a
 * program from the outside.  It can be taken out by a shell
 * or other surrounding structure, and then used to control
 * the program.  Think of killing, debugging, added streams for
 * logging, debugging, call tracing or profiling.  Note that it
 * speaks _about_ a program and is usually handled outside it.
 *
 * The additional stream "stdopt" is meant to supply options
 * that can be used.  This can be used for such things as
 * command completion, a review of available options and its
 * automation-friendly output can even be used to fill menus
 * in an interactive environment.
 *
 * All these handles are pre-opened, though not yet filled
 * with their underlying FILE * streams.
 */
extern struct multty _multty_stdin ;
extern struct multty _multty_stdout;
extern struct multty _multty_stderr;
extern struct multty _multty_stdctl;
extern struct multty _multty_stdopt;

#define multty_stdin  (&_multty_stdin )
#define multty_stdout (&_multty_stdout)
#define multty_stderr (&_multty_stderr)
#define multty_stdctl (&_multty_stdctl)
#define multty_stdopt (&_multty_stdopt)


/* Output a buffer of a given length over the given mulTTY stream.
 * Apply the indicated escape style to avoid confusion with mulTTY
 * encoding.  The style varies between MULTTY_ESC_BINARY rigour
 * and MULTTY_ESC_ASCII for readability, but MULTTY_ESC_NOTHING
 * would set up the stream for trouble.
 */
ssize_t mtyescwrite (multty_t *mty, const uint8_t *bufptr, size_t buflen, uint32_t style);


/* Be sure that the current output stream equals mty.  If not yet
 * set like that, send the ASCII codes to switch.
 *
 * Note, this does not take program multiplexing into account.
 *
 * Return true if a switch was made, false otherwise.
 */
static inline bool mtyflow_outputstream (multty_t *mty) {
	if (mty->flow->curout != mty) {
		mty->flow->curout = mty;
		if ((mty == multty_stdout) || (mty == multty_stdin) || (*mty->name == '\0')) {
			fputc (c_SO, mty->flow->realout);
		} else {
			fprintf (mty->flow->realout, s_SOH "%s" s_SO, mty->name);
		}
		return true;
	} else {
		return false;
	}
}


/* Send an ASCII string buffer to the given mulTTY stream.  It
 * will be escaped using MULTTY_ESC_MIXED style which leaves
 * many common controls unchanged for readability, but escapes
 * those used by mulTTY, plus the exceptional values <NUL> and
 * <IAC>.
 *
 * Return the number of input characters written.
 */
static inline size_t mtyputstrbuf (multty_t *mty, const char *strbuf, int buflen) {
	return (size_t) mtyescwrite (mty, strbuf, buflen, MULTTY_ESC_MIXED);
}



/* Send an ASCII string to the given mulTTY steam.  It will be
 * escaped using the MULTTY_ESC_MIXED style which leaves many
 * common controls unchanged for readability, but escapes those
 * used by mulTTY, plus the exceptional values <NUL> and <IAC>.
 *
 * Drop-in replacement for fputs() with FILE changed to multty_t.
 * Returns >=0 on success, else EOF/errno
 */
static inline int mtyputs (const char *s, multty_t *mty) {
	bool ok = mtyputstrbuf (mty, s, strlen (s));
	return ok ? 0 : EOF;
}


/* Send binary data to the given mulTTY stream.
 *
 * Drop-in replacement for write() with FD changed to MULTTY*.
 * Note: This is *NOT* a drop-in replacement for fwrite().
 *
 * Since it passes over ASCII, some codes will be escaped, using
 * the MULTTY_ESC_BINARY style to escape all controls and <IAC>.
 * This causes a change to the wire size that is not reflected in
 * the return value that holds the number of input bytes sent.
 */
static inline ssize_t mtywrite (multty_t *mty, const void *buf, size_t count) {
	return (ssize_t) mtyescwrite (mty, buf, count, MULTTY_ESC_BINARY);
}


/* Flush the currenout mulTTY output stream.
 */
static inline void mtyflush (multty_t *mty) {
	fflush (mty->flow->realout);
}




/**********   PROGRAM AND MULTIPLEXER DEFINITIONS   **********/




/** The structure for a mulTTY program, with the current one on top.
 *
 * The global variable TODO:MULTTY_PROG references the current program, as
 * defined by the mulTTY library.  It is also available to programs
 * that are not multiplexers.
 *
 * Every program has a multtyprog structure, and this may just be a
 * global variable.  When multtyplex support is added, there can be
 * a tree of other programs underneath:
 *  - next, prev point to siblings at the same level
 *  - parent points one level up
 *  - children points to the children one level down
 * Initially, all these values are NULL, multiplexers work on that.
 *
 * It is generally an error if an attempt is made to multiplex to
 * a level above the parent.  Though this program may be part of
 * a larger hierarchy, such requests should be shielded from it
 * by the multiplexer that drives it.  Failure to do so is likely
 * a sign that the sysop did not properly combine programs.
 *
 * Programs have a name that can be used for fast switching by
 * multiplexer ASCII instructions.  The program itself is known
 * as "" and is not usually addressed explicitly.
 *
 * The program has a current stream, separately for input and output.
 * The program has a default stream, separately for input and output.
 * All these are usually initialised to a flow for stdin/stdout.
 */
typedef struct multtyprog {
	//
	// Relations to other mulTTY programs, suited for multtyplexing
	struct multtyprog *next, *prev, *parent;
	struct multtyprog *children;
	//
	// The program name is up to 32 chars but 32+ differs from 32
	char name [33];
	//
	// The head of the list of streams for this program
	struct multty *streams;
	//
	// The current input and output stream for this program
	struct multty *curin, *curout;
	//
	// The default input and output stream for this program
	struct multty *dflin, *dflout;
} multtyprog_t;


/** The program can reference itself as multtyprog_main.
 *
 * This is a global variable, created as part of the mulTTY library.
 * It is not just available to multiplexers.
 *
 * This self-referencing program is setup with a default flow that
 * has a "stdio" as well as an "errctl" stream.
 */
extern struct multtyprog _multtyprog_main;
#define multtyprog_main (&_multtyprog_main)



/** Add a mulTTY stream to a mulTTY program.
 *
 * An initialised stream should be assigned to one flow so it can be found
 * when it processes input text.
 *
 * The flow will be added as the last list element, the first one added will
 * (continue to) be the default flow.
 *
 * Multiplexers use this call to dynamically add programs to their managed
 * program hierarchy.
 *
 * Returns true when the stream name is new and was therefore added.
 * Returns false otherwise.
 */
bool multtyprog_addstream (struct multtyprog *prog, struct multty *mty);


/** Remove a mulTTY stream from a mulTTY program.
 *
 * This is the opposite of multtyprog_add_stream().
 *
 * Multiplexers use this call to dynamically remove programs from their
 * managed program hierarchy.
 *
 * Returns true when the stream existed and was therefore removed.
 * Returns false otherwise.
 */
bool multtyprog_delstream (struct multty *mty);


/** Add a mulTTY program to a mulTTY flow.
 *
 * Multiplexers keep track of multiple programs, each with their own set
 * of streams.
 */
void multtyflow_addprog (struct multtyflow *mtyf, struct multtyprog *mtyp);


/** Delete a mulTTY program from a mulTTY flow.
 *
 * This is the opposite of multtyflow_add_program().
 */
void multtyflow_delprog (struct multtyflow *mtyf, struct multtyprog *mtyp);




/**********   OLDER DEFINITIONS   **********/




/* The types for a program set and program are opaque.
 */
//MAYBE// struct multty_progset;
//MAYBE// struct multty_prog;
typedef struct multty_progset MULTTY_PROGSET;
typedef struct multty_prog    MULTTY_PROG   ;


/* The handle structure, with builtin buffer and stream name,
 * for a MULTTY stream.
 */
struct OLD_multty {
	struct multty *next;
	MULTTY_PROG *prog;
	int shift;
	int fill;
	int rdofs;
	uint8_t buf [PIPE_BUF];
};
typedef struct OLD_multty MULTTY;


/* The input flow structure, over which multiplexed traffic
 * arrives, and from which it is split into programs and/or
 * streams.
 */
typedef struct multty_inflow MULTTY_INFLOW;


/* Standard pre-opened handles for "stdin", "stdout", "stderr".
 * Stored in global variables that may be included.
 */
#if 0
extern struct multty multty_stdin;
extern struct multty multty_stdout;
extern struct multty multty_stderr;
#define MULTTY_STDIN  (&multty_stdin )
#define MULTTY_STDOUT (&multty_stdout)
#define MULTTY_STDERR (&multty_stderr)
#endif


/* We can have a global variable with the default program set.
 * It will be instantiated when it referenced, anywhere.
 * Functions call for a pointer, hence MULTTY_PROGRAMS.
 */
extern struct multty_progset MULTTY_PROGRAMSET_DEFAULT;
#define MULTTY_PROGRAMS (&MULTTY_PROGRAMSET_DEFAULT)



/********** FUNCTIONS FOR STREAM MULTIPLEXING **********/



/* Construct a standard identity structure from an identity string
 * (up to 32 chars) with an optionally appended <US> when it will
 * carry along a description.
 *
 * Return true on success, or else false.
 */
bool mtyp_mkid (const char *id, bool with_descr, MULTTY_PROGID prgid);


/* Close the MULTTY handle, after flushing any remaining
 * buffer contents.  It is the opposite of mtyoutstream()
 *
 * Drop-in replacement for fclose() with FILE changed to MULTTY.
 * Returns 0 on success, else EOF+errno.
 */
int mtyclose (MULTTY *mty);


/* Escape a string and move it into the indicated MULTTY buffer.
 * The escaping style is provided as a parameter.
 *
 * Escaping is always done by prefixing `<DLE>` and adding the
 * character XOR 0x40.  Control codes end up as 0x40 to 0x5f,
 * DEL becomes 0x3f and Telnet IAC becomes 0xbf.
 *
 * Returns the number of escaped characters, not counting the
 * escapes themselves.  Does not fail other than incomplete
 * translation, which is always accountable to a filled buffer.
 * Note that the return value may be 0, but mtyflush() should
 * then allow further use of this function.
 */
size_t OLD_mtyescape (uint32_t style, MULTTY *mty, const uint8_t *ptr, size_t len);


/* Flush the MULTTY buffer to the output, using writev() to
 * ensure atomic sending, so no interrupts with other streams
 * even in a multi-threading program.
 *
 * The buffer is assumed to already be escaped inasfar as
 * necessary.
 *
 * Drop-in replacement for fflush() with FILE changed to MULTTY.
 * Returns 0 on success, else EOF+errno.
 */
int OLD_mtyflush (MULTTY *mty);


/* Open an MULTTY buffer for input ("r") or output ("w")
 * and using the given stream name.  TODO: Currently the
 * mode "r" / "w" is not used.  TODO: Output is always
 * shift-out based, using `<SO>` ASCII, not `<SI>`.
 *
 * The mty is setup with the proper iov[] describing its
 * switch from stdout, to its buffer, back to stdout.  This
 * is pretty trivial for stdout, of course, and treated in
 * more optimally than the others.
 *
 * The buffer is assumed to already be escaped inasfar as
 * necessary.
 *
 * There are default handles opened for MULTTY_STDOUT and
 * MULTTY_STDIN and MULTTY_STDERR.  Note that "stderr" is
 * in no way special because it is just a named stream,
 * but "stdout" and "stdin" are default streams and need
 * no stream shifting because we return to it after all
 * other writes and reads.  If you open "stdout" and
 * "stdin" here you do get them with the explicit stream
 * shift, which is harmless and actually necessary when
 * not all other uses return to the default stream.
 *
 * The streamname must be free from any control codes or
 * other aspects that would incur MULTTY_ESC_BINARY, or
 * else mtyoutstream() returns NULL/EINVAL.
 *
 * Drop-in replacement for fopen() with FILE changed to MULTTY.
 * Returns a handle on success, else NULL+errno.
 */
MULTTY *mtyoutstream (const char *streamname);


/* Send an ASCII string buffer to the given mulTTY stream.
 * Since it is ASCII, it will be escaped as seen fit.
 *
 * Return true on success, else false/errno.
 */
bool OLD_mtyputstrbuf (MULTTY *mty, const char *strbuf, int buflen);


/* Send an ASCII string to the given mulTTY steam.
 * Since it is ASCII, it will be escaped as seen fit.
 *
 * Drop-in replacement for fputs() with FILE changed to MULTTY.
 * Returns >=0 on success, else EOF/errno
 */
inline int OLD_mtyputs (const char *s, MULTTY *mty) {
	bool ok = OLD_mtyputstrbuf (mty, s, strlen (s));
	return ok ? 0 : EOF;
}


/* Send binary data to the given mulTTY steam.
 * Since it passes over ASCII, some codes will
 * be escaped, causing a change to the wire size.
 * Reading back would unescape and remove this.
 *
 * Drop-in replacement for write() with FD changed to MULTTY*.
 * Note: This is *NOT* a drop-in replacement for fwrite().
 * Returns buf-bytes written on success, else -1&errno
 */
ssize_t OLD_mtywrite (MULTTY *mty, const void *buf, size_t count);


/* Open an inflow for a given file descriptor.
 *
 * Returns non-NULL pointer or NULL/errno.
 */
MULTTY_INFLOW *mtyinflow (int infd);



/********** FUNCTIONS FOR STREAM READER DISPATCH **********/



/* When ASCII data is ready for dispatch, a callback is
 * triggered, to be used in an event loop for future
 * processing.  The buffer is not yet unescaped, and
 * asking for this to be done with the right profile
 * is part of the task of the callback.
 *
 * The callback is not free to defer the extraction.
 * It is especially not in a position to wait for more
 * input, for instance to complete an ASCII structure
 * that was read only partially, as that may be the
 * result of stream switching or program multiplexing
 * and the other traffic also needs to get through.
 *
 * Note that dangling <DLE> characters are cared for
 * in the unescape routines, but your application
 * structures may need to be manually dealt with.
 *
 * When an <SOH> is encountered, it is delivered as
 * a naming prefix for a new callback invocation,
 * with a length and the following control code.
 * Without <SOH>, name is NULL, namelen is -1 and
 * control is <NUL>.  No <SOH> should be expected
 * midway a fragment provided through mtycb_ready().
 * Note that some of the <SOH> are consumed by the
 * dispatcher, namely to distinguish streams named
 * before <SO> or <SI> shifting.
 *
 * Control codes <SO> or <SI> may be delivered if
 * they imply a change to the stream's shift status,
 * which is initially set as though <SO> was issued.
 *
 * Callbacks are registered with a MULTTY, together with
 * a userdata pointer reproduced here.
 */
typedef void mtycb_ready (MULTTY_INFLOW *flow, void *userdata,
		char *name, int namelen, uint8_t control);


/* Register a callback function with arbitrary userdata
 * pointer, to be invoked when data arrives for the
 * named stream at the given inflow.
 *
 * The inflow may be NULL to reference the MULTTY_STDIN
 * default.
 *
 * The stream name may be NULL to indicate the default
 * stream, which may be compared to stdin/stdout.  The
 * name is assumed to be a static string.
 *
 * The callback function may be NULL to indicate that
 * the previously registered function is to stop being
 * called.  This may involve the cleanup of the stream
 * from the inflow (when it is not the default), though
 * that may also be deferred until inflow cleanup.
 *
 * The userdata is passed whenever the callback function
 * is invoked; this also happens when it is NULL.
 *
 * Return true on success, else false/errno
 */
bool mtyregister_ready (MULTTY_INFLOW *flow, char *stream,
			mtycb_ready *rdy, void *userdata);


/* Extract escaped data from a MULTTY handle, and place
 * it in the given buffer.  The return value is the
 * number of bytes actually retrieved.  The size of the
 * destination is never more than the size of the source.
 *
 * The destination will not be filled with mulTTY codes
 * for stream switching or program multiplexing.  Future
 * releases will support delegation of these facilities,
 * which may lead to more variety in what is internal.
 *
 * Inasfar as control codes are used as stream-internal
 * separators they will be kept.  As a consequence, the
 * <SOH> naming before stream switching and program
 * multiplexing is kept internally, but the <SO> or <SI>
 * control codes at the end of stream switches are
 * passed when they change the stream's shift status.
 *
 * Since <SOH> naming prefixes may serve many uses, it
 * is always taken out and delivered as the beginning
 * of a separate callback operatior.  In other words,
 * this operation does not pass in <SOH> fragments,
 * unless these result from escaping, of course.
 *
 * TODO: Consider additional checking of input:
 *  - removing  NUL characters (if they were escaped)
 *  - rejecting IAC characters
 *  - rejecting DLE before unexpected codes
 *  - rejecting DLE before impossible codes
 */
int mtyunescape (uint32_t escstyle, MULTTY_INFLOW *flow,
		uint8_t *dest, int destlen);


/* Given the number of bytes to be extracted from the
 * MULTTY handle, assuming it is stable at this time.
 * This is the number of bytes that will be delivered
 * by mtyunescape() if the destlen is high enough.
 * When less is delivered, a further read will provide
 * the remaining bytes.
 */
int mtyinputsize (uint32_t escstyle, MULTTY_INFLOW *flow);


/* Given input bytes, dispatch as much of it as
 * possible between the streams of one program.
 * Program switches are not accepted by this
 * function, and lead to a buffer being returned.
 *
 * The MULTTY provided is the first in the list
 * for a given program.  This routine itself is
 * not aware of program multiplexing.
 *
 * If the input stream is blocking, then this call
 * is also blocking.
 *
 * Returns false when unprocessed program switching
 * input is waiting to be further processed, or true
 * when it could all be processed as stream input.
 */
bool mtydispatch_internal (MULTTY *current);


/* Read the input stream and dispatch its data over
 * streams which each receive their own chunks.
 * Dispatched chunks end when <SOH> is encountered,
 * as that may introduce a switch of a [program or]
 * stream.  As soon as a program switch is found,
 * the dispatcher also ends.
 *
 * This is a simple wrapper around mtydispatch that
 * rejects input with program multiplexing commands.
 *
 * If the input stream is blocking, then this call
 * is also blocking.
 */
void mtydispatch_streams (MULTTY *current);


/* Read the input stream and dispatch data across
 * programs and their streams.  Initially, the data
 * is supplied to the current program; but if its
 * dispatcher runs into a program switch it returns
 * for further processing in another program.
 *
 * This is an iterative wrapper around mtydispatch
 * that relays input with program multiplexing commands.
 *
 * If the input stream is blocking, then this call
 * is also blocking.
 */
void mtydispatch_programs (void);



/********** FUNCTIONS FOR PROGRAM MULTIPLEXING **********/



/* Drop a program in the program set, silently
 * ignoring if it is absent.  The id is how it
 * is located, the with_descr option indicates
 * if a description should be attached, as that
 * differentiates the name.
 */
void mtyp_drop (MULTTY_PROGSET *progset, MULTTY_PROG *prog);


/* Find a program in the program set, based on
 * its 33-character name with optionally included
 * <US> attachment for programs with a description.
 * When the name is empty, prog0 is returned.
 */
MULTTY_PROG *mtyp_find (MULTTY_PROGSET *progset, const MULTTY_PROGID id_us, MULTTY_PROG *prog0);


/* Have a program in the program set, silently
 * sharing if it already exists.  The id is how it
 * is located, the opt_descr is for human display
 * purposes and may be later updated if it is
 * provided here.  Whether or not a description
 * was added is part of the program identity.
 *
 * Returns a handle on success, or else NULL/errno.
 */
MULTTY_PROG *mtyp_have (MULTTY_PROGSET *progset, const char *id, const char *opt_descr);


/* Describe a program with a new string.  This
 * includes a test whether the string contains
 * only passable ASCII content, so no control codes
 * that could confuse mulTTY.
 *
 * Note that setting a description is only welcome
 * if it was opened to have one.
 *
 * Returns true on success, or else false/errno.
 */
bool mtyp_describe (MULTTY_PROG *prog, const char *descr);


/* Send raw data for mulTTY program multiplexing.  This stands above
 * the streams for individual programs.  STREAMS SHOULD NOT USE THIS
 * BUT mtywrite() TO SEND BINARY DATA.
 *
 * This is not a user command, it is intended for mulTTY internals.
 *
 * The content supplied is sent atomically, which means it should
 * not exceed PIPE_BUF.  If it does, errno is set to EMSGSIZE.
 *
 * The raw content is supplied as a sequence of pointer and length:
 *  - uint8_t *buf
 *  - int      len
 * The number of these pairs is given by the numbufs parameter.
 *
 * This returns true on success, or else false/errno.
 */
bool mtyp_raw (int numbufs, ...);


/* Switch to another program, and send the corresponding control code
 * over stdout.  The identity can be constructed with mtyp_mkid()
 * and hints at an optional description.
 *
 * It is assumed that the program switched to exists.
 *
 * TODO: Streams may need to claim until they release.
 *
 * Return 0 on success or else -1/errno.
 */
int mtyp_switch (MULTTY_PROG *prog);



/********** FUNCTIONS FOR GENERAL USE **********/



/* INTERNAL ROUTINE for sending literaly bytes from an iovec
 * array to stdout.  This is used after composing a complete
 * structure intended to be sent.
 *
 * The primary function here is to send atomically, to
 * avoid one structure getting intertwined with another.
 * This is necessary for security and general correctness.
 * This requirement imposes a PIPE_BUF as maximum for len.
 *
 * Stream output should be sent such that it returns to
 * the default stream within the atomic unit, which is
 * established through a file buffering scheme.
 *
 * Returns true on succes, or false/errno.  Specifically
 * note EMSGSIZE, which is returned when the message is
 * too large (the limit is PIPE_BUF).  This might occur
 * when writing "<SOH>id<US>very_long_description<XXX>"
 * or similar constructs that user input inside an atom.
 * It may then be possible to send "<SOH>id<US><XXX>".
 */
bool mtyv_out (int len, int ioc, const struct iovec *iov);


/* INTERNAL ROUTINE for dispatching input read events by
 * reading additional input and distributing as much as
 * possible over flows.
 *
 * Program control is only processed when an additional
 * callback is provided to handle specifically those.
 * It is normally set to call mtyprog_vin_cb().
 */
void mtyv_in (MULTTY_INFLOW *flow, mtycb_ready *opt_mtyplex_cb, void *cbdata);


#endif /* ARPA2_MULTTY_H */
