/* mulTTY -> gotty.c -- Do a command, multiplex its streams.
 *
 * This runs a command that was written for plain stdin, stdout
 * and stderr streams.  It merges stdout and stderr, but does
 * this by switching between their streams.
 *
 * A very coarse approximation of "gotty -- cmd" is "cmd 2>&1"
 * which differs, because it does not produce output such that
 * it shifts between the streams.
 *
 * TODO:
 * This program is meant to grow into a fullblown mulTTY wrapper
 * around a single command.  It should at some point include a
 * stdctl channel for standard operations that pause, resume,
 * start and stop the program and, possibly, debug it remotely.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>

#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <pty.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>

#include <arpa2/multty.h>



/* Simple redefinitions for stdout and stderr,
 * meant to be there at any time, with simple
 * wrapper macros to write text or data to them.
 *
 * TODO: Use <SI>/<SO> with possible <SOH>stderr
 */
int newout = 1;
int newerr = 2;

#define bufout(s,l) write(newout, (s), (l))
#define buferr(s,l) write(newerr, (s), (l))

#define txtout(s) bufout((s), strlen ((s)))
#define txterr(s) buferr((s), strlen ((s)))


#define BUFLEN 1024


/* Combine the child's stdout and stderr streams on newout.
 * We only use stderr to report our own errors.
 * Returns only non-zero on success.
 */
int combine_streams (int subout, int suberr, multty_t *stream_out, multty_t *stream_err) {
	int ok = 1;
	int level = 1;
	int maxfd = (subout > suberr) ? subout : suberr;
	fd_set both;
	int more = 1;
	char buf [BUFLEN];
	ssize_t gotlen;
	while ((subout >= 0) || (suberr >= 0)) {
		FD_ZERO (&both);
		if (subout >= 0) FD_SET (subout, &both);
		if (suberr >= 0) FD_SET (suberr, &both);
		if (select (maxfd+1, &both, NULL, NULL, NULL) < 0) {
			txterr ("Failed to select stdout/stderr");
			exit (1);
		}
		if ((subout >= 0) && FD_ISSET (subout, &both)) {
			gotlen = read (subout, buf, BUFLEN);
			if ((gotlen < 0) && (errno == EIO) && (isatty (subout))) {
				/* correct for loud pty breakdown */
				gotlen = 0;
			}
			if (gotlen < 0) {
				txterr ("Error reading from child stdout\n");
				ok = 0;
				subout = -1;
			} else if (gotlen == 0) {
				subout = -1;
			} else {
				if (!mtyputstrbuf (stream_out, buf, gotlen)) {
					txterr ("Unable to pass child stdout\n");
					ok = 0;
					subout = -1;
				}
				fflush (stdout);
			}
		}
		if ((suberr >= 0) && FD_ISSET (suberr, &both)) {
			gotlen = read (suberr, buf, BUFLEN);
			if (gotlen < 0) {
				txterr ("Error reading from child stderr\n");
				ok = 0;
				suberr = -1;
			} else if (gotlen == 0) {
				suberr = -1;
			} else {
				if (!mtyputstrbuf (stream_err, buf, gotlen)) {
					txterr ("Unable to pass child stderr\n");
					ok = 0;
					suberr = -1;
				}
				fflush (stdout);
			}
		}
	}
	return ok;
}


/* The main routine runs a child process with
 * reorganised stdout and stderr handles.  It
 * passes stdin without change.
 */
int main (int argc, char *argv []) {
	int ok = 1;
	//
	// Parse commandline arguments
	multty_t *stream_out = multty_stdout;
	multty_t *stream_err = multty_stderr;
	int opt;
	bool help = false;
	bool error = false;
	bool pty_force = false;
	bool pty_enabled = true;
	bool readonly_mode = false;
	char *socket_dirname = NULL;
	char *program_name = NULL;
	while ((opt = getopt (argc, argv, "htTS:p:r")) != -1) {
		switch (opt) {
		case 't':
			pty_force = true;
			break;
		case 'T':
			pty_enabled = false;
			break;
		case 'p':
			program_name = optarg;
			break;
		case 'S':
			socket_dirname = optarg;
			break;
		case 'r':
			readonly_mode = true;
			break;
		default:
			error = true;
			/* continue into 'h' */
		case 'h':
			help = true;
			break;
		}
	}
	int argi = optind;
	if ((argi > argc) && (strcmp (argv [argi], "--") == 0)) {
		argi++;
	}
	if (argc - argi < 1) {
		error = error || !help;
	}
	if (pty_force && !pty_enabled) {
		txterr ("You cannot force and disable pseudo-terminals with -t and -T\n");
		error = true;
	}
	if (socket_dirname != NULL) {
		if (program_name == NULL) {
			txterr ("You should specify a program name with -p to benefit from -S\n");
			error = true;
		}
	} else if (program_name != NULL) {
		socket_dirname = "/var/run/mulTTY";
	}
	if (help || error) {
		txterr ("Usage: gotty [-t|-T|-p NAME|-S SOCKETDIR|-h]... [--] cmd [args...]\n");
		exit (error ? 1 : 0);
	}
	//
	// Setup a mulTTY flow
	struct multtyflow mufl;
	multtyflow_init (&mufl, false);
	//
	// Optionally bypass stdin/stdout to a UNIX domain socket
	int socket_lockfd = -1;
	if (program_name != NULL) {
		//
		// Determine socket file names. longest first
		char gotty_sockname [PATH_MAX+5];
		char gotty_lockname [PATH_MAX+5];
		char netty_sockname [PATH_MAX+5];
		int slen = snprintf (gotty_sockname, PATH_MAX+2, "%s/%s.client", socket_dirname, program_name);
		if (slen > PATH_MAX) {
			txterr ("Excessive file name length from -S and -n\n");
			exit (1);
		}
		snprintf (gotty_lockname, PATH_MAX+2, "%s/%s.lock", socket_dirname, program_name);
		snprintf (netty_sockname, PATH_MAX+2, "%s/netty.service", socket_dirname);
		//
		// Lock my own socket name for as long as this program is active
		mkdir (socket_dirname, 0775 | S_ISVTX);
		int socket_lockfd = open (gotty_lockname, O_RDONLY | O_CREAT, 0600);
		if (socket_lockfd < 0) {
			txterr ("Failed to open/create socket lock file\n");
			exit (1);
		}
		if (flock (socket_lockfd, LOCK_EX | LOCK_NB) != 0) {
			txterr ("Another process locks the socket with my name\n");
			close (socket_lockfd);
			exit (1);
		}
		//
		// Bind to the socket; we locked it and may remove an older one
		unlink (gotty_sockname);
		struct sockaddr_un gotty_sockaddr;
		struct sockaddr_un netty_sockaddr;
		memset (&gotty_sockaddr, 0, sizeof (gotty_sockaddr));
		memset (&netty_sockaddr, 0, sizeof (netty_sockaddr));
		gotty_sockaddr.sun_family = AF_UNIX;
		netty_sockaddr.sun_family = AF_UNIX;
		strncpy (gotty_sockaddr.sun_path, gotty_sockname, sizeof (gotty_sockaddr.sun_path));
		strncpy (netty_sockaddr.sun_path, netty_sockname, sizeof (netty_sockaddr.sun_path));
		int socket_stream = socket (AF_UNIX, SOCK_STREAM, 0);
		if (socket_stream < 0) {
			txterr ("Failed to allocate a socket\n");
			exit (1);
		}
		if (bind     (socket_stream, (struct sockaddr *) &gotty_sockaddr, sizeof (gotty_sockaddr)) < 0) {
			txterr ("Failed to bind to the gotty socket for the program name\n");
			exit (1);
		}
fprintf (stderr, "DEBUG: Connecting to netty at %s==%s\n", netty_sockname, netty_sockaddr.sun_path);
		if (connect (socket_stream, (struct sockaddr *) &netty_sockaddr, sizeof (netty_sockaddr)) < 0) {
			txterr ("Failed to connect to the netty socket name\n");
			exit (1);
		}
		//
		// Now replace stdin and stdout with the socket link to netty
		// but leave stderr be, since it is only for use by gotty itself.
		if ((dup2 (socket_stream, 0) < 0) || (dup2 (socket_stream, 1) < 0)) {
			txterr ("Failed to redirect to the stream socket\n");
			exit (1);
		}
		//
		// Do not claim the original socket_stream handle
		close (socket_stream);
	}
	//
	// For readonly_mode, close the stdin channel
	if (readonly_mode) {
		int null = open ("/dev/null", O_RDONLY);
		if (null < 0) {
			txterr ("Failed to redirect input from /dev/null\n");
			exit (1);
		}
		if (dup2 (null, 0) < 0) {
			txterr ("Failed to replace input with /dev/null\n");
			exit (1);
		}
		close (null);
	}
	//
	// Construct redirection pipes
	int pipout [2];
	int piperr [2];
	int tmpout = dup (1);
	int tmperr = dup (2);
	if ((tmpout < 0) || (tmperr < 0)) {
		txterr ("Failed to create replicas for stdout/stderr\n");
		exit (1);
	}
	newout = tmpout;
	newerr = tmperr;
	int failed_pipout;
	bool pty_used = (pty_force || (pty_enabled && isatty (1)));
	if (pty_used) {
		// Use a pty for stdout, but not stderr; master will be us
		struct termios tio;
		struct winsize wsz;
		failed_pipout = openpty (pipout+0, pipout+1, NULL,
				(ioctl (1, TCGETS,     &tio) == 0) ? &tio : NULL,
				(ioctl (1, TIOCGWINSZ, &wsz) == 0) ? &wsz : NULL);
	} else {
		failed_pipout = pipe (pipout);
	}
printf ("failed_pipout=%d with pty_used=%d\n", failed_pipout, pty_used);
	if (failed_pipout || (dup2 (pipout [1], 1) != 1)) {
		txterr ("Failed to wrap stdout file\n");
		exit (1);
	}
	if (pipe (piperr) || (dup2 (piperr [1], 2) != 2)) {
		txterr ("Failed to wrap stderr file\n");
		exit (1);
	}
	close (pipout [1]);
	close (piperr [1]);
	//
	// Start a child process with the right piping
	pid_t child = fork ();
	switch (child) {
	case -1:
		/* Failure */
		txterr ("Failed to fork a child process\n");
		exit (1);
	case 0:
		/* Child */
		close (pipout [0]);
		close (piperr [0]);
		close (newout);
		close (newerr);
		if (socket_lockfd >= 0) {
			close (socket_lockfd);
		}
		execvp (argv [argi], argv+argi);
		txterr ("Child process failed to run the command\n");
		exit (1);
	default:
		/* Parent */
		close (0);
		dup2 (newout, 1);
		dup2 (newerr, 2);
		close (newout);
		close (newerr);
		newout = 1;
		newerr = 2;
	}
	//
	// Multiplex the messages sent to stdout and stderr
	// Process optional stream names given with -o and -e
	ok = ok && combine_streams (pipout [0], piperr [0], stream_out, stream_err);
	// Wait for the child to finish, then wrapup
	int status;
	waitpid (child, &status, 0);
#if 0
	if (stream_out != multty_stdout) {
		mtyclose (stream_out);
		stream_out = NULL;
	}
	if (stream_err != multty_stderr) {
		mtyclose (stream_err);
		stream_err = NULL;
	}
#endif
	multtyflow_fini (&mufl);
	exit (ok ? 0 : 1);
}

