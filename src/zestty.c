/* zestty.c -- Send or receive Z-MODEM files with the sz / rz commands.
 *
 * This uses a mulTTY stream named "zmodem" for Z-MODEM file transfer.
 *
 * The Z-MODEM protocol is old, but the use case of passing files along
 * a command session is as powerful as ever.  Any mulTTY streams with
 * registered names can trigger a helper program in a client, such as
 * one to send or receive a file.  The Z-MODEM protocol is the carrier
 * over this dedicated ASCII flow.
 *
 * On the remote end, the command "sz" or "rz" is replaced with the
 * "zestty" command which acts accordingly to send or receive files.
 * On the local end, a helper can pickup and respond.
 *
 * When called without arguments, zestty receives like "rz -beZ".
 * When called with file names, zestty sends like "sz -beEknZ".
 * Options such as quiet/verbose/protect/rename may also carry over.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>


#include <arpa2/multty.h>


#define SZ_NAME "sz"
#define RZ_NAME "rz"

#define SZ_OPTS "-beEknZ"
#define RZ_OPTS "-beZ"



int main (int argc, char *argv []) {
	//
	// Process arguments
	uint32_t flags = 0;
	bool send_mode = (argc < 2);
	//
	// Construct the underlying command
	int retval;
	int sub_argc = argc+2;
	const char *sub_argv [sub_argc+1];
	if (send_mode) {
		sub_argv [0] = SZ_NAME;
		sub_argv [1] = SZ_OPTS;
	} else {
		sub_argv [0] = RZ_NAME;
		sub_argv [1] = RZ_OPTS;
	}
	memcpy (sub_argv + 2, argv, argc);
	sub_argv [sub_argc] = NULL;
	//
	// Start the subcommand, to be fed/forwarded via mulTTY
	...start.subcmd... (sub_argc, sub_argv);
	//
	// Feed and forward the subcommand via mulTTY
	...much.like.gotty...
	//
	// Return final code
	return retval;
}
