/* ascii2ctrl.c -- Write <controls> to represent ASCII control codes.
 *
 * This program helps to map the binary form of a mulTTY stream
 * to ASCII annotations like <LF> and even <NUL> or <DC3>.  Input
 * is read from stdin, output is printed on stdout.
 *
 * This program is part of mulTTY, but may be generally useful.
 * The control names in mulTTY match those in plain ASCII, plus
 *  - <IAC> for 0xff, the Telnet escape character
 *  - <LT>  for 0x3c or '<'
 *  - <GT>  for 0x3e or '>'
 * If need be, we might also consider hexadecimal notation.
 * There could only confusion for <FF> which has an alias
 * as <IAC> and <ff> in lowercase may be interpreted as hex.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa2/multty.h>



static const char *ctrlname [256] = {
	/* 0X */
	"<NUL>", "<SOH>", "<STX>", "<ETX>",
	"<EOT>", "<ENQ>", "<ACK>", "<BEL>",
	"<BS>",  "<HT>",  "<LF>",  "<VT>",
	"<FF>",  "<CR>",  "<SO>",  "<SI>",
	/* 1X */
	"<DLE>", "<DC1>", "<DC2>", "<DC3>",
	"<DC4>", "<NAK>", "<SYN>", "<ETB>",
	"<CAN>", "<EM>", "<SUB>", "<ESC>",
	"<FS>", "<GS>", "<RS>", "<US>",
	/* 2X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 3X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	"<LT>", NULL, "<GT>", NULL,
	/* 4X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 5X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 6X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 7X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, "<DEL>",
	/* 8X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 9X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* aX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* bX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* cX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* dX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* eX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* fX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, "<IAC>"
};


enum ansimode {
	ansi_plain = 0,
	ansi_invert,
	ansi_bold
};



/* Insert ASCII specials <XXX> from the text by overwriting the buffer.
 * Special forms <GT> and <LT> are used for '<' and '>' literal characters.
 * Return the number of characters in the total string, allowing binaries.
 * The text is written from the end down, to accommodate the expansion
 * as a result of the longer ASCII characters.
 */
unsigned insert_controls (char *inbuf, unsigned buflen, unsigned inlen, enum ansimode ansimode) {
	unsigned outofs = buflen;
	assert (buflen >= 5 * inlen);
	const char *pre = "";
	const char *post = "\x1b[0m";
	switch (ansimode) {
	case ansi_plain:
		post = "";
		break;
	case ansi_bold:
		pre = "\x1b[1m";
		break;
	case ansi_invert:
		pre = "\x1b[7m";
		break;
	}
	unsigned prelen = strlen (pre);
	unsigned postlen = strlen (post);
	while (inlen-- > 0) {
		char inchar = inbuf [inlen];
		const char *ctrlstr = ctrlname [inchar];
		if (ctrlstr) {
			unsigned ctrllen = strlen (ctrlstr);
			outofs -= prelen + ctrllen + postlen;
			memcpy (inbuf + outofs,                    pre,     prelen );
			memcpy (inbuf + outofs + prelen,           ctrlstr, ctrllen);
			memcpy (inbuf + outofs + prelen + ctrllen, post,    postlen);
		} else {
			inbuf [--outofs] = inchar;
		}
	}
	return (buflen - outofs);
}


int main (int argc, char *argv []) {
	bool bad = false;
	bool help = false;
	char *teefilename = NULL;
	enum ansimode ansimode = isatty (1) ? ansi_bold : ansi_plain;
	//
	// Parse arguments
	int c;
	while (c = getopt (argc, argv, "hclpibt:"), c != -1) {
		switch (c) {
		case 'c':
			/* Suppress <CR> in the output */
			ctrlname [13] = NULL;
			break;
		case 'l':
			/* Suppress <LF> in the output */
			ctrlname [10] = NULL;
			break;
		case 'p':
			/* Plain control characters */
			ansimode = ansi_plain;
			break;
		case 'i':
			/* Invert control characters */
			ansimode = ansi_invert;
			break;
		case 'b':
			/* Bold control characters */
			ansimode = ansi_bold;
			break;
		case 't':
			/* Tee mode */
			teefilename = optarg;
			break;
		case 'h':
			/* Print help text */
			help = true;
			break;
		default:
			bad = true;
			break;
		}
	}
	if (optind < argc) {
		bad = true;
	}
	if (bad || help) {
		fprintf (stderr, "Usage:   ...source... | %s [-c] [-l] [-p|-b|-i] [-t TEEFILE]\n"
			"Purpose: Represent ASCII control codes in a readable form\n",
			argv [0]);
		exit (bad ? 1 : 0);
	}
	//
	// Open the "tee" filename, if given
	int teefd = -1;
	if (teefilename != NULL) {
		teefd = open (teefilename, O_WRONLY | O_TRUNC | O_CREAT);
		if (teefd < 0) {
			perror ("Failed to open -t file");
			exit (1);
		}
	}
	//
	// Read from stdin, represent controls, and write to stdout
	unsigned bufdiv = 0;
	switch (ansimode) {
	case ansi_bold:
	case ansi_invert:
		bufdiv = 4 + 5 + 5;
		break;
	case ansi_plain:
		bufdiv = 5;
		break;
	}
	char buf [1024 * bufdiv];
	ssize_t rdlen;
	//
	// Read chunks of input, attached to an oldlen remaining from the last run
	while (rdlen = read (0, buf, sizeof (buf) / bufdiv), rdlen > 0)  {
		ssize_t wrlen;
		//
		// In tee mode we pass unmodified while tee'ing the readable controls
		if (teefd >= 0) {
			wrlen = write (1, buf, rdlen);
			if (wrlen < 0) {
				perror ("Failed to pass through");
			} else if (wrlen != rdlen) {
				fprintf (stderr, "Passed %d out of %d\n", wrlen, rdlen);
				exit (1);
			}
		}
		//
		// Insert controls at the end of the buffer
		unsigned binlen = insert_controls (buf, sizeof (buf), rdlen, ansimode);
		//
		// Write the output (which may be up to 5x as long
		wrlen = write ((teefd >= 0) ? teefd : 1, buf + sizeof (buf) - binlen, binlen);
		if (wrlen < 0) {
			perror ("Failed to write");
		} else if (wrlen != binlen) {
			fprintf (stderr, "Written %d out of %d\n", wrlen, binlen);
			exit (1);
		}
		//
		// Cycle around, using oldlen as it should
	}
	//
	// Finaly checks on read error or, if read ended properly, trailing oldlen
	if (rdlen < 0) {
		perror ("Error reading");
		exit (1);
	}
	//
	// We are done
	if (teefd >= 0) {
		close (teefd);
	}
	return 0;
}

