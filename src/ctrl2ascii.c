/* ctrl2ascii.c -- Parse <controls> and write as ASCII control codes.
 *
 * This program helps to map ASCII with annotations like <LF> and
 * even <NUL> into binary form.  Input may be on the commandline,
 * or other wise it will be taken from standard input.  Output is
 * always on stdout.
 *
 * This program is part of mulTTY, but may be generally useful.
 * The control names in mulTTY match those in plain ASCII, plus
 *  - <IAC> for 0xff, the Telnet escape character
 *  - <LT>  for 0x3c or '<'
 *  - <GT>  for 0x3e or '>'
 * If need be, we might also consider hexadecimal notation.
 * There could only confusion for <FF> which has an alias
 * as <IAC> and <ff> in lowercase may be interpreted as hex.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/multty.h>



static const char *ctrlname [256] = {
	/* 0X */
	"<NUL>", "<SOH>", "<STX>", "<ETX>",
	"<EOT>", "<ENQ>", "<ACK>", "<BEL>",
	"<BS>",  "<HT>",  "<LF>",  "<VT>",
	"<FF>",  "<CR>",  "<SO>",  "<SI>",
	/* 1X */
	"<DLE>", "<DC1>", "<DC2>", "<DC3>",
	"<DC4>", "<NAK>", "<SYN>", "<ETB>",
	"<CAN>", "<EM>", "<SUB>", "<ESC>",
	"<FS>", "<GS>", "<RS>", "<US>",
	/* 2X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 3X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	"<LT>", NULL, "<GT>", NULL,
	/* 4X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 5X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 6X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 7X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, "<DEL>",
	/* 8X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* 9X */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* aX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* bX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* cX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* dX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* eX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	/* fX */
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, "<IAC>"
};



/* Remove ASCII specials <XXX> from the text by overwriting it in place.
 * Special forms <GT> and <LT> are used for '<' and '>' literal characters.
 * Return the number of characters in the total string, allowing binaries.
 */
unsigned remove_controls (char *instr, unsigned inlen) {
	char *outstr = instr;
	unsigned retlen = 0;
	char *inend = instr + inlen;
	while (instr < inend) {
		if (*instr != '<') {
			outstr [retlen++] = *instr++;
		} else {
			char *end = memchr (instr, '>', (size_t) (inend - instr));
			if (end == NULL) {
				fprintf (stderr, "ASCII did not close <...> from \"%s\"\n", instr);
				exit (1);
			}
			unsigned ctrllen = end - instr + 1;
			int c;
			for (c = 0x00; c <= 0xff; c++) {
				if (ctrlname [c] != NULL) {
					if (strncasecmp (ctrlname [c], instr, ctrllen) == 0) {
						outstr [retlen++] = (char) c;
						instr += ctrllen;
						break;
					}
				}
			}
			if (c > 0xff) {
				fprintf (stderr, "Unrecognised control %.*s\n", ctrllen, instr);
				exit (1);
			}
		}
	}
	return retlen;
}


int main (int argc, char *argv []) {
	//
	// Parse arguments (mostly skip "--")
	int argi = 1;
	if ((argi < argc) && (strcmp (argv [argi], "--") == 0)) {
		argi++;
	}
	//
	// Process input, one chunk at a time
	while (argi < argc) {
		unsigned outlen = remove_controls (argv [argi], strlen (argv [argi]));
		ssize_t wrlen = write (1, argv [argi], outlen);
		if (wrlen < 0) {
			perror ("Failed to write");
			exit (1);
		} else if (wrlen != outlen) {
			fprintf (stderr, "Written %d out of %d for \"%.*s\"\n", wrlen, outlen, outlen, argv [argi]);
			exit (1);
		}
		argi++;
	}
	//
	// Without arguments, we have not done anything, and use stdin
	if (argc < 2) {
		char buf [1024];
		unsigned oldlen = 0;
		ssize_t rdlen;
		//
		// Read chunks of input, attached to an oldlen remaining from the last run
		while (rdlen = read (0, buf + oldlen, sizeof (buf) - oldlen), rdlen > 0)  {
			//
			// Include the oldlen bytes from the previous round
			rdlen += oldlen;
			//
			// Find if bytes should carry to the next round
			oldlen = 0;
			while (true) {
				if (buf [rdlen - oldlen] == '>') {
					oldlen = 0;
					break;
				}
				if (buf [rdlen - oldlen] == '<') {
					break;
				}
				if (++oldlen == rdlen) {
					oldlen = 0;
					break;
				}
			}
			rdlen -= oldlen;
			//
			// Remove controls and output the result
			unsigned binlen = remove_controls (buf, rdlen);
			//
			// Write the output
			ssize_t wrlen = write (1, buf, binlen);
			if (wrlen < 0) {
				perror ("Failed to write");
			} else if (wrlen != binlen) {
			fprintf (stderr, "Written %d out of %d\n", wrlen, binlen);
				exit (1);
			}
			//
			// Move the old bits to the start of the buffer
			if (oldlen > 0) {
				memcpy (buf, buf + rdlen, oldlen);
			}
			//
			// Cycle around, using oldlen as it should
		}
		//
		// Finaly checks on read error or, if read ended properly, trailing oldlen
		if (rdlen < 0) {
			perror ("Error reading");
			exit (1);
		} else if (oldlen > 0) {
			fprintf (stderr, "Escape at the end was not closed\n");
			exit (1);
		}
	}
	//
	// We are done
	return 0;
}

