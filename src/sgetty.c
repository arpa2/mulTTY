/* SASL-based getty, using mulTTY stream coding.
 *
 * This is an alternative to unformatted text logins, which end up imposing
 * password disciplines.  Terminals are too important to not protect better.
 * This is the server side.
 *
 * The exchange starts with a human-readable instruction announcing the SASL
 * login, and then starts the ASCII-coded exchange.  The client is expected
 * to handle this properly.  The hope is to show the various getty programs
 * an alternative that they may want to integrate.
 *
 * The backend to this program is not a password file, but a Quick-DiaSASL
 * connection to a server that may support Realm Crossover for SASL.  This
 * means that account management is externalised.
 *
 * After success with SASL, this program starts /bin/login with accepted
 * username of the form user@domain.name.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>

#include <ev.h>

#include <arpa2/multtysasl.h>
#include <arpa2/socket.h>
#include <arpa2/except.h>


#if LOG_STYLE == LOG_STYLE_STDOUT
#error "LOG_STYLE_STDOUT causes sgetty(8) bugs.  Please consider LOG_STYLE_STDERR."
#endif



/* Timeout handler.
 */
static void timer_cb (EV_P_ ev_timer *tout, int revents) {
	ev_break (EV_A_ EVBREAK_ALL);
}


/* General signal handler.
 */
static void signal_cb (EV_P_ ev_signal *sig, int revents) {
	ev_break (EV_A_ EVBREAK_ALL);
}


/* Client input handler.
 */
static void clientinput_cb (EV_P_ ev_io *io, int revents) {
	char buf [2048];
	ssize_t rdlen = read (0, buf, sizeof (buf));
	fprintf (stderr, "Input offered %zd bytes to be processed...\n", rdlen);
}


/* Quick-DiaSASL backend response handler.
 */
static void quickdiasasl_cb (EV_P_ ev_io *io, int revents) {
	struct diasasl_node *dianode = (struct diasasl_node *) io->data;
	int32_t com_errno = diasasl_process (dianode, false);
	if (com_errno != 0) {
		diasasl_break (dianode, com_errno);
		ev_break (EV_DEFAULT, EVBREAK_ALL);
	}
}


/* Print a SASL string to the output.  We have setup the stream
 * with "<SOH>sasl<SO>" by the time we get here, and we have no
 * need to switch back until the login is through, so this is a
 * trivial output statement with immediate flush.
 */
bool send2terminal_cb (struct multtysasl_server *this, const char *multtysasl_string) {
	fputs (multtysasl_string, stdout);
	fflush (stdout);
	return true;
}


/* The SASL exchange has reached a final state, so the mulTTY SASL
 * process can be stopped.
 */
void finalstate_cb (struct multtysasl_server *this) {
	ev_break (EV_DEFAULT, EVBREAK_ALL);
}


/* Run the mulTTY SASL login procedure.
 *
 * Start:
 *  - Clear success/reject outputs.  Clear environment variables.
 *  - Setup a timeout and various signal handlers.
 *  - Connect to the Quick-DiaSASL backend node.
 *  - Switch the output to the "sasl" stream.
 *  - Start the mulTTY SASL exchange.
 *  - Redirect input   to the mulTTY SASL exchange.
 *  - Redirect backend to the mulTTY SASL exchange.
 *
 * Processing:
 *  - Redirect input to parsing.  Handle "sasl" and drop the rest.
 *  - Forward "sasl" output to the terminal.
 *  - Bail out on success, rejection, errors, timeout, signals.
 *
 * Stop:
 *  - Switch off backend handling.
 *  - Return to normal input handling.
 *  - Harvest data from the mulTTY SASL exchange.
 *  - Stop the mulTTY SASL exchange.
 *  - Return to the "stdout" stream.
 *  - Disconnect the Quick-DiaSASL backend node.
 *  - Cleanup event handlers.
 *
 * Return:
 *  - Return success (true) when there was no processing error.
 *  - Setup envvars after authentication success.
 *  - Report output booleans for login success/reject.
 */
bool multtysasl_login (float login_timeout, bool *success, bool *failure) {
	bool ok = true;

	//
	/////   START   /////
	//
	// Clear success/reject outputs
	*success = false;
	*failure = false;
	//
	// Clear environment variables
	unsetenv ("SASL_SECURE");
	unsetenv ("SASL_REALM");
	unsetenv ("SASL_MECH");
	unsetenv ("REMOTE_USER");
	//
	// Setup a timeout and various signal handlers
	struct ev_loop *loop = EV_DEFAULT;
	ev_timer login_timer;
	ev_timer_init (&login_timer, timer_cb, login_timeout, 0.);
	ev_timer_start (loop, &login_timer);
	ev_signal sigalrm;
	ev_signal sighup;
	ev_signal sigint;
	ev_signal_init (&sigalrm, signal_cb, SIGALRM);
	ev_signal_init (&sighup , signal_cb, SIGHUP );
	ev_signal_init (&sigint , signal_cb, SIGINT );
	ev_signal_start (loop, &sigalrm);
	ev_signal_start (loop, &sighup );
	ev_signal_start (loop, &sigint );
	//
	// Connect to the Quick-DiaSASL backend node
	struct sockaddr_storage nodename;
	struct diasasl_node dianode;
	memset (&dianode, 0, sizeof (struct diasasl_node));
	diasasl_node_open (&dianode);
char *diasasl_ip = "::1";	//TODO//FIXEDVALUE//
char *diasasl_port = "3000";	//TODO//FIXEDVALUE//
	if (ok && !socket_parse (diasasl_ip, diasasl_port, &nodename)) {
		log_error ("Syntax error in IP address %s and/or TCP port %s for Quick-DiaSASL backend node",
				diasasl_ip, diasasl_port);
		ok = false;
	}
	if (ok && !socket_client (&nodename, SOCK_STREAM, &dianode.socket)) {
		log_error ("Failed to connect to Quick-DiaSASL backend node at IP %s and TCP port %s",
				diasasl_ip, diasasl_port);
		ok = false;
	}
	if (ok && !socket_nonblock (dianode.socket)) {
		log_warning ("Failed to switch Quick-DiaSASL backend node to non-blocking mode");
	}
	//
	// Return early if there were problems
	if (!ok) {
		return ok;
	}
	//
	// Switch the output to the "sasl" stream
	fputs ("Login... kiekeboe!\n", stdout);  //TODO//TESTOUTPUT//
	fflush (stdout);
	fputs (s_SOH "sasl" s_SO, stdout);
	//
	// Start the mulTTY SASL exchange
	struct multtysasl_server *srv = NULL;
	ok = ok && multtysasl_open (&srv,
		0, send2terminal_cb, finalstate_cb,
		&dianode,
		"pixie.demo.arpa2.org",
		"pixie.demo.arpa2.org",
		"diasasl-demo",
		multtyprog_main, NULL);
	//
	// Redirect input to the mulTTY SASL exchange.
	ev_io c2s;
	ev_io_init (&c2s, clientinput_cb, 0, EV_READ);
	c2s.data = (void *) srv;
	ev_io_start (loop, &c2s);
	//
	// Redirect backend to the mulTTY SASL exchange.
	ev_io qds;
	ev_io_init (&qds, quickdiasasl_cb, dianode.socket, EV_READ);
	qds.data = (void *) &dianode;
	ev_io_start (loop, &qds);

	//
	/////   PROCESSING   /////
	//
	log_debug ("Starting event loop");
	ev_run (loop, 0);
	log_debug ("Stopping event loop");

	//
	/////   STOP   //////
	//
	// Switch off backend handling.
	ev_io_stop (loop, &qds);
	//
	// Return to normal input handling.
	ev_io_stop (loop, &c2s);
	//
	// Harvest data from the mulTTY SASL exchange
	switch (srv->state) {
	case MSS_REJECTED:
		*failure = true;
		break;
	case MSS_AUTHENTICATED:
		*success = true;
		break;
	case MSS_ERROR:
	case MSS_CLEAN:
	default:
		ok = false;
		break;
	}
	//
	// Stop the mulTTY SASL exchange
	multtysasl_close (&srv);
	ev_run (loop, EVRUN_NOWAIT);
	//
	// Return to the "stdout" output stream.
	fputs (s_SO, stdout);
	fflush (stdout);
	//
	// Disconnect the Quick-DiaSASL backend node
	//diasasl_break (&dianode, ECONNABORTED); //TODO//STUCK_in_ripple_dropwhile_SPINLOCK(next)// 
	socket_close (dianode.socket);
	dianode.socket = -1;
	diasasl_node_close (&dianode); //TODO//STUCK_in_ripple_dropwhile_SPINLOCK(next)// 
	//
	// Cleanup event handlers
	ev_signal_stop (loop, &sigalrm);
	ev_signal_stop (loop, &sighup );
	ev_signal_stop (loop, &sigint );
	ev_timer_stop  (loop, &login_timer);
	ev_loop_destroy (loop);

	//
	/////   RETURN   /////
	//
	// Report output booleans for login success/reject.
	*success = ok && *success && !*failure;
	*failure = ok && *failure;
	//
	// Setup envvars after authentication success
	if (*success) {
		setenv ("SASL_SECURE", "true", 1);
		//TODO//SASL_REALM//
		//TODO//SASL_MECH//
		//TODO//REMOTE_USER//
	}
	//
	// Return success (true) when there was no processing error.
	return ok;
}


/* After authentication, find a best-matching user account.
 * Return false if none found, or anything else went wrong.
 */
bool match_account (uid_t *uid, gid_t *gid) {
	char pwdbuf [4096];
	struct passwd pwdin, *pwd;
char *remote_user = "john@pixie.demo.arpa2.org";	//TODO//FIXED//
	memset (pwdbuf, 0, sizeof (pwdbuf));
	memset (&pwdin, 0, sizeof (pwdin ));
	pwd = NULL;
	//TODO// Iterate account names
	if (getpwnam_r (remote_user, &pwdin, pwdbuf, sizeof (pwdbuf), &pwd) == 0) {
		*uid = pwd->pw_uid;
		*gid = pwd->pw_gid;
		setenv ("USER", pwd->pw_name, 1);
		char uidstr [75];
		snprintf (uidstr, 70, "%d", pwd->pw_uid);
		setenv ("UID", uidstr, 1);
		char gidstr [75];
		snprintf (gidstr, 70, "%d", pwd->pw_gid);
		setenv ("GID", gidstr, 1);
		setenv ("HOME", pwd->pw_dir, 1);
		setenv ("SHELL", pwd->pw_shell, 1);
		return true;
	}
	return false;
}


/* After successful login, enter the user's account and start their shell.
 * We fork a shell while cloning the environment.  The parent waits until
 * the child exits.  The child sets uid, gid and homedir to run the shell.
 */
void run_shell (uid_t uid, gid_t gid) {
	fputs ("Welcome.  Sleeping 5s as a poor shell substitute (TODO)\n", stdout);
	sleep (5);
	fflush (stdout);
}


/* The main program initialises the tty and enters an infinite loop in
 * which it offers login with mulTTY SASL and enters the user account
 * and runs their shell only after successful authentication.
 */
int main (int argc, char *argv []) {
	//
	// Parse arguments
	int baudrate = 0;
	char *domainname = NULL;
	char *initstring = "";
	float login_timeout = 60.0;
	bool wait_newline = false;
	bool bad = false;
	bool help = false;
	int ch;
	while (ch = getopt (argc, argv, "hb:d:I:t:w"), ch != -1) {
		switch (ch) {
		case 'b':
			baudrate = atoi (optarg);
			break;
		case 'd':
			domainname = optarg;
			break;
		case 'I':
			initstring = optarg;
			break;
		case 't':
			login_timeout = atof (optarg);
			break;
		case 'w':
			wait_newline = true;
			break;
		case 'h':
			help = true;
			break;
		default:
			bad = true;
			break;
		}
	}
	if (argc - optind != 3) {
		bad = true;
	}
	if (bad || help) {
		fprintf (stderr, "Usage: %s [OPTIONS] DEVICE DIASASL-IP DIASASL-PORT\n",
				argv [0]);
		exit (bad ? 1 : 0);
	}
	char *device       = argv [optind+0];
	char *diasasl_ip   = argv [optind+1];
	char *diasasl_port = argv [optind+2];
	//
	// Open the serial device
	int tty = -1;
reset:
	if (tty >= 0) {
		close (tty);
		tty = -1;
	}
	tty = open (device, O_RDWR);
	if (tty < 0) {
		perror ("Failed to open tty");
		exit (1);
	}
	//
	// Setup the serial line
	if (baudrate > 0) {
		struct termios tio;
		if (tcgetattr (tty, &tio) != 0) {
			perror ("Failure getting termios data for tty");
			exit (1);
		}
		if (cfsetspeed (&tio, baudrate) != 0) {
			perror ("Failure setting the requested baudrate");
			exit (1);
		}
		if (tcsetattr (tty, TCSANOW, &tio) != 0) {
			perror ("Failure setting termios data for tty");
			exit (1);
		}
	}
	//
	// Reset the terminal
	fputs (initstring, stdout);
	fflush (stdout);
	//
	// Clear _all_ environment variables
	clearenv ();
	//
	// Enter the authentication loop
	bool login_proper = true;
	while (true) {
		//
		// Optionally wait until a CR or LF is pressed
		if (wait_newline || !login_proper) {
			fputs ("\n\nPress enter to initiate mulTTY SASL login\n", stdout);
			fflush (stdout);
			int ch;
			do {
				ch = getchar ();
				if (ch == EOF) {
					//
					// Escape, then reset the modem
					fputs ("+++", stderr);
					fflush (stdout);
					sleep (1);
					goto reset;
				}
			} while ((ch != '\r') && (ch != '\n'));
		}
		//
		// Avoid fast cycling upon failure
		login_proper = false;
		//
		// Actually run the login procedure
		bool success = false, failure = false;
		if (multtysasl_login (login_timeout, &success, &failure)) {
			login_proper = true;
		}
		//
		// Respond to success or failure
		if (failure) {
			fputs ("Access denied\n", stdout);
			fflush (stdout);
			login_proper = false;
		} else if (!success) {
			fputs ("Access undecided (if this persists, contact your admin)\n", stdout);
			fflush (stdout);
			login_proper = false;
		} else {
			uid_t uid;
			gid_t gid;
			if (match_account (&uid, &gid)) {
				run_shell (uid, gid);
				clearenv ();
			} else {
				fputs ("No account for your identity\n", stdout);
				fflush (stdout);
			}
		}
	}
	//
	// This loop does not end (but the program might, if it exits halfway)
}

