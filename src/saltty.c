/* mulTTY -> saltty.c -- handler program for SASL streams
 *
 * This runs a command that may insert a "sasl" stream, which
 * is then handled locally.  Other than this, it passes the
 * mulTTY flow.  It may add authentication to the stdin/stdout.
 *
 * One way a program can add a "sasl" stream is by relying on
 * the pam_saltty_diasasl.so PAM module for authentication.
 *
 * At some point, this program will be called from a mulTTY
 * wrapper that demultiplexes programs and flows, and passes
 * any "sasl" stream through this kind of helper.  This tool
 * only services one program, not its sub-programs.  This is
 * helpful to protect users from zealous credential probes.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <pty.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/ioctl.h>

#include <arpa2/multty.h>
#include <arpa2/except.h>

#include <arpa2/quick-sasl.h>



#define BUFLEN 1024


/* Global variables used throughout the program.
 */
int backin  = -1;
int backout = -1;
char **hosts = NULL;
char **services = NULL;


/* Loosely defined mulTTY program and streams, not completely used though.
 */

static struct multty text_c2s;
static struct multty text_s2c;
static struct multty sasl_c2s;
static struct multty sasl_s2c;

static struct multtyprog multtyprog_back = {
	.next = NULL,
	.prev = NULL,
	.parent = NULL,
	.children = NULL,
	.name = { 'b', 'a', 'c', 'k', 0 },
	.curin = &text_s2c,
	.curout = &text_c2s,
};

static struct multty text_s2c = {
	.next = &text_c2s,
	.prev = NULL,
	.flow = NULL,
	.program = &multtyprog_back,
	.name = { 't', 'e', 'x', 't', 0 },
	.description = NULL,
	.description_fresh = false,
};

static struct multty text_c2s = {
	.next = &sasl_s2c,
	.prev = &text_s2c,
	.flow = NULL,
	.program = &multtyprog_back,
	.name = { 't', 'e', 'x', 't', 0 },
	.description = NULL,
	.description_fresh = false,
};

static struct multty sasl_s2c = {
	.next = &sasl_c2s,
	.prev = NULL,
	.flow = NULL,
	.program = &multtyprog_back,
	.name = { 's', 'a', 's', 'l', 0 },
	.description = NULL,
	.description_fresh = false,
};

static struct multty sasl_c2s = {
	.next = NULL,
	.prev = &sasl_s2c,
	.flow = NULL,
	.program = &multtyprog_back,
	.name = { 's', 'a', 's', 'l', 0 },
	.description = NULL,
	.description_fresh = false,
};

void challenge_response (char siso, char *opt_token, unsigned tokenlen);


/* Print a SASL fragment into the backend's SASL inflow.
 *
 * TODO: Avoid breaking into main stream <SOH>... prefixes.
 */
#define child_inflow_vprintf_sasl(siso,fmt,...) \
	dprintf (backin, s_SOH "sasl%c" fmt "%c", siso, ##__VA_ARGS__, siso);


/* Receive SASL callbacks from the s2c "sasl" stream, and adapt the local
 * client's SASL state accordingly.
 */
char *server_hostname = NULL;
char *server_service = NULL;
QuickSASL quicksasl = NULL;
//
void sasl_s2c_textcb (multty_t *mty, multty_event_t evt,
			char *txt, unsigned len,
			bool first, bool last) {
	static char buffer_hostname [256];
	static char buffer_service  [101];
	static unsigned buflen_hostname;
	static unsigned buflen_service;
	unsigned len2;
	log_debug ("sasl_s2c_textcb event %d text \"%.*s\"", evt, len, txt);
	switch (evt) {
	case MULTTY_SASL_S2C_INITIATE_HOSTNAME:
		//
		// Setup the hostname to log into
		log_debug ("SASL initiate hostname \"%.*s\"", len, txt);
		if (first) {
			buflen_hostname = 0;
			server_hostname = NULL;
		}
		len2 = (buflen_hostname + len > 255) ? (255 - buflen_hostname) : len;
		if (len2 > 0) {
			memcpy (buffer_hostname + buflen_hostname, txt, len2);
			buflen_hostname += len2;
		}
		if (last) {
			buffer_hostname [buflen_hostname] = '\0';
		}
		if (hosts [0] != NULL) {
			char **host2check = hosts;
			while (*host2check != NULL) {
				if (strcasecmp (*host2check, buffer_hostname) == 0) {
					server_hostname = *host2check;
					break;
				}
				host2check++;
			}
		} else {
			server_hostname = buffer_hostname;
		}
		break;
	case MULTTY_SASL_S2C_INITIATE_SERVICE:
		//
		// Setup the service to log into
		log_debug ("SASL initiate service name \"%.*s\"", len, txt);
		if (first) {
			buflen_service = 0;
			server_service = NULL;
		}
		len2 = (buflen_service + len > 100) ? (100 - buflen_service) : len;
		if (len2 > 0) {
			memcpy (buffer_service + buflen_service, txt, len2);
			buflen_service += len2;
		}
		if (last) {
			buffer_service [buflen_service] = '\0';
			char **serv2check = services;
			while (*serv2check != NULL) {
				if (strcasecmp (*serv2check, buffer_service) == 0) {
					server_service = *serv2check;
					break;
				}
				serv2check++;
			}
		}
		break;
	case MULTTY_SASL_S2C_INITIATE_MECHLIST:
		//
		// Start a new SASL session
		log_debug ("SASL initiate mechanism list \"%.*s\"", len, txt);
assertxt (first && last, "Not Implemented Yet: Fragmented mechanism lists");
		if (quicksasl != NULL) {
			qsasl_close (&quicksasl);
		}
		if ((server_hostname == NULL) || (server_service == NULL)) {
			/* SASL teardown from client to server */
			log_debug ("Teardown after mechlist, server_hostname=%s, _service=%s", server_hostname, server_service);
			child_inflow_vprintf_sasl (mty->flow->siso_in, s_SYN s_CAN s_ETB);
		} else {
			log_debug ("Creating client Quick-SASL session handler");
			//TODO// Consider server_service <--> services [0]
			qsasl_client (&quicksasl, services [0], NULL, NULL, 0);
			membuf mechs;
			mechs.bufptr = txt;
			mechs.buflen = len;
			log_debug ("Setting client Quick-SASL mechlist to %.*s", mechs.buflen, mechs.bufptr);
			membuf clirealm = { .bufptr = services [0],    .buflen = strlen (services [0])    };
			membuf srvrealm = { .bufptr = server_hostname, .buflen = strlen (server_hostname) };
			qsasl_set_client_realm (quicksasl, clirealm);
			qsasl_set_server_realm (quicksasl, srvrealm);
			qsasl_set_mech (quicksasl, mechs);
			char *env;
			if (env = getenv ("SASL_CLIENTUSER_LOGIN")) {
				membuf crs_user = { .bufptr=(uint8_t *) env, .buflen=strlen (env) };
				qsasl_set_clientuser_login (quicksasl, crs_user);
			}
			if (env = getenv ("SASL_CLIENTUSER_ACL")) {
				membuf crs_user = { .bufptr=(uint8_t *) env, .buflen=strlen (env) };
				qsasl_set_clientuser_acl (quicksasl, crs_user);
			}
			challenge_response (mty->flow->siso_in, NULL, 0);
		}
		break;
	case MULTTY_SASL_S2C_TOKEN:
		//
		// Receive a *present* challenge token
		log_debug ("SASL token is present");
		log_data ("s2c", txt, len, 0);
assertxt (first && last, "Not Implemented Yet: Fragmented tokens");
		challenge_response (mty->flow->siso_in, txt, len);
		break;
	case MULTTY_SASL_C2S_TOKEN:
		//
		// Report a constructive message for client-to-server tokens
		fprintf (stderr, "Unexpected client-to-server token in \"sasl\" stream\n", evt);
		break;
	default:
		//
		// Report a constructive message for unexpected callbacks
		fprintf (stderr, "Unrecognised text event %d in \"sasl\" stream\n", evt);
		break;
	}
}
//
void sasl_s2c_eventcb (multty_t *mty, multty_event_t evt,
			bool named, bool described) {
	//
	// Setup for cleanup handling
	bool cleanup = false;
	//
	// Decide how to respond
	switch (evt) {
	case MULTTY_SASL_S2C_TEARDOWN:
		//
		// Tear down any SASL session (forget about the failure)
		log_debug ("SASL teardown");
		cleanup = true;
		break;
	case MULTTY_SASL_S2C_NO_TOKEN:
		//
		// Process an *absent* challenge token
		log_debug ("SASL token is absent");
		challenge_response (mty->flow->siso_in, NULL, 0);
		break;
	case MULTTY_SASL_SUCCESS:
		//
		// The SASL session is done (and we forget the result)
		log_debug ("SASL authentication was successful");
		cleanup = true;
		break;
	case MULTTY_SASL_FAILURE:
		//
		// The SASL session is done (and we forget the result)
		log_debug ("SASL authentication failed");
		cleanup = true;
		break;
	case MULTTY_SASL_C2S_MECHNAME:
	case MULTTY_SASL_C2S_TEARDOWN:
	case MULTTY_SASL_C2S_NO_TOKEN:
		//
		// Report a constructive message for client-to-server callbacks
		fprintf (stderr, "Unexpected client-to-server event %d in \"sasl\" stream\n", evt);
		break;
	default:
		//
		// Report a constructive message for unexpected callbacks
		fprintf (stderr, "Unrecognised event %d in \"sasl\" stream\n", evt);
		break;
	}
	//
	// Cleanup or session take-over
	if (cleanup) {
		if (quicksasl) {
			log_debug ("Destroying client Quick-SASL session handler");
			qsasl_close (&quicksasl);
		}
		server_hostname = NULL;
		server_service  = NULL;
	}
}
//
// bool sasl_s2c_badcharcb (multty_t *mty, multty_event_t evt, char *bad) {
// 	fprintf (stderr, "Ignoring bad character '%c' (0x%02x) in \"sasl\" stream\n", *bad, (int) *bad);
// 	return false;
// }


/* Pass the input of the child program, but take note of the stream name.
 * SASL traffic may be inserted at any time, but afterwards the backflow
 * must return to the same stream as before.
 */
char main2back_stream [33];
char main2back_prefix [33];
bool main2back_passit = false;
//
void child_inflow_textcb (multtyflow_t *mtyf, multty_event_t evt,
			char *txt, unsigned len,
			bool first, bool last) {
	static unsigned prefixofs = 0;
	//
	// Handle various kinds of event
	switch (evt) {
	case MULTTY_PREFIX_NAME:
		if (first) {
			prefixofs = 0;
		}
		unsigned len2 = (prefixofs + len > 32) ? 32 - prefixofs : len;
		if (len2 > 0) {
			printf ("%s%.*s", ((prefixofs == 0) && (len > 0)) ? s_SOH : "", len, txt);
			memcpy (main2back_prefix + prefixofs, txt, len2);
			prefixofs += len;
		}
		if (last) {
			main2back_prefix [prefixofs] = '\0';
		}
		break;
	case MULTTY_CONTENT:
		if (main2back_passit) {
			log_debug ("Passing mulTTY content from main to backend");
			printf ("%.*s", len, txt);
			fflush (stdout);
		}
		break;
	case MULTTY_DESCRIPTION:
		if (main2back_passit) {
			log_debug ("Passing mulTTY description from main to backend");
			printf ("%s%.*s", first ? s_US : "", len, txt);
		}
		break;
	default:
		//
		// Report a constructive message for unexpected callbacks
		fprintf (stderr, "Unrecognised text event %d in input\n", evt);
	}
}
//
void child_inflow_eventcb (multtyflow_t *mtyf, multty_event_t evt,
			bool named, bool described) {
	switch (evt) {
	case MULTTY_SHIFT_STREAM:
		//
		// Pass the <SO> or <SI> character
		printf ("%c", mtyf->siso_in);
		if (named) {
			memcpy (main2back_stream, main2back_prefix, sizeof (main2back_stream));
		} else {
			main2back_stream [0] = '\0';
		}
		main2back_passit = (strcasecmp (main2back_stream, "sasl") != 0);
		break;
	case MULTTY_END_STREAM:
		printf (s_EM);
		break;
	case MULTTY_PROGRAM_PARENT:
	case MULTTY_PROGRAM_STOP:
	case MULTTY_PROGRAM_CHILD:
	case MULTTY_PROGRAM_SIBLING:
		//
		// Report a constructive message for mulTTYplex callbacks
		fprintf (stderr, "Unexpected mulTTYplex event %d in input\n", evt);
		break;
	default:
		//
		// Report a constructive message for unexpected callbacks
		fprintf (stderr, "Unrecognised event %d in input\n", evt);
		break;
	}
}
//
bool child_inflow_badcharcb (multtyflow_t *mtyf, multty_event_t evt, char *bad) {
	fprintf (stderr, "Ignoring bad character '%c' (0x%02x) in input\n", *bad, (int) *bad);
	return false;
}


/* Process an optional challenge token and send any response.
 * If there is no current QuickSASL session, send back teardown.
 */
void challenge_response (char siso, char *opt_token, unsigned tokenlen) {
	//
	// Handle having no current QuickSASL session
	if (quicksasl == NULL) {
		goto teardown;
	}
	//
	// Make a client step
	membuf s2c = { .bufptr = opt_token, .buflen = tokenlen };
	membuf c2s;
	membuf mech;
	log_debug ("Stepping client Quick-SASL object with s2c #%d", s2c.buflen);
	log_data ("Token s2c", s2c.bufptr, s2c.buflen, 0);
	if (!qsasl_step_client (quicksasl, s2c, &mech, &c2s)) {
		goto teardown;
	}
	//
	// There may (not) be a mech, there may (not) be a mech
	log_debug ("Formatting client Quick-SASL with mulTTY; mech-is-NULL=%d, c2s-is-NULL=%d", mech.bufptr == NULL, c2s.bufptr == NULL);
	log_data ("mech", mech.bufptr, mech.buflen, 0);
	log_data ("c2s",   c2s.bufptr,  c2s.buflen, 0);
	char * mech_pre = (mech.bufptr == NULL) ? "" : s_CAN;
	char *token_pre = (c2s .bufptr == NULL) ? "" : s_US ;
	//
	// Now produce the four variants with the proper prefixes
	child_inflow_vprintf_sasl (siso,
			s_SYN "%s%.*s" "%s%.*s" s_ETB,
			mech_pre,  mech.buflen, mech.bufptr,
			token_pre, c2s .buflen, c2s .bufptr);
	//
	// We are done
	return;
	//
	// SASL teardown from client to server
teardown:
	if (quicksasl != NULL) {
		qsasl_close (&quicksasl);
	}
	child_inflow_vprintf_sasl (siso, s_SYN s_CAN s_ETB);
}


/* Split the output of the child program into the "sasl" stream and the rest.
 * Any "sasl" stream fragments are bound to be followed by <SI> or <SO>, which
 * is then to be forwarded.  While handling the "sasl" stream content, output
 * is passed into the SASL parser structure, which makes its own callbacks.
 */
bool sasl_stream = false;
bool sasl_prefix = false;
//
void child_outflow_eventcb (multtyflow_t *mtyf, multty_event_t evt,
			bool named, bool described) {
	switch (evt) {
	case MULTTY_SHIFT_STREAM:
log_debug ("named==%d and sasl_prefix==%d at line %d", named, sasl_prefix, __LINE__);
		if (!named) {
			sasl_prefix = false;
		}
		sasl_stream = sasl_prefix;
log_debug ("sasl_stream := %d at line %d", sasl_stream, __LINE__);
		if (!sasl_stream) {
			printf ("%c", mtyf->siso_in);
		}
		break;
	case MULTTY_END_STREAM:
		if (!sasl_stream) {
			printf (s_EM);
		}
		sasl_stream = false;
log_debug ("sasl_stream := %d at line %d", sasl_stream, __LINE__);
		break;
	case MULTTY_PROGRAM_PARENT:
	case MULTTY_PROGRAM_STOP:
	case MULTTY_PROGRAM_CHILD:
	case MULTTY_PROGRAM_SIBLING:
		//
		// Report a constructive message for mulTTYplex callbacks
		fprintf (stderr, "Unexpected mulTTYplex event %d in output\n", evt);
		break;
	default:
		//
		// Report a constructive message for unexpected callbacks
		fprintf (stderr, "Unrecognised event %d in output\n", evt);
		break;
	}
}
//
void child_outflow_textcb (multtyflow_t *mtyf, multty_event_t evt,
			char *txt, unsigned len,
			bool first, bool last) {
	static const char *matchtxt = "sasl";
	static unsigned matchofs = 0;
	static bool matchmay = false;
	log_debug ("child_outflow_textcb event %d", evt);
	//
	// We only handle specific kinds of text
	switch (evt) {
	case MULTTY_PREFIX_NAME:
		//
		// Discover whether the "sasl" stream name is used (perhaps broken up)
		if (first) {
			matchofs = 0;
			matchmay = true;
			sasl_stream = false;
log_debug ("sasl_stream := %d at line %d", sasl_stream, __LINE__);
			sasl_prefix = false;
log_debug ("sasl_prefix := %d at line %d", sasl_prefix, __LINE__);
		}
		matchmay = matchmay && (matchofs + len <= 4);
log_debug ("matchmay = %d at line %d after preliminarily comparing %d to 4", matchmay, __LINE__, matchofs + len);
		if (len > 0) {
			matchmay = matchmay && (strncmp (matchtxt + matchofs, txt, len) == 0);
log_debug ("matchmay = %d at line %d after comparing \"%.*s\" to \"%.*s\"", matchmay, __LINE__, len, matchtxt + matchofs, len, txt);
			if (matchmay) {
				matchofs += len;
				len = 0;
			}
		}
		if (last) {
			matchmay = matchmay && (matchofs == 4);
log_debug ("matchmay = %d at line %d after comparing %d to 4", matchmay, __LINE__, matchofs);
			sasl_prefix = matchmay;
log_debug ("sasl_prefix := %d at line %d", sasl_prefix, __LINE__);
		}
		if (!matchmay) {
			if (first || (matchofs > 0)) {
				printf (s_SOH "%.*s%.*s", matchofs, matchtxt, len, txt);
				matchofs = 0;
			}
		}
		break;
	case MULTTY_DESCRIPTION:
		//
		// Drop the description after "<SOH>sasl", which is a bit crude
		if (!sasl_prefix) {
			printf ("%s%.*s", first ? s_US : "", len, txt);
		}
		break;
	case MULTTY_CONTENT:
		//
		// Relay the content to the SASL handling or simply send it out
		if (sasl_stream) {
log_debug ("Content in SASL stream, len=%d, first=%d, last=%d", len, first, last);
log_data ("Parsing SASL", txt, len, 0);
log_debug ("Entering SASL recursion on \"%.*s\" #%d, going from flow %p with text parser %p into stream %p with text parser %p", len, txt, len, mtyf, mtyf->text_cb, &sasl_s2c, sasl_s2c.text_cb);
			mtyf->curin = &sasl_s2c;
			multtysasl_parse_cb (&sasl_s2c, evt, txt, len, first, last);
log_debug ("Leaving SASL recursion");
log_debug ("Parsed  SASL");
			mtyf->curin = &text_s2c;
		} else {
log_debug ("Content outside of SASL stream, len=%d, first=%d, last=%d", len, first, last);
log_data ("Non-SASL", txt, len, 0);
			printf ("%.*s", len, txt);
			fflush (stdout);
		}
		break;
	default:
		//
		// Report a constructive message for unexpected callbacks
		fprintf (stderr, "Unrecognised text event %d in output\n", evt);
	}
}
//
bool child_outflow_badcharcb (multtyflow_t *mtyf, multty_event_t evt, char *bad) {
	fprintf (stderr, "Ignoring bad character '%c' (0x%02x) in output\n", *bad, (int) *bad);
	return false;
}


/* Relay child's stdout to parent's stdout and parent's stdin to child's stdin.
 * One exception is made for the "sasl" flow, which is handled by passing it
 * via a Quick-SASL client process.
 */
int relay_streams (pid_t child, multtyflow_t *mainflow, multtyflow_t *backflow) {
#if NONBLOCKING_ATTEMPT
	int fdfl = fcntl (backout, F_GETFL);
	if (fdfl != -1) {
		fcntl (backout, F_SETFL, fdfl | O_NONBLOCK);
	}
#endif
	int ok = 1;
	int level = 1;
	fd_set both;
	fd_set gone;
	int more = 1;
	char buf [BUFLEN];
	ssize_t gotlen;
	while (backout >= 0) {
		FD_ZERO (&both);
		FD_ZERO (&gone);
		FD_SET (0,       &both);
		FD_SET (backout, &both);
		FD_SET (backout, &gone);
		log_debug ("salTTY is waiting for input on %d [or 0]", backout);
		if (select (backout+1, &both, NULL, &gone, NULL) < 0) {
			fprintf (stderr, "Failed to select stdin/stdout\n");
			exit (1);
		}
		if (FD_ISSET (backout, &both)) {
log_debug ("salTTY got readable event for backout %d", backout);
			gotlen = read (backout, buf, BUFLEN);
log_debug ("salTTY backout, gotlen=%d", gotlen);
			if (gotlen < 0) {
				if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
log_debug ("salTTY event on backout did not yield any data");
					gotlen = 0;
#if PERMIT_EXIT_IN_SHELL
				} else if ((errno == EIO) && (isatty (backout))) {
					/* correct for loud pty breakdown */
log_debug ("salTTY event on backout resulted in I/O error");
					gotlen = 0;
#endif
				}
			}
			if (gotlen < 0) {
				fprintf (stderr, "Error reading from child stdout\n");
				ok = 0;
				backout = -1;
			} else if (gotlen == 0) {
				//TODO//BADIDEA// backout = -1;
				backout = -1;
			} else {
log_debug ("backout passes into multtyflow_process_text()");
log_data ("backout", buf, gotlen, 0);
				multtyflow_process_text (backflow, buf, gotlen);
			}
		}
		if (FD_ISSET (0, &both)) {
			gotlen = read (0, buf, BUFLEN);
log_debug ("salTTY stdin,  gotlen=%d", gotlen);
			if (gotlen < 0) {
				fprintf (stderr, "Error reading from saltty stdin\n");
				ok = 0;
			} else if (gotlen == 0) {
				kill (child, SIGHUP);
			} else {
log_data ("To backin", buf, gotlen, 0);
				write (backin, buf, gotlen);	/* TODO: DIRECT METHOD */
			}
		}
		if (FD_ISSET (backout, &gone)) {
log_debug ("Child is gone");
			backout = -1;
		}
	}
	return ok;
}


/* The main routine runs a child process with mulTTY flows
 * on input and output.  It takes out the "sasl" stream from
 * the child process's output flow and passes it into a
 * Quick-SASL client exchange; output is merged into the
 * child process's input flow as its "sasl" stream.
 */
int main (int argc, char *argv []) {
	int ok = 1;
	//
	// Parse commandline arguments
	multty_t *stream_in  = multty_stdin ;
	multty_t *stream_out = multty_stdout;
	int opt;
	bool help = false;
	bool error = false;
	bool pty_force = false;
	bool pty_enabled = true;
	bool client = false;
	char *socket = NULL;
	char *servv [argc+1];
	char *hostv [argc+1];
	int servc = 0;
	int hostc = 0;
	while ((opt = getopt (argc, argv, "HctTS:s:h:")) != -1) {
		switch (opt) {
		case 'c':
			client = true;
			break;
		case 'S':
			socket = optarg;
			fprintf (stderr, "Sockets are not supported yet\n");
			break;
		case 't':
			pty_force = true;
			break;
		case 'T':
			pty_enabled = false;
			break;
		case 's':
			servv [servc++] = optarg;
			break;
		case 'h':
			hostv [hostc++] = optarg;
			break;
		default:
			error = true;
			/* continue into 'H' */
		case 'H':
			/* ugly, consider --help longopt */
			help = true;
			break;
		}
	}
	int argi = optind;
	if ((argi > argc) && (strcmp (argv [argi], "--") == 0)) {
		argi++;
	}
	if (argc - argi < 1) {
		error = error || !help;
	}
	if (pty_force && !pty_enabled) {
		fprintf (stderr, "You cannot force and disable pseudo-terminals with -t and -T\n");
		error = true;
	}
	if (servc == 0) {
		servv [servc++] = "host";
	}
	hostv [hostc] = NULL;
	servv [servc] = NULL;
	hosts = hostv;
	services = servv;
	if (help || error) {
		fprintf (stderr, "Usage: saltty [-c|-t|-T|-S socket]... [--] cmd [args...]\n");
		exit (error ? 1 : 0);
	}
	//
	// Setup Quick-SASL
	qsasl_init (NULL, "Hosted_Identity");
	//
	// Setup a mulTTY flows, main looks to the caller and back to the command
	multtyflow_t mainflow;
	multtyflow_t backflow;
	multtyflow_init (&mainflow, false);
	multtyflow_init (&backflow, false);
	//
	// Construct redirection pipes
	int pipin  [2];
	int pipout [2];
	int tmpin  = dup (0);
	int tmpout = dup (1);
	if ((tmpin < 0) || (tmpout < 0)) {
		fprintf (stderr, "Failed to create replicas for stdin/stdout\n");
		exit (1);
	}
	int newin  = tmpin ;
	int newout = tmpout;
	int failed_pipin ;
	int failed_pipout;
	bool pty_used = (pty_force || (pty_enabled && isatty (1)));
	if (pty_used) {
		// Use a pty for stdout, but not stderr; master will be us
		struct termios tio;
		struct winsize wsz;
		bool have_tio = (isatty (0) && (tcgetattr (0, &tio) == 0));
		if (have_tio) {
			tio.c_lflag &= ~ECHO;
		}
		bool have_wsz = (ioctl (1, TIOCGWINSZ, &wsz) == 0);
		failed_pipout = openpty (pipout+0, pipout+1, NULL,
					have_tio ? &tio : NULL,
					have_wsz  ? &wsz : NULL);
		// The same file descriptor, used in the opposite direction;
		// match the order of file descriptors from pipe()
		pipin [0] = dup (pipout [1]);
		pipin [1] = dup (pipout [0]);
		failed_pipin  = (pipin [0] < 0) || (pipin [1] < 0);
	} else {
		failed_pipin  = pipe (pipin );
		failed_pipout = pipe (pipout);
	}
	if (failed_pipin  || (dup2 (pipin  [0], 0) != 0)) {
		fprintf (stderr, "Failed to wrap stdin file\n");
		exit (1);
	}
	if (failed_pipout || (dup2 (pipout [1], 1) != 1)) {
		fprintf (stderr, "Failed to wrap stdout file\n");
		exit (1);
	}
	close (pipin  [0]);
	close (pipout [1]);
	//
	// Start a child process with the right piping
	pid_t child = fork ();
	switch (child) {
	case -1:
		/* Failure */
		fprintf (stderr, "Failed to fork a child process\n");
		exit (1);
	case 0:
		/* Child */
		close (pipin  [1]);
		close (pipout [0]);
		close (newin );
		close (newout);
		dup2 (1, 2);
		setsid ();
		execvp (argv [argi], argv+argi);
		fprintf (stderr, "Child process failed to run the command\n");
		exit (1);
	default:
		/* Parent */
		close (0);
		dup2 (newin,  0);
		dup2 (newout, 1);
		close (newin );
		close (newout);
		newin  = 0;
		newout = 1;
	}
	//
	// Multiplex the messages sent to stdout and stderr
	// Process optional stream names given with -o and -e
	backflow.text_cb    = child_outflow_textcb;
	backflow.event_cb   = child_outflow_eventcb;
	backflow.badchar_cb = child_outflow_badcharcb;
	mainflow.text_cb    = child_inflow_textcb;
	mainflow.event_cb   = child_inflow_eventcb;
	mainflow.badchar_cb = child_inflow_badcharcb;
	sasl_s2c.text_cb    = sasl_s2c_textcb;
	sasl_s2c.event_cb   = sasl_s2c_eventcb;
	// sasl_s2c.parse_cb   = multty_sasl_parse_cb;
	sasl_s2c.flow       = &backflow;
	//sasl_s2c.badchar_cb = sasl_s2c_badcharcb;
	backin  = pipin  [1];
	backout = pipout [0];
	ok = ok && relay_streams (child, &mainflow, &backflow);
	// Wait for the child to finish, then wrapup
	int status;
log_debug ("Waiting for child process %d", child);
	waitpid (child, &status, 0);
	multtyflow_fini (&backflow);
	multtyflow_fini (&mainflow);
	qsasl_fini ();
	exit (ok ? 0 : 1);
}

