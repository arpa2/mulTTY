/* netty.c -- Collect information from programs and dispatch over TCP
 *
 * This is a multiplexer for program streams, which will be combined
 * as distinguishable programs, each with its own distinguishable
 * streams, and distributed to any clients.
 *
 * Programs connect to netty through a named UNIX domain socket.  The
 * name of the socket is formed as "PROGRAM.client" where the PROGRAM
 * is the name chosen on the commandline to identify the instance.
 *
 * The PROGRAM name is used to name the multiplexing program; the
 * fact that it is a bound socket helps to ensure that it is unique.
 * It is introduced with <SUB> as soon as the socket is created, and
 * terminated as soon as it closes, all under the <STX> context for
 * neTTY itself.
 *
 * TCP clients share their view on the data, and even share the input
 * streams.  But the input and output flows differ somewhat.  Output
 * for each TCP client may start from a different point, and that takes
 * some initialisation with <SOH>PROG<STX> to start in the currently
 * productive program, and an explicit <SOH>PROG<SUB> avoids confusion.
 * Input flows are different, and clients may be working on different
 * programs.  It is also possible however that they share an input
 * stream and then need to be careful not to confuse the program.
 * This is more flexble than most shared sessions, and it can be a
 * useful mode during realtime communication, both to show programs
 * doing their thing and to hand over control.  Use it wisely :)
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>

#include <ev.h>

#include <arpa2/multty.h>
#include <arpa2/socket.h>
#include <arpa2/except.h>

#include "utlist.h"


/* The structure maintained for a program.
 * This includes the program's name and socket.
 * As a multiplexer, neTTY must juggle these
 * structures by knowing which is current, or
 * previous, and so on.
 */
struct program {
	struct ev_io evio;
	struct program *next;
	int flow;
	struct sockaddr_un sockname;
	char *progname;
	int prognamelen;
};


/* The structure maintained for a client.
 * This is nameless, but holds any rights that
 * the connection acquired.
 */
struct client {
	struct ev_io evio;
	struct client *next;
	int cnx;
	struct sockaddr_in6 sockname;
	struct program *curin;
	struct program *curout;
	//TODO// char *userid;
	//TODO// char *domain;
	//TODO// void *rights;
};



/* Global variables listing all programs and clients.
 */
struct program *all_programs = NULL;
struct client  *all_clients  = NULL;

static struct program null_prog = {
	.progname = "(none)",
	.prognamelen = 6,
};

struct program *current_output_program = &null_prog;




/* Forward definitions
 */
void prg_read (EV_P_ ev_io *evio, int revents);
void cli_read (EV_P_ ev_io *evio, int revents);



/* Add a new program to a listenint socket.  Accept the new socket
 * and take over responsibility, even when this call fails.
 * Start the event handler.
 *
 * Once allocated, the structure must be freed with del_program.
 */
struct program *add_program (int sox) {
	struct program *prg = malloc (sizeof (struct program));
	if (prg == NULL) {
		log_error ("Failed to add a new program");
		goto fail;
	}
	prg->flow = -1;
	socklen_t namelen = sizeof (prg->sockname);
	prg->flow = accept (sox, (struct sockaddr *) &prg->sockname, &namelen);
	if (prg->flow < 0) {
		log_error ("Failed to extract program name from socket");
		goto fail;
	}
	prg->progname = (prg->sockname.sun_path [0] != '/') ? NULL : strrchr (prg->sockname.sun_path + 1, '/');
	if (prg->progname == NULL) {
		log_error ("Program socket should bind to an absolute path name");
		goto fail;
	}
	//TODO// Could compare to the socketdir to avoid progname overlap
	prg->progname++;
	prg->prognamelen = strlen (prg->progname) - 7;
	if ((prg->prognamelen < 1) || (strcmp (prg->progname+prg->prognamelen, ".client") != 0)) {
		log_info ("Program socket lacks .client suffix");
		goto fail;
	}
	if (prg->prognamelen) {
		if (strcmp (prg->progname + prg->prognamelen - 7, ".client") == 0) {
			prg->prognamelen -= 7;
		}
	}
	LL_PREPEND (all_programs, prg);
	ev_io_init (&prg->evio, prg_read, prg->flow, EV_READ);
	ev_io_start (EV_DEFAULT, &prg->evio);
	return prg;
fail:
	if (prg != NULL) {
		if (prg->flow >= 0) {
			close (prg->flow);
		}
		free (prg);
	}
	return NULL;
}


/* Remove a program, closing the resources (including the socket)
 * that comes with it.  Stop the event handler.
 */
void del_program (struct program *prg) {
	log_debug ("Program closed UNIX domain socket %d", prg->flow);
	if (prg == current_output_program) {
		current_output_program = &null_prog;
	}
	ev_io_stop (EV_DEFAULT, &prg->evio);
	LL_DELETE (all_programs, prg);
	close (prg->flow);
	prg->flow = -1;
	free (prg);
}


/* Add a new client, starting by accept()ing its connection
 * and adopting control through its client structure.
 * On success, start the event handler.
 */
struct client *add_client (int sox) {
	struct client *cli = malloc (sizeof (struct client));
	if (cli == NULL) {
		log_error ("Failed to add a new client");
		goto fail;
	}
	socklen_t namelen = sizeof (struct sockaddr_in6);
	cli->cnx = accept (sox, (struct sockaddr *) &cli->sockname, &namelen);
	LL_PREPEND (all_clients, cli);
	ev_io_init (&cli->evio, cli_read, cli->cnx, EV_READ);
	ev_io_start (EV_DEFAULT, &cli->evio);
	return cli;
memfail:
	free (cli);
fail:
	return NULL;
}


/* Remove a client, cleaning up its resources as well.
 * Stop the event handler.
 */
void del_client (struct client *cli) {
	log_debug ("Client closed TCP socket %d", cli->cnx);
	ev_io_stop (EV_DEFAULT, &cli->evio);
	LL_DELETE (all_clients, cli);
	close (cli->cnx);
	cli->cnx = -1;
	free (cli);
}


/* Read event on the UNIX domain socket:
 * A new program is connecting to neTTY.
 */
void prg_connect (EV_P_ ev_io *evio, int revents) {
	struct program *prg = add_program (evio->fd);
	log_debug ("Program connected to UNIX domain socket %d: %.*s",
			prg->flow, prg->prognamelen, prg->progname);
	; // Nothing more to do
}


/* Read event on the TCP socket:
 * A new client is connecting to neTTY.
 */
void cli_connect (EV_P_ ev_io *evio, int revents) {
	struct client *cli = add_client (evio->fd);
	log_debug ("Client connected to TCP socket %d", cli->cnx);
	char swi [1 + current_output_program->prognamelen + 2];
	int swilen;
	if (current_output_program != &null_prog) {
		swilen = sprintf (swi, "%c%.*s%c",
				c_SOH,
				current_output_program->prognamelen,
				current_output_program->progname,
				c_STX);
	} else {
		swilen = sprintf (swi, "%c%c",
				c_STX,
				c_EOT);
	}
	if (send (cli->cnx, swi, swilen, MSG_NOSIGNAL) != swilen) {
		log_info ("Client failed to initialise; tearing down");
		del_client (cli);
	}
}


/* Dispatch a piece of text to all clients.
 */
void cli_dispatch (char *txt, size_t txtlen) {
	struct client *cli;
	LL_FOREACH (all_clients, cli) {
		ssize_t wrlen = send (cli->cnx, txt, txtlen, 0);
		if (wrlen != txtlen) {
			log_error ("Client got only %zd out of %zd bytes", wrlen, txtlen);
		}
	}
}


/* Proxy data from a program's UNIX domain socket.
 */
void prg_read (EV_P_ ev_io *evio, int revents) {
	struct program *prg = (struct program *) evio;
	log_debug ("Program data to be read: %.*s", prg->prognamelen, prg->progname);
	char buf [4096];
	ssize_t prelen = 0;
	if (current_output_program != prg) {
		prelen = snprintf (buf, sizeof (buf)-100, "%c%.*s%c",
			c_SOH,
			prg->prognamelen, prg->progname,
			c_SUB);
	}
	ssize_t rdlen = recv (prg->flow, buf + prelen, sizeof (buf) - prelen, 0);
	if (rdlen < 0) {
		if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
			return;
		}
		log_error ("Failure reading from program socket");
	} else if (rdlen == 0) {
		buf [prelen++] = c_EOT;
		/* We could also insert a closedown reason with <EM> */
		current_output_program = prg;
		cli_dispatch (buf, prelen + rdlen);
		del_program (prg);
	} else {
		current_output_program = prg;
		cli_dispatch (buf, prelen + rdlen);
	}
}


/* Proxy data from a client's TCP socket.  At the end of
 * the connection, close it down.
 */
void cli_read (EV_P_ ev_io *evio, int revents) {
	struct client *cli = (struct client *) evio;
	log_debug ("Client data to be read");
	char buf [4096];
	ssize_t rdlen = read (cli->cnx, buf, sizeof (buf));
	if (rdlen < 0) {
		if ((errno != EAGAIN) && (errno != EWOULDBLOCK)) {
			log_error ("Error reading from client socket");
		}
	} else if (rdlen == 0) {
		del_client (cli);
	} else {
		; /* TODO: Drop client-sent data for the time being */
	}
}


/* Open the UNIX domain socket where programs connect to neTTY.
 */
int create_program_service (char *socketdir) {
	//
	// The UNIX domain socket to return, and a lock handle that
	// will be forgotten about while it is open; meaning that
	// unlock does not happen before program exit
	int sox = -1;
	int lockfd = -1;
	//
	// Form the address for the UNIX domain socket
	struct sockaddr_un sun;
	memset (&sun, 0, sizeof (sun));
	sun.sun_family = AF_UNIX;
	int len = snprintf (sun.sun_path, sizeof (sun.sun_path), "%s/netty.service", socketdir);
	if (len >= sizeof (sun.sun_path)) {
		fprintf (stderr, "Socket path too long\n");
		goto fail;
	}
	//
	// Make sure that the socket directory exists
	mkdir (socketdir, 0775 | S_ISVTX);
	//
	// Claim the netty.lock file before restarting the UNIX domain socket
	char lockname [PATH_MAX];
	snprintf (lockname, PATH_MAX, "%s/netty.lock", socketdir);
	lockfd = open (lockname, O_RDONLY | O_CREAT, 0600);
	if (lockfd < 0) {
		perror ("Failed to open/create socket lock file\n");
		goto fail;
	}
	if (flock (lockfd, LOCK_EX | LOCK_NB) != 0) {
		perror ("Failed to locks the neTTY socket\n");
		goto fail;
	}
	unlink (sun.sun_path);
	//
	// Start the socket, which we now know we own and rule
	sox = socket (AF_UNIX, SOCK_STREAM, 0);
	if (sox < 0) {
		perror ("Unable to allocate socket\n");
		goto fail;
	}
	if (bind (sox, (struct sockaddr *) &sun, sizeof (sun)) < 0) {
		perror ("Failed to bind to the neTTY socket");
		goto fail;
	}
	listen (sox, 10);
	//
	// Setup event handling for newly arrived requests
	if (!socket_nonblock (sox)) {
		fprintf (stderr, "Failed to switch to non-blocking UNIX domain socket\n");
		goto fail;
	}
	//
	// Return the socket while leaving the lock file open
	return sox;
	//
	// Upon failure, drop the lock and return -1
fail:
	if (sox >= 0) {
		close (sox);
	}
	if (lockfd >= 0) {
		close (lockfd);
	}
	return -1;
}


/* Open the TCP socket where clients connect to neTTY.
 */
int create_client_service (char *address, char *port) {
	struct sockaddr_storage sa;
	int sox = -1;
	if (!socket_parse (address, port, &sa)) {
		fprintf (stderr, "Failed to parse the address\n");
		goto fail;
	}
	if (!socket_server (&sa, SOCK_STREAM, &sox)) {
		fprintf (stderr, "Failed to start TCP service\n");
		goto fail;
	}
	if (!socket_nonblock (sox)) {
		fprintf (stderr, "Failed to switch to non-blocking TCP service\n");
		goto fail;
	}
	return sox;
fail:
	if (sox >= 0) {
		close (sox);
	}
	return -1;
}


/* The main routine sets up a network socket for monitoring clients
 * and a UNIX domain socket for programs that offer monitoring.
 * Then, it enters a responsive loop to pass information.
 */
int main (int argc, char *argv []) {
	//
	// Parse commandline arguments
	char *netty = argv [0];
	char *socketdir = NULL;
	bool ok = true;
	bool help = false;
	char *address = NULL;
	char *port = "23";
	int opt;
	while ((opt = getopt (argc, argv, "hS:")) != -1) {
		switch (opt) {
		case 'h':
			help = true;
			break;
		case 'S':
			socketdir = optarg;
			break;
		default:
			ok = false;
			help = true;
		}
	}
	if (optind++ < argc) {
		address = argv [optind-1];
	}
	if (optind < argc) {
		port = argv [optind++];
	}
	if (optind != argc) {
		help = true;
		ok = false;
	}
	if (help) {
		fprintf (stderr, "Usage: %s [-h] [-S /path/to/socketdir] address [port]\n", netty);
		exit (ok ? 0 : 1);
	}
	if (socketdir == NULL) {
		socketdir = "/var/run/mulTTY";
	}
	//
	// Create the program and client service sockets
	int prgsox = create_program_service (socketdir);
	int clisox = create_client_service (address, port);
	if ((prgsox < 0) || (clisox < 0)) {
		exit (1);
	}
	//
	// Setup event handlers for program and client services
	ev_io eviocli;
	ev_io evioprg;
	ev_io_init (&eviocli, cli_connect, clisox, EV_READ);
	ev_io_init (&evioprg, prg_connect, prgsox, EV_READ);
	ev_io_start (EV_DEFAULT, &eviocli);
	ev_io_start (EV_DEFAULT, &evioprg);
	//
	// Run the event loop
	ev_run (EV_DEFAULT, 0);
	ev_io_stop (EV_DEFAULT, &evioprg);
	ev_io_stop (EV_DEFAULT, &eviocli);
	//
	// Parse commandline arguments
	multty_t *stream_out = multty_stdout;
	multty_t *stream_err = multty_stderr;
	//
	// Orderly return
	return 0;
}

