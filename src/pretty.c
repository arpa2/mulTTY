/* Pretty Print mulTTY flows with non-mulTTY output formats
 *
 * This program turns mulTTY streams and multiplexes into output
 * that is pleasant and functional to read.  It can select what is
 * shown, and aims to use colour, bold output and so on.
 *
 * Default output is for a terminal pager with ANSI colour support.
 * Alternatives are possible, including interactive ones.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include <arpa2/socket.h>
#include <arpa2/multty.h>
#include <arpa2/except.h>

/* We use Troy D. Hanson's macro kit for linked lists.
 * https://troydhanson.github.io/uthash/utlist.html
 */
#include "utlist.h"


/*
 * DEFINITIONS FOR MODES
 */


typedef unsigned mode_t;

#define MODE_ANSI	0x00000001
#define MODE_BW		0x00000002
#define MODE_DARK	0x00000010
#define MODE_LIGHT	0x00000020
#define MODE_TEXTDUMP	0x00000100
#define MODE_HEXDUMP	0x00000200
#define MODE_DERDUMP	0x00000400
#define MODE_TTY	0x00010000
#define MODE_MARKDOWN	0x00020000
#define MODE_HTML	0x00040000

#define MODE_DEFAULT	( MODE_TTY | MODE_ANSI | MODE_TEXTDUMP )

typedef bool (*modesetfun_t) (mode_t *mode);

struct modeinfo {
	char *name;
	modesetfun_t setmode;
	uint32_t flags;
};

bool setmode_ansi    (mode_t *mode);
bool setmode_bw      (mode_t *mode);
bool setmode_dark    (mode_t *mode);
bool setmode_light   (mode_t *mode);
//TODO// bool setmode_tty     (mode_t *mode);
//TODO// bool setmode_md      (mode_t *mode);
//TODO// bool setmode_html    (mode_t *mode);
//TODO// bool setmode_txt     (mode_t *mode);
//TODO// bool setmode_hex     (mode_t *mode);
//TODO// bool setmode_der     (mode_t *mode);

static struct modeinfo modes [] = {
	{ "ansi",    setmode_ansi,    MODE_ANSI     },
	{ "bw",      setmode_bw,      MODE_BW       },
	{ "dark",    setmode_dark,    MODE_DARK     },
	{ "light",   setmode_light,   MODE_LIGHT    },
	//TODO// { "tty",     setmode_tty,     MODE_TTY      },
	//TODO// { "md",      setmode_md,      MODE_MARKDOWN },
	//TODO// { "html",    setmode_html,    MODE_HTML     },
	//TODO// { "txt",     setmode_txt,     MODE_TEXTDUMP },
	//TODO// { "hex',     setmode_hex,     MODE_HEXDUMP  },
	//TODO// { "der",     setmode_der,     MODE_DERDUMP  },
	{ NULL,      NULL,            0            }
};

/*
 * DEFINITONS FOR COLOURS
 */

typedef uint8_t colour_t;

char *pallette8 [] = {
	"black",
	"red",
	"green",
	"yellow",
	"blue",
	"magenta",
	"cyan",
	"white"
};

char *pallette256 [] = {
	"Black",
	"Maroon",
	"Green",
	"Olive",
	"Navy",
	"Purple",
	"Teal",
	"Silver",
	"Grey",
	"Red",
	"Lime",
	"Yellow",
	"Blue",
	"Fuchsia",
	"Aqua",
	"White",
	"Grey0",
	"NavyBlue",
	"DarkBlue",
	"Blue3",
	"Blue3",
	"Blue1",
	"DarkGreen",
	"DeepSkyBlue4",
	"DeepSkyBlue4",
	"DeepSkyBlue4",
	"DodgerBlue3",
	"DodgerBlue2",
	"Green4",
	"SpringGreen4",
	"Turquoise4",
	"DeepSkyBlue3",
	"DeepSkyBlue3",
	"DodgerBlue1",
	"Green3",
	"SpringGreen3",
	"DarkCyan",
	"LightSeaGreen",
	"DeepSkyBlue2",
	"DeepSkyBlue1",
	"Green3",
	"SpringGreen3",
	"SpringGreen2",
	"Cyan3",
	"DarkTurquoise",
	"Turquoise2",
	"Green1",
	"SpringGreen2",
	"SpringGreen1",
	"MediumSpringGreen",
	"Cyan2",
	"Cyan1",
	"DarkRed",
	"DeepPink4",
	"Purple4",
	"Purple4",
	"Purple3",
	"BlueViolet",
	"Orange4",
	"Grey37",
	"MediumPurple4",
	"SlateBlue3",
	"SlateBlue3",
	"RoyalBlue1",
	"Chartreuse4",
	"DarkSeaGreen4",
	"PaleTurquoise4",
	"SteelBlue",
	"SteelBlue3",
	"CornflowerBlue",
	"Chartreuse3",
	"DarkSeaGreen4",
	"CadetBlue",
	"CadetBlue",
	"SkyBlue3",
	"SteelBlue1",
	"Chartreuse3",
	"PaleGreen3",
	"SeaGreen3",
	"Aquamarine3",
	"MediumTurquoise",
	"SteelBlue1",
	"Chartreuse2",
	"SeaGreen2",
	"SeaGreen1",
	"SeaGreen1",
	"Aquamarine1",
	"DarkSlateGray2",
	"DarkRed",
	"DeepPink4",
	"DarkMagenta",
	"DarkMagenta",
	"DarkViolet",
	"Purple",
	"Orange4",
	"LightPink4",
	"Plum4",
	"MediumPurple3",
	"MediumPurple3",
	"SlateBlue1",
	"Yellow4",
	"Wheat4",
	"Grey53",
	"LightSlateGrey",
	"MediumPurple",
	"LightSlateBlue",
	"Yellow4",
	"DarkOliveGreen3",
	"DarkSeaGreen",
	"LightSkyBlue3",
	"LightSkyBlue3",
	"SkyBlue2",
	"Chartreuse2",
	"DarkOliveGreen3",
	"PaleGreen3",
	"DarkSeaGreen3",
	"DarkSlateGray3",
	"SkyBlue1",
	"Chartreuse1",
	"LightGreen",
	"LightGreen",
	"PaleGreen1",
	"Aquamarine1",
	"DarkSlateGray1",
	"Red3",
	"DeepPink4",
	"MediumVioletRed",
	"Magenta3",
	"DarkViolet",
	"Purple",
	"DarkOrange3",
	"IndianRed",
	"HotPink3",
	"MediumOrchid3",
	"MediumOrchid",
	"MediumPurple2",
	"DarkGoldenrod",
	"LightSalmon3",
	"RosyBrown",
	"Grey63",
	"MediumPurple2",
	"MediumPurple1",
	"Gold3",
	"DarkKhaki",
	"NavajoWhite3",
	"Grey69",
	"LightSteelBlue3",
	"LightSteelBlue",
	"Yellow3",
	"DarkOliveGreen3",
	"DarkSeaGreen3",
	"DarkSeaGreen2",
	"LightCyan3",
	"LightSkyBlue1",
	"GreenYellow",
	"DarkOliveGreen2",
	"PaleGreen1",
	"DarkSeaGreen2",
	"DarkSeaGreen1",
	"PaleTurquoise1",
	"Red3",
	"DeepPink3",
	"DeepPink3",
	"Magenta3",
	"Magenta3",
	"Magenta2",
	"DarkOrange3",
	"IndianRed",
	"HotPink3",
	"HotPink2",
	"Orchid",
	"MediumOrchid1",
	"Orange3",
	"LightSalmon3",
	"LightPink3",
	"Pink3",
	"Plum3",
	"Violet",
	"Gold3",
	"LightGoldenrod3",
	"Tan",
	"MistyRose3",
	"Thistle3",
	"Plum2",
	"Yellow3",
	"Khaki3",
	"LightGoldenrod2",
	"LightYellow3",
	"Grey84",
	"LightSteelBlue1",
	"Yellow2",
	"DarkOliveGreen1",
	"DarkOliveGreen1",
	"DarkSeaGreen1",
	"Honeydew2",
	"LightCyan1",
	"Red1",
	"DeepPink2",
	"DeepPink1",
	"DeepPink1",
	"Magenta2",
	"Magenta1",
	"OrangeRed1",
	"IndianRed1",
	"IndianRed1",
	"HotPink",
	"HotPink",
	"MediumOrchid1",
	"DarkOrange",
	"Salmon1",
	"LightCoral",
	"PaleVioletRed1",
	"Orchid2",
	"Orchid1",
	"Orange1",
	"SandyBrown",
	"LightSalmon1",
	"LightPink1",
	"Pink1",
	"Plum1",
	"Gold1",
	"LightGoldenrod2",
	"LightGoldenrod2",
	"NavajoWhite1",
	"MistyRose1",
	"Thistle1",
	"Yellow1",
	"LightGoldenrod1",
	"Khaki1",
	"Wheat1",
	"Cornsilk1",
	"Grey100",
	"Grey3",
	"Grey7",
	"Grey11",
	"Grey15",
	"Grey19",
	"Grey23",
	"Grey27",
	"Grey30",
	"Grey35",
	"Grey39",
	"Grey42",
	"Grey46",
	"Grey50",
	"Grey54",
	"Grey58",
	"Grey62",
	"Grey66",
	"Grey70",
	"Grey74",
	"Grey78",
	"Grey82",
	"Grey85",
	"Grey89",
	"Grey93"
};

#define ATTR_BOLD      0x01
#define ATTR_BRIGHT    0x01
#define ATTR_DIM       0x02
#define ATTR_FAINT     0x02
#define ATTR_ITALICS   0x04
#define ATTR_UNDERLINE 0x08
#define ATTR_INVERSE   0x10
#define ATTR_REVERSE   0x10

typedef uint8_t attrset_t;

struct attrinfo {
	char *name;
	attrset_t attr_add;
};

struct attrinfo attributes [] = {
	{ "bold",      ATTR_BOLD      },
	{ "bright",    ATTR_BRIGHT    },
	{ "dim",       ATTR_DIM       },
	{ "faint",     ATTR_FAINT     },
	{ "italics",   ATTR_ITALICS   },
	{ "underline", ATTR_UNDERLINE },
	{ "inverse",   ATTR_INVERSE   },
	{ "reverse",   ATTR_REVERSE   },
	{ NULL,        0              }
};



/*
 * COLOUR HANDLING
 *
 * To pick colours, the following priority order is used:
 *  - Modes "dark", "light" and "bw" render certain colours as in-use
 *  - Named colours in selectors are applied with force, and may overlap
 *  - Positions of selectors come second, claiming colours without overlap
 *  - Dynamic programs without assigned colour present their name to find a colour
 *
 * Current colour allocation acts as if all colours are equally suited.
 * It may be more attactive to take colour separation from each other and
 * from the background into a count as an optimisation criterium.
 *
 */



/* Return a static buffer with the ANSI escape codes for setting the
 * given foreground colour and attribute set.
 */
char *ansi_escapes (colour_t fg, attrset_t attr) {
	char *dimbr;
	if ((attr & (ATTR_BOLD | ATTR_DIM)) == ATTR_BOLD) {
		dimbr = ";1";
	} else if ((attr & (ATTR_BOLD | ATTR_DIM)) == ATTR_DIM ) {
		dimbr = ";2";
	} else {
		dimbr = "";
	}
	char *it = ((attr & ATTR_ITALICS  ) != 0) ? ";3" : "";
	char *ul = ((attr & ATTR_UNDERLINE) != 0) ? ";4" : "";
	char *iv = ((attr & ATTR_INVERSE  ) != 0) ? ";7" : "";
	static char escbuf [50];
	//OLD// sprintf (escbuf, "\x1b[0m" "\x1b[38;5;%dm", fg);
	sprintf (escbuf, "\x1b[0;38;5;%d%s%s%s%sm", fg, dimbr, it, ul, iv);
	return escbuf;
}

static unsigned colour_taken [256];

void block_colour (colour_t c) {
	colour_taken [c] = 9999;
}

bool setmode_ansi (mode_t *mode) {
	//TODO// Sanity check
	*mode |= MODE_ANSI;
	return true;
}

bool setmode_bw (mode_t *mode) {
	//TODO// Sanity check
	for (int ci = 2; ci < 256; ci++) {
		block_colour ((colour_t) ci);
	}
	*mode |= MODE_BW;
	return true;
}

// Visually, from https://www.ditig.com/256-colors-cheat-sheet
bool setmode_dark (mode_t *mode) {
	//TODO// Sanity check
	colour_t darkfg [] = { 0, 16, 17, 18, 52, 53, 232, 233, 234, 235, 236 };
	int idx = sizeof (darkfg) / sizeof (colour_t);
	while (idx-- > 0) {
		block_colour (darkfg [idx]);
	}
	*mode |= MODE_DARK;
	return true;
}

// Visually, from https://www.ditig.com/256-colors-cheat-sheet
bool setmode_light (mode_t *mode) {
	//TODO// Sanity check
	colour_t lightfg [] = { 15, 195, 231, 254, 255 };
	int idx = sizeof (lightfg) / sizeof (colour_t);
	while (idx-- > 0) {
		block_colour (lightfg [idx]);
	}
	*mode |= MODE_LIGHT;
	return true;
}

/* Claim a colour, possibly requesting for a unique entry.  Return success.
 */
bool claim_colour (colour_t request, bool unique) {
	if (unique && (colour_taken [request] > 0)) {
		return false;
	}
	colour_taken [request]++;
	return true;
}

/* Find a colour by name, return colour_t or -1 for not found.
 * Use claim_colour() to actually claim it.
 */
int find_colour (char *colname) {
	for (int ci = 0; ci < 256; ci++) {
		if (strcasecmp (pallette256 [ci], colname) == 0) {
			return ci;
		}
	}
	for (int ci = 0; ci < 8; ci++) {
		if (strcasecmp (pallette8 [ci], colname) == 0) {
			return ci;
		}
	}
	return -1;
}

/* Find an attrset by name, return the bits it adds or 0 for not found.
 */
attrset_t find_attrset (char *attrname) {
	struct attrinfo *attrs = attributes;
	while (attrs->name != NULL) {
		if (strcasecmp (attrs->name, attrname) == 0) {
			return attrs->attr_add;
		}
		attrs++;
	}
	return 0;
}

/* Find a colour, using the opt_hint to scatter the domain somewhat,
 * in the hope to have some consistency in colours picked between
 * runs.  If opt_hint is NULL, choose a random colour.
 * Always return a colour code.
 */
colour_t pick_colour (char *opt_hint) {
	unsigned hintcode = 0xd9;
	//
	// Form a hint code, either from the opt_hint or random
	if (opt_hint != NULL) {
		while (*opt_hint) {
			hintcode = hintcode + hintcode + (hintcode >> 7);
			hintcode ^= *opt_hint++;
		}
	} else {
		hintcode = random () & 0xff;
	}
	//
	// Find a free colour code, or resort to reusing a colour
	//TODO// This leaves room for many tasteful improvements
	for (unsigned ci = hintcode; ci < hintcode + 256; ci++) {
		if (colour_taken [hintcode] == 0) {
			return (colour_t) (hintcode & 0xff);
		}
	}
	return (colour_t) hintcode;
}



/*
 *
 * PROGRAM AND STREAM STRUCTURES
 *
 */


struct program;

struct stream {
	//
	// Name and colour information
	char *name;
	attrset_t attr_add, attr_del;
	bool has_colour;
	colour_t colour;
	//
	// Links to the owning program
	struct program *owner;
	//
	// Links to other streams
	struct stream *next;
};

struct program {
	//
	// Name and colour information
	char *name;
	colour_t colour;
	attrset_t attr_add;
	//
	// Links to other programs
	struct program *next;
	struct program *parent, *children;
	struct program *current_child, *previous_child;
	//
	// Links to streams of this program
	struct stream *streams;
	struct stream *current_input, *current_output;
	struct stream *default_input, *default_output;
	//
	// Whether dynamic streams are blocked
	bool block_dynamic_streams;
	//
	// Whether the program was closed by <EOT>
	bool closed;
};


/* The "preTTY" program is not explicitly created; it is a global.
 * This saves a lot of special arrangements, among others caused
 * by NULL values and not being able to point at its parent.
 */
static struct program pretty_program = {
	.name = "preTTY",
	.attr_add = ATTR_INVERSE,
	.block_dynamic_streams = true,
};


/* The current_program is the start of any operations of programs.
 * It is also the path to finding the current stream, which is a
 * local property of a program (including the current program).
 */

static struct program *current_program = &pretty_program;


/* Compare streams by their name.
 */
int stream_compare (struct stream *s1, struct stream *s2) {
	return strcasecmp (s1->name, s2->name);
}

/* Compare programs by their name.
 */
int program_compare (struct program *p1, struct program *p2) {
	return strcasecmp (p1->name, p2->name);
}

/* Return a static buffer with the colour and attributes for the
 * current program and stream.  Indicate whether the input or
 * output stream is targeted.
 */
char *update_colour (bool in_not_out) {
	//
	// Colour normally comes from the program, but streams may override
	colour_t fg = current_program->colour;
	struct stream *str = in_not_out ? current_program->current_input
	                                : current_program->current_output;
	if (str->has_colour) {
		fg = str->colour;
	}
	//
	// Attributes normally come from streams, but the program may suggest
	attrset_t attr = (current_program->attr_add & ~str->attr_del) | str->attr_add;
	//
	// Compose colour and attributes into an ANSI escape sequence
	return ansi_escapes (fg, attr);
}

/* Be sure to have a stream with the given name, adding it if not found.
 * Make this the default input or output stream, as per flagged request.
 */
struct stream *have_stream (char *name, bool make_current_input, bool make_current_output) {
	struct stream *str = NULL;
	//
	// Find the stream in the current program
	struct stream sought;
	memset (&sought, 0, sizeof (sought));
	sought.name = name;
	LL_SEARCH (current_program->streams, str, &sought, stream_compare);
	//
	// If not found, allocate a new stream
	if (str == NULL) {
		//
		// Allocate and fill the structure
		str = calloc (sizeof (struct stream), 1);
		if (str == NULL) {
			perror ("No room for stream");
			exit (1);
		}
		str->name = name;
		//
		// Enqueue the stream
		LL_PREPEND (current_program->streams, str);
		//
		// Try to copy from a template in the "preTTY" root program
		if (current_program != &pretty_program) {
			struct stream *tmpl;
			LL_SEARCH (pretty_program.streams, tmpl, &sought, stream_compare);
			if (tmpl != NULL) {
				str->has_colour = tmpl->has_colour;
				str->colour     = tmpl->colour;
				str->attr_add   = tmpl->attr_add;
				str->attr_del   = tmpl->attr_del;
			}
		}
	}
	//
	// Make the (new) stream as the current input/output
	if (make_current_input) {
		current_program->current_input  = str;
	}
	if (make_current_output) {
		current_program->current_output = str;
	}
	//
	// Return the stream
	return str;
}

/* Add default streams to the current program: stdin, stdout, stderr, stdctl.
 * Set stdin as current_/default_input and stdout as current_/default_output.
 */
void add_default_streams (void) {
	struct stream *ctl              = have_stream ("stdctl", false, false);
	struct stream *err              = have_stream ("stderr", false, false);
	current_program->default_input  = have_stream ("stdin",  true,  false);
	current_program->default_output = have_stream ("stdout", false, true );
	//
	// If this is the pretty_program, set its default attributes
	if (current_program == &pretty_program) {
		ctl->attr_add = ATTR_DIM;
		err->attr_add = ATTR_BOLD;
	}
}

/* At the current level (0), the parent (+1) or child (-1) be sure to
 * have a program with the given name, adding it if not found.
 * Make this the default program.
 */
struct program *have_program (int level, char *name) {
	struct program *prg = NULL;
	//
	// Possibly change the head for this program level
	struct program *parent;
	if (level > 0) {
		if (current_program == &pretty_program) {
			fprintf (stderr, "Attempt to rise above the root program\n");
			return NULL;
		}
		if (current_program->parent == &pretty_program) {
			fprintf (stderr, "Attempt to be on par with the root program\n");
			return NULL;
		}
		parent = current_program->parent;
	} else if (level < 0) {
		if (current_program->closed) {
			fprintf (stderr, "Attempt to dive below a closed program\n");
			return NULL;
		}
		parent = current_program;
	} else {
		parent = current_program->parent;
	}
	//
	// First try to find the program
	struct program sought;
	memset (&sought, 0, sizeof (sought));
	sought.name = name;
	LL_SEARCH (parent->children, prg, &sought, program_compare);
	//
	// If not found, allocate a new program
	if (prg == NULL) {
		//
		// Allocate and fill the structure
		prg = calloc (sizeof (struct program), 1);
		if (prg == NULL) {
			perror ("No room for program");
			exit (1);
		}
		prg->name = name;
		prg->parent = parent;
		//
		// Hang the program into parent/children/next structures
		LL_PREPEND (parent->children, prg);
	}
	//
	// Set the program we (now) have as the default
	if (prg != parent->current_child) {
		parent->previous_child = parent->current_child;
		parent->current_child = prg;
	}
	current_program = prg;
	//
	// If the program was closed, then it is now revived
	prg->closed = false;
	//
	// Be sure to have the customary streams
	if (prg->streams == NULL) {
		//
		// Create the usual streams for mulTTY with the
		// "stdin"/"stdout" as input/output for current and default
		add_default_streams ();
	}
	//
	// Return the program
	return prg;
}



/*
 * MULTTY PARSER CALLBACK PROCESSING
 */



char prefix [50+1];
char descr [1024+1];

unsigned prefix_len;
unsigned descr_len;

bool silent = false;


/* Callback reporting on text received, including prefix names/descriptions
 * for an upcoming mulTTY event.
 */
void text_cb (struct multtyflow *mty, multty_event_t evt,
		char *txt, unsigned len,
		bool first, bool last) {
#if 0
	char *kind = NULL;
	if (first) {
		kind = last ? "only" : "frst";
	} else {
		kind = last ? "last" : "part";
	}
	printf ("TEXT %d/%s%3d long, \"%.*s\"\n",
			evt, kind,
			len,
			len, txt);
#endif
	//
	// MULTTY_CONTENT is printed on stdout
	if (evt == MULTTY_CONTENT) {
		if (!silent) {
			fwrite (txt, 1, len, stdout);
		}
	//
	// MULTTY_PREFIX_NAME is saved in prefix
	} else if (evt == MULTTY_PREFIX_NAME) {
		if (first) {
			prefix_len = 0;
		}
		if (prefix_len + len > sizeof (prefix) - 1) {
			len = sizeof (prefix) - 1 - prefix_len;
		}
		if (len > 0) {
			memcpy (prefix + prefix_len, txt, len);
			prefix_len += len;
		}
		if (last) {
			prefix [prefix_len] = '\0';
		}
	//
	// MULTTY_DESCRIPTION is saved in descr
	} else if (evt == MULTTY_DESCRIPTION) {
		if (first) {
			descr_len = 0;
		}
		if (descr_len + len > sizeof (descr) - 1) {
			len = sizeof (descr) - 1 - descr_len;
		}
		if (len > 0) {
			memcpy (descr + descr_len, txt, len);
			descr_len += len;
		}
		if (last) {
			descr [descr_len] = '\0';
		}
	//
	// Other events are not recognised
	} else {
		fprintf (stderr, "Unrecognised text event %d\n", evt);
	}
}


/* Callback reporting on the events received.
 */
void event_cb (struct multtyflow *mty, multty_event_t evt,
		bool named, bool described) {
	char *name, *kind;
#if 0
	printf ("EVENT %d, named: %s, described: %s\n",
			evt,
			named ? "yes" : "no",
			described ? "yes" : "no");
#endif
	switch (evt) {
	case MULTTY_SHIFT_STREAM:
		silent = (have_stream (named ? prefix : "stdout", false, true) == NULL);
		if (!silent) {
			fputs (update_colour (false), stdout);
		}
		return;
	case MULTTY_END_STREAM:
		if (current_program->closed) {
			kind = "PROGRAM";
			name = "";
		} else {
			kind = "STREAM ";
			name = current_program->current_output->name;
		}
		if (described) {
			printf ("\nEND OF %s%s DUE TO %s (%s)\n", kind, name, prefix, descr);
		} else if (named) {
			printf ("\nEND OF %s%s DUE TO %s\n", kind, name, prefix);
		} else {
			printf ("\nEND OF %s%s\n", kind, name);
		}
		silent = true;
		return;
	case MULTTY_PROGRAM_PARENT:
		if (current_program->closed) {
			fprintf (stderr, "Unable to move to parent of closed program\n");
			return;
		} else if (current_program->parent == NULL) {
			fprintf (stderr, "The root program has no parent\n");
			return;
		} else if (named) {
			have_program (+1, prefix);
		} else {
			current_program = current_program->parent;
		}
		silent = current_program->closed;
		break;
	case MULTTY_PROGRAM_STOP:
		if (named) {
			have_program (0, prefix);
		}
		silent = current_program->closed = true;
		break;
	case MULTTY_PROGRAM_CHILD:
		if (current_program->closed) {
			fprintf (stderr, "Unable to move to child of closed program\n");
			return;
		} else if (named) {
			have_program (-1, prefix);
		} else {
			current_program = current_program->current_child;
		}
		silent = (current_program == NULL) || (current_program->closed);
		break;
	case MULTTY_PROGRAM_SIBLING:
		if (current_program->closed) {
			fprintf (stderr, "Unable to move to sibling of closed program\n");
			return;
		} else if (named) {
			have_program (0, prefix);
		} else if (current_program->parent->previous_child == NULL) {
			fprintf (stderr, "There is no previous sibling under this parent\n");
		} else {
			struct program *newprg = current_program->parent->previous_child;
			current_program->parent->previous_child = current_program->parent->current_child;
			current_program->parent->current_child = newprg;
			current_program = newprg;
			return;
		}
		silent = current_program->closed;
		break;
	default:
		fprintf (stderr, "Unrecognised event %d\n", evt);
		return;
	}
	if (!silent) {
		fputs (update_colour (false), stdout);
	}
}


/* Callback reporting parsing errors (bad characters).
 */
bool badchar_cb (struct multtyflow *mty, multty_event_t evt,
		char *bad) {
	fprintf (stderr, "ERROR: BAD CHARACTER '%c'\n", *bad);
	return false;
}




/*
 * COMMANDLINE PARSING AND PREPARATION
 */



void parse_switch (char *arg) {
	mode_t mode = 0;
	switch (*arg) {
	case 'm':
		arg++;
		struct modeinfo *moderunner = modes;
		while (moderunner->name) {
			if (strcmp (moderunner->name, arg) == 0) {
				if (!moderunner->setmode (&mode)) {
					fprintf (stderr, "Failed to set mode %s\n", arg);
				}
				return;
			}
			moderunner++;
		}
		fprintf (stderr, "Unknown mode %s\n", arg);
		break;
	case 'f':
	case 'c':
	case 'd':
	case 's':
		/* Input options are processed elsewhere */
		break;
	default:
		fprintf (stderr, "Unknown switch -%c\n", *arg);
		break;
	}
}


/* Parse selector strings, which combine program names, stream names
 * and colours/attributes.  Have elements for each in the hierarchy
 * of programs and streams under the "preTTY" root program.
 *
 * If any definition is made without explicit reference to a program,
 * have the stream and its properties attached to preTTY, where they
 * serve as templates for new discoveries during input processing.
 * The "preTTY" program itself is _not_ a template, just its streams.
 */
void parse_selector (char *sel) {
	//
	// Assume that we found nothing
	bool has_programs = false;
	bool has_streams = false;
	bool has_colours = false;
	//
	// Require a (or the) rooted program?
	bool root = false;
	if (*sel == '/') {
		root = true;
		sel++;
	} else {
		//SUPERFLUOUS// fprintf (stderr, "Assuming you meant to start at the root program\n");
		root = true;
	}
	//
	// Recognise program names
	struct program *selected_program = NULL;
	char *slash;
	while (slash = strchr (sel, '/')) {
		has_programs = true;
		//
		// Go one step down and require to have the program
		char *progname = strndup (sel, (unsigned) (slash - sel));
		selected_program = have_program (-1, progname);
		//
		// Skip beyond this program and its slash
		sel = slash + 1;
	}
	//
	// Test if we have streams and colours, and their string position
	char *streams = sel;
	char *colours;
	has_streams = ((*streams != 0x00) && (*streams != '+') && (*streams != '-'));
	char *cplus = strchr (sel, '+');
	char *cmin = strchr (sel, '-');
	if (cplus == NULL) {
		colours = cmin;
	} else if (cmin == NULL) {
		colours = cplus;
	} else if (cmin < cplus) {
		colours = cmin;
	} else {
		colours = cplus;
	}
	has_colours = (colours != NULL);
	//
	// Collect any streams under the program (up to a high count)
	unsigned numstreams = 0;
	struct stream *colstreams [100];
	if (has_streams) {
		//
		// Block dynamic addition of streams; they were made explicit
		if (selected_program) {
			selected_program->block_dynamic_streams = true;
		}
		//
		// Have all the streams specified -- even as "preTTY" templates
		char *newstr = streams;
		while (numstreams * sizeof (struct stream *) < sizeof (colstreams)) {
			//
			// Set comma to the endpoint of the streamname
			char *comma = strchr (newstr, ',');
			if (has_colours) {
				if ((comma == NULL) || (comma > colours)) {
					comma = colours;
				}
			} else if (comma == NULL) {
				comma = newstr + strlen (newstr);
			}
			//
			// Test if we are done
			if (comma <= newstr) {
				break;
			}
			//
			// Be sure to have a stream by that name
			char *dupname = strndup (newstr, (unsigned) (comma - newstr));
			bool mk_in  = (strcmp (dupname, "stdin" ) == 0);
			bool mk_out = (strcmp (dupname, "stdout") == 0);
			colstreams [numstreams] = have_stream (dupname, mk_in, mk_out);
			//
			// Quietly ignore any previously reported errors
			if (colstreams [numstreams] != NULL) {
				numstreams++;
			}
			//
			// Continue after the comma
			newstr = comma + 1;
		}
	}
	//
	// If no streams were given, make a list of all found in the program
	if (!has_streams) {
		struct stream *curstr = current_program->streams;
		while (numstreams * sizeof (struct stream *) < sizeof (colstreams)) {
			if (curstr == NULL) {
				break;
			}
			colstreams [numstreams++] = curstr;
			curstr = curstr->next;
		}
	}
	//
	// Iterate over colours to set them to all streams of the program
	if (has_colours) {
		char *newcol = colours;
		attrset_t attrs_to_add = 0;
		attrset_t attrs_to_del = 0;
		bool set_new_colour = false;
		colour_t colour_to_set;
		//
		// Loop over all colour instructions
		while ((*newcol == '+') || (*newcol == '-')) {
			//
			// Mark the kind of colour instruction
			bool is_add = (*newcol++ == '+');
			//
			// Find the end of the colour instruction
			unsigned newcollen = 0;
			while (true) {
				char n = newcol [newcollen];
				if ((n == '\0') || (n == '+') || (n == '-')) {
					break;
				}
				newcollen++;
			}
			//
			// Skip the new colour after copying it the stack
			char dupcol [newcollen+1];
			memcpy (dupcol, newcol, newcollen);
			dupcol [newcollen] = '\0';
			newcol += newcollen;
			//
			// Map the colour name to an attribute or colour
			int try_colour = find_colour (dupcol);
			attrset_t try_attrs = find_attrset (dupcol);
			if (try_colour >= 0) {
				if (set_new_colour) {
					fprintf (stderr, "Conflicting colour settings, forcing to %s\n", dupcol);
				}
				colour_to_set = try_colour;
				set_new_colour = true;
			} else if (try_attrs > 0) {
				if (is_add) {
					attrs_to_add |=  try_attrs;
				} else {
					attrs_to_del |=  try_attrs;
				}
			} else {
				fprintf (stderr, "Unrecognised colour/attribute %s\n", dupcol);
			}
		}
		//
		// Apply colour/attribute settings to the collection of streams
		for (unsigned stri = 0; stri < numstreams; stri++) {
			if (set_new_colour) {
				colstreams [stri]->has_colour = true;
				colstreams [stri]->colour = colour_to_set;
			}
			colstreams [stri]->attr_add &= ~attrs_to_del;
			colstreams [stri]->attr_del &= ~attrs_to_add;
			colstreams [stri]->attr_add |=  attrs_to_add;
			colstreams [stri]->attr_del |=  attrs_to_del;
		}
	}
	//
	// Return to the root program, "preTTY"
	current_program = &pretty_program;
}


int main (int argc, char *argv []) {
	//
	// Process mode switching arguments -- programs/streams follow below
	for (int argi = 1; argi < argc; argi++) {
		if (*argv [argi] == '-') {
			parse_switch (argv [argi] + 1);
		}
	}
	//
	// Initialise the pretty_program and its basic streams
	add_default_streams ();
	if (claim_colour (0, true)) {
		pretty_program.colour = 0;
	} else {
		claim_colour (7, false);
		pretty_program.colour = 7;
	}
	//
	// Process programs/streams arguments -- mode switches handled above
	for (int argi = 1; argi < argc; argi++) {
		if (*argv [argi] != '-') {
			parse_selector (argv [argi]);
		}
	}
	//
	// Setup for the current/default output stream
	fputs (update_colour (false), stdout);
	//
	// Process ASCII flows and turn program/stream switches into
	// ANSI escape sequences, and possibly not show it at all,
	// as specified by the selections on the commandline.
	char *inport = "23";
	bool use_stdin = true;
	for (int argi = 1; argi <= argc; argi++) {
		//
		// Setup some form of "infile", or loop around if not acceptable
		FILE *infile = NULL;
		char intype = '?';
		if (argi == argc) {
			/* No more arguments, check use_stdin as a last resort */
			if (!use_stdin) {
				continue;
			}
			log_debug ("Input from stdin");
			infile = stdin;
			intype = '0';
		} else if (*argv [argi] == '-') {
			/* This is an input argument.  Have a ball. */
			intype = argv [argi][1];
			switch (intype) {
			case 'm':
				/* Mode options are processed elsewhere */
				continue;
			case 'f':
				use_stdin = false;
				log_debug ("Input from file: %s", argv[argi]+2);
				infile = fopen (argv[argi]+2, "r");
				break;
			case 'c':
				use_stdin = false;
				log_debug ("Input from command: %s", argv[argi]+2);
				infile = popen (argv[argi]+2, "r");
				break;
			case 'd':
				use_stdin = false;
				log_debug ("Input from device: %s", argv[argi]+2);
				infile = fopen (argv[argi]+2, "r");
				break;
			case 's':
				use_stdin = false;
				if (argv[argi][2] == '/') {
					/* Path to a UNIX domain socket */
					log_debug ("TODO: Unimplemented: Input from UNIX domain client: %s", argv[argi]+2);
				} else if ((argv[argi][2] == ':') && (argv[argi][3] != ':')) {
					/* Port to be used in the following */
					if (argv[argi][3] == '\0') {
						log_debug ("Switching TCP port to connect to, old=%s, new=23", inport);
						inport = "23";
					} else {
						log_debug ("Switching TCP port to connect to, old=%s, new=%s", inport, argv[argi]+3);
						inport = argv [argi] + 3;
					}
				} else {
					/* Address of a TCP socket (TODO: non-23-port?) */
					log_debug ("Input from TCP client: %s", argv[argi]+2);
					struct sockaddr_storage sa;
					if (socket_parse (argv[argi]+2, inport, &sa)) {
						int sox = -1;
						if (socket_client (&sa, SOCK_STREAM, &sox)) {
							infile = fdopen (sox, "r");
							if (infile == NULL) {
								close (sox);
							}
						}
					}
				}
				break;
			default:
				break;
			}
		} else {
			/* Skip over non-input arguments */
			continue;
		}
		if (infile == NULL) {
			fprintf (stderr, "Failed on input %s\n", argv [argi]);
			continue;
		}
		//
		// Setup buffering without delay
		//OLD// char filebuf [1024];
		//OLD// setvbuf (infile, filebuf, _IONBF, sizeof (filebuf));
		setvbuf (infile, NULL, _IONBF, 0);
		//
		// Setup a flow object
		struct multtyflow mtyf;
		multtyflow_init (&mtyf, true);
		mtyf.text_cb = text_cb;
		mtyf.event_cb = event_cb;
		mtyf.badchar_cb = badchar_cb;
		//
		// Read and process input, one chunk at a time
		char inbuf [1024];
		size_t inlen;
		do {
			inlen = fread (inbuf, 1, sizeof (inbuf), infile);
			if (inlen > 0) {
				multtyflow_process_text (&mtyf, inbuf, inlen);
			}
		} while (inlen > 0);
		//
		// Cleanup the flow object
		multtyflow_fini (&mtyf);
		//
		// Cleanup output formatting
		fputs ("\x1b[0m", stdout);
		//
		// Close the input source if/as appropriate
		switch (intype) {
		case 'f':
			fclose (infile);
			break;
		case 'c':
			/* Note: we might learn the exit() state at this point */
			pclose (infile);
			break;
		case 's':
			fclose (infile);
			break;
		default:
			break;
		}
	}
}

