# Installing mulTTY

> *The mulTTY library operates mostly on characters, and is built around
> a Ragel parser for the ASCII forms as documented.  The one exception
> is when the SASL extension is used; this depends on a few other
> components to connect to an external validator, possibly with support
> for Realm Crossover.*

The mulTTY libraries are written in C.  Yeah, that is hardly fashionable,
but until modern languages can be included into all other languages like
C can, it is still the preferred library language.


## Build Options

  * `MULTTY_QUICK_DIASASL` adds an implementation of SASL authentication,
    which is a far more structured approach to login than a mere prompt
    for passwords.  The option causes side-tracking of flows named `SASL`
    to a backend, using a simple
    [Quick-DiaSASL protocol]()
    to a local login oracle.  This oracle may be very simple, but it may
    also be a complete Diameter node that maps the SASL flow into a
    [Realm Crossover]()
    procedure against a client's own domain.  This allows clients to bring
    their own identity (BYOID) related to their own domain.

  * **TODO:** `MULTTY_ACCESS_CONTROL` adds Access Rights lookups in a
    centrally managed database.  This database can be updated at any
    time, to influence the rights of newly authenticating users.  This
    module relies on `MULTTY_QUICK_DIASASL` for authentication, because
    most ASCII protocols have no sufficiently modern alternative.


## Build Dependencies

To build, you need:

  * CMake, to configure the package
  * Ragel, for generation of parsers
  * C building tools, to build the libraries

If you selected the SASL option, you also need:

  * The Quick-DiaSASL library from the ARPA2 stack,
    built as part of the
    [Quick-SASL](https://gitlab.com/arpa2/Quick-SASL)
    package, to pass SASL queries to a Diameter node.
  * The Quick-DER library from the ARPA2 stack,
    built from the
    [Quick-DER](https://gitlab.com/arpa2/Quick-DER)
    package, to pack and unpack data structures.
  * The `libev` library for event-based handling of
    the Quick-DiaSASL protocol.

If you selected the Access Control option, you also need:

  * The ARPA2 Access library from the ARPA2 stack,
    built as part of the
    [ARPA2 Common](https://gitlab.com/arpa2/arpa2common)
    package, to evaluate whether an authenticated user
    may proceed with the session at hand.  **TODO**


## Build Procedure

Set options to CMake on the commandline, as in `cmake -D MULTTY_QUICK_DIASASL=ON`,
or use a command like `ccmake` to do it interactively.  While generating a
configuration, CMake reports any problems with dependencies.  Now you can build.

Generally:

```
git clone https://gitlab.com/arpa2/mulTTY
cd mulTTY
mkdir build
cd build
cmake ..
make
# make test
make install
```


## Installed Components

The **libraries** built are:

  * `libmultty.so` is the library that forks and joins the flows that go in and out of a single program.
  * `libmulttyplex.so` is the library that multiplexes programs in a tree-shape.
  * **TODO:** `libmulttysasl.so` is the optional library that passes SASL to a Quick-DiaSASL backend.

The **programs** built are:

  * `dotty` is the program that merges files of a program into flows.
  * `pretty` is the program that formats multiplexes and flows in a handy and colourful manner.
  * **TODO:** `netty`, `saltty`, `clotty`, `initty`, `shotty`, `pitty`, `rutty`, `witty`, ...

