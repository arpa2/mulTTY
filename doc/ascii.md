# MulTTY: ASCII for Multiplexing Terminals

> *Technicians never stopped using terminals, and the rise
> of container technology only adds more of it.  Top that
> off with devices everywhere.  But we still make those
> terminals a little better.*

MulTTY is a package for multiplexing terminals by making
better use of the ASCII table, and interpreting a few
extra codes.  The proposal has been checked against the
two most common terminals, namely the Xterminal and the
Linux console, and are a strict extension.

We essentially propose two changes:

  * Start considering TTY flows as multiplexes of one or
    more program, each carrying one or more stream.
 
  * Consider TTY sessions over `SOCK_SEQPACKET` to keep
    atomic chunks together in transit.  This option is
    available for UNIX domain sockets and `socketpair()`
    and for plain `socket()` it connects through SCTP.
    This is not a strict requirement, but it simplifies
    matters and improves consistency and possibly also
    security.

In addition, we will add program and flow naming
information, so they are self-descriptive and thus help
their interpretation.  Most streams will be ASCII text
with extensions such as UTF-8, but some channels might
carry binary data (with escapes) or other structured
content which may be as expressive as tables or menus.

  * Multiplexing can switch between multiple programs,
    but one program would look just as it always did.
    Programs carry a name, and when programs are brought
    together they will be subjected to multiplexing, so
    switching to a named program or using a quick-hand
    reference to a recent one for speed.

  * Programs can have many streams, of which `stdout`
    and `stderr` are common examples on output.  Streams
    are not multiplexed but instead merged per direction,
    switching focus before data of a stream.  The switch
    uses the name of a stream, which also aids in
    interpreting their purpose.  There is an obvious
    default stream (`stdout` or `stdin`) before a first
    switch occurs.  By also using streams on input, it
    is possible to add something like a `stdctl` for
    controlling a program with things like starting,
    stopping, pausing, resuming and configuration reloads.
    The streams can also be forked off and treated in
    a different manner, which gives rise to more
    intelligent pipelining combinations.

One problem solved by keeping streams separate is that
each can carry its own rendering attributes, like foreground
and background colours.  A switch to another stream causes a
switch to those attributes.  This saves disturbances in
rendering when colourful output is merged.  The same idea
is also helpful to keep the text itself more clearly
distinct, and even allow pulling it apart in retrospect.

A problem solved by naming programs and having streams
for each is that we can see where output comes from, and
direct where input goes to.  Ever wanted to type the
next command to run while a task was running?  Now you
can be sure that an unexpected prompt won't lap it up.

Splitting the data is really not difficult and it all
integrates easily with current technology, by using
a few simple control codes that are currently not
in use or not standardised.  We can have much more
control about how to display things if we are able to
separate flows of control and programs producing them
independently.

Most importantly though, we get an option of much more
control: 

  * *How would you like* to tell your terminal to
    a tab for `stderr` so error messages do not mix with the
    `stdout` flow?
  * *How would you like* to make command
    editing a local task, without remote dictatation of
    available editing controls? 
  * *How would you like* to
    send a command over `stdctl` to add an output stream
    for logging at a certain level? 
  * *How would you like* your
    terminal to tell the remote to attach a debugger to a
    program and connect a local debugger to it over a new
    set of streams?


## Prefixing Names

We regularly use names to specify a managed entity.
This naming can be relative, and assumed known because
relative movements bring attention back to a
previously named entity, or a name may be expicitly
prefixed and its scope determined by the control code
that follows it.

Names start with `<SOH>` and are followed with a
sequence of characters in the range 0x20 through 0x7e.
Following this machine-friendly ASCII name there is an
option to add a control `<US>` followed by a human-friendly
UTF-8 name for displaying purposes.

The ASCII representation can be of any size, and
especially the human form can be a bit longer, but
any difference between instances must be made in the
first 32 characters.  The `<US>` character is not
considered part of the machine-friendly ASCII name.

Programs may choose to only display the machine-friendly
name, or its beginning of up to 32 characters, if so
desired.  An absent `<US>` may be treated like this, but
it is also possible to look for the last matching ASCII
name which did add a `<US>` and a friendly name.  These
semantics imply that a `<US>` addition may be used at
any time to update the human-friendly description for
a fixed machine-friendly ASCII name; this may however
lead to confusion, and is not advised.


## Hierarchy of Program Sets

Multiplexers are not just programs, they are managers
of programs.  They may manage such programs in a
hierarchy, either because the multiplexer thinks that
is a clever idea or as a result of one multiplexer
running within another multiplexer.

The outer level of a multiplexer presents a set of
programs.  Such a set has a current and previous
program, between which it can switch quickly.  Any
other switches are made by referencing a program
by name.  These switches indicate to the receiver
that the output sent towards it is moving from
one program to another.

This is used so that a multiplexer can forward the
output from multiple programs.  By explicitly switching
to another program, the receiver knows to process
the output as coming from the newly selected program.
The codes below explain how this is encoded in ASCII.

Each program has a current stream for input and another
one for output.  When switching programs, this current
stream is not changed.  For a multiplexer, this is
not of any influence, but for a receiver it means that
the current stream must be annotated with each program
whose output is processed.  Programs may change their
current stream, but only in the output direction.
Again, the aim is to be able to merge traffic from a
few connections in such a way that they can be split at
the receiving end.

Each program can be a parent to yet another program
set.  This is useful when a multiplexer wants to add
scoping or, more likely, when a multiplexer merges
traffic from another multiplexer into its own.  It
is easier to nest those multiplexers, than to rename
everything.  It also has a potentially better link
with intentions for access control, which can then
reference the nesting structure.

Separate commands exist for moving to a program to
its parent, or to the program set for which it is
the parent.  An "outside" multiplexer may have to
be a little creative with these codes.

It is important to understand that ASCII codes are
usualy single-byte entities, but we do build a few
structures, such as the `<SOH>` names and the `<EM>`
concept.  Care has been taken to support setups with
a minimum of state, because this state must usually
be kept when an "outside" multiplexer needs to
make a switch.


## Control Codes

A few ASCII control codes in use by mulTTY are ignored
by most programs.  We do not touch the ones that are
in common use, such as `<LF>`, `<BS>`, `<BEL>` and `<ESC>`;
mulTTY is not a terminal standard, but a set of control
codes that can be used orthogonally to multiplex and
redirect traffic between terminals.  Even it may be made
part of a terminal program, it could still be exploited
in an orthogonal client application or intermediate proxy.

These are the control codes that we believe are useful
for the kind of middleware control and multiplexing
that mulTTY adds:

  * `<SOH>` starts a name for an entity that will be
    mentioned afterwards.  See above for deails, and
    for `<UL>` to mix machine names and human names.
    The first control after the `<SOH>` name (and a
    possible `<UL>` string) determines what is being
    named and the name (up to but excluding `<UL>`,
    if any) is considered unique for its context.

  * `<STX>`, `<ETX>`, `<EOT>` and `<SUB>` control program changes,
    but only in the input/output direction it is sent.
    Unknown names may be created on the fly or ignored,
    depending on sender/receiver semantics.  Programs
    may be removed, though the real actual effect
    again depends on sender/receiver semantics.
    Preceding `<SOH>` naming can be used to signal
    the name concerned, to replace relative program
    references with absolute ones.

    These codes are not usually sent by a program,
    which is mostly concerned with streams.  These
    codes are used for multiplexing programs.  Since
    multiplexers are also programs, multiplexing can
    be nested and the program names will inherit the
    nesting structure.  For every set of programs at
    every nesting level or branch being managed, the
    state to maintain includes a current program and
    the previous program.  This is needed to implement
    these device controls.  In addition, there is a
    globally active program set.

    `<ETX>` moves from the current program set to
    its direct parent set.  Normally, it moves to the
    current program in that set, but an `<SOH>` name
    might cause a search in the parent's program set.

    `<EOT>` ends the current program, after optionally
    switching to an `<SOH>` named program from the
    current program set.  It also ends any programs for
    which this is a parent, directly or indirectly, and
    it implicitly ends the streams for each ended program.

    After this, there is no current program, even though
    there is still a current program set to choose from.
    This recognisable situation can be used to send an
    `<EM>` code as specified below, but now applicable
    to the entire program, possibly giving a reason
    for ending the program.

    `<STX>` moves from the current program to the
    (child) program set whose parent it is.  When
    an `<SOH>` name is prefixed, a search for that
    name will be performed next.

    `<SUB>` swaps the current program with another
    program within the current program set.  If an
    `<SOH>` name is given, the other program is
    defined by its name, and otherwise the previous
    program is used.

    Names that are being referenced but that are not
    known yet are taken to be introductions of new
    names.  This is even true for `<EOT>`, but it is
    likely to implement compatible behaviour that
    skips a few steps.  After most commands a new
    program is set as the current; it is only set
    to undefined after `<EOT>`.  After all commands
    the previous current program has moved and, for
    this program set, made the new nameless target
    for a next `<SUB>` control.

  * `<EM>` marks the end of a stream of the current
    program run.  After `<EOT>` there is no current
    program but then the special usecase is for the
    terminated program as a whole.  When ending a
    stream, the current program returns to the
    default stream without affecting `<SO>` or `<SI>`
    status (described below).

    When no `<SOH>` name precedes `<EM>`, it is taken
    to mean that nothing interesting was to be said
    about ending the stream or program.  Usually,
    that means all went as expected.  If an `<SOH>`
    name precedes `<EM>` however, it is considered
    to report a reason for ending the stream or
    program.  When a `<US>` is part of the reason,
    it separates a machine-readable form from a
    human-friendly text.  The machine-readable form
    should be portable, and so the POSIX error codes
    cannot be used.  The `com_err` library provides
    numeric values that *are* portable, and can be
    used in this position as decimally written
    signed 32-bit integers, of which 0 represents
    OK.  The names of POSIX errors can also be used
    in their uppercase string forms, because these
    names are portable (but note that they are not
    always unambiguous, and not always canonical).

    Multiplexing can be nested, and because of that
    its interaction with `<EM>` and in general longer
    codes can be confusing; in general, during any
    byte sequence there may be a need to switch to
    another program, but for the locally generated
    `<EOT><SOH>xxx<EM>` that would only be caused by
    parent environments.  When these return in the
    current environment, the relaying of the `<SOH>`
    should continue as if nothing happened.  Then,
    when `<EM>` ends it, the retained information
    about current programs and their streams is
    still available, and can be used to detect how
    to interpret the information.
    **TODO:** Can parents jump in for a named child?

  * `<SI>` and `<SO>` switch to another representation
    under the common ASCII standards, with the
    default as if `<SO>` had been given.  Occurrences
    are shifting to a red ink ribbon instead of
    black, or shifting to another character set in
    Xterm and Linux Console.  We make an orthogonal
    extension by prefixing an `<SOH>` name for a
    stream, or assuming the default streams when no
    name precedes it.  New stream names will be created.
    For output, the default is `stdout` and there
    is an additional `stderr` that can be selected
    with an `<SOH>stderr` prefix to either `<SO>` or
    `<SI>` (the best choice is whatever the current
    shift status is).  For input, the default is
    `stdin` and there may also be a `stdctl` with
    `<SOH>stdctl` prefixed to either `<SI>` or
    `<SO>` control.

  * `<DLE>` is used for binary content that wishes to
    include codes that would otherwise be interpreted
    as control codes.  The customary pattern is that a
    code in the range 0x00 to 0x1f plus 0x7f can be
    escaped by XOR with 0x40 and then prefixed with
    `<DLE>`.  This means that `<DLE>G` is sent to avoid
    0x08 being interpreted as `<BEL>`.  Applications that
    carry binary content, such as SASL, also need to
    escape `<NUL>` as that might otherwise be removed
    as a gratuit filler.  Applications that attach to a
    Telnet port would be wise to escape 0xff to `<DLE>`
    and 0xbf, to not trigger command interpretation,
    also by XOR with 0x40 and prefixing with `<DLE>`.

A few extra ASCII controls may be used in streams that
define an interpretation to them.  These too are subject
to `<DLE>` escaping when they occur.

  * `<STX>` and `<ETX>` and possibly `<SOH>`, `<EOT>` and
    `<ETB>` are intended to frame information, and can be
    used freely when this is meaningful for interpretation
    of a (typed through naming) stream.  Using this,
    `<STX>` starts framing information and either `<STX>`
    or `<ETX>` ends any previous framing.  Text before
    a first `<STX>` and between `<ETX>` and `<STX>` is
    not framed and would be treated differently by the
    stream.  Being able to frame data allows markation
    of a piece of text as something to be parsed as
    a unit, and for which to raise syntax errors
    when it does not seem fit.  Note that framing
    is per stream and a frame may be split due to
    a multiplexer's switch to another frame.

  * `<FS>`, `<GS>`, `<RS>`, `<US>` are separators for fields,
    groups, records and units.  They may be used to
    add some structure to a stream's flow.  Again, the
    stream name suggests what meaning these separators
    have.  Ideas that come to mind include tables, menus
    and user dialogs, all data with some structure, though
    not necessarily as tight as XML, JSON, CBOR or ASN.1.

  * `<SYN>`, `<ENQ>`, `<CAN>`, `<ACK>` and `<NAK>` can
    add some structure to protocols, which differs
    from structuring text or data.  Only the `<ENQ>` is
    used in Xterm statusses via stdio, but other
    streams can use these signals in their own way.
    An idea that comes to mind is better automation
    for authentication flows.  We will indeed define
    how SASL can use such things.


## Reference

This has loose correspondance with ITU Recommendation T.50,
*International Reference Alphabet*, as revised in 1992,
formerly known as *International Alphabet No 5* or IA5.

We interpret the general and somewhat vague descriptions
in this specification with more specificity:

  * We use `<SOH>` liberally as a naming prefix, not just
    to preface text.  This is not a current practice so
    there would be no clashes, but it is a bit creative.
    Programs that do not recognise `<SOH>` may ignore it
    and end up printing the content that follows (which
    is not proper handling of this ASCII code).

  * We prefix `<SI>` and `<SO>` with a name to vary the
    multiplexing operation, but other than that we retain
    its meaning and should see no changes, even without
    removal of multiplexing, other than the possible
    printing of unrecognised `<SOH>` name prefixes.

  * We do not use the `<DCx>` codes in mulTTY, but still
    require that streams treat it like we did.  This
    means that the applications running these streams
    will escape these codes and not pass them in the
    raw to a device which may want to interpret them.
    A common example is the XON/XOFF signaling with
    `<DC1>`/`<DC3>` codes.

  * We use `<STX>` to enter a child and `<ETX>` to go
    back up to the parent.  We use `<SUB>` to switch to
    another program at the same leven and `<EOT>` to
    terminate a program.


