# Experiments with lrzsz


## Protocol Capture

When `sz` starts, it actively sends out messages.
After a few attempts, `sz` quits.

```
shell$ sz /etc/modules | hd
00000000  72 7a 0d 2a 2a 18 42 30  30 30 30 30 30 30 30 30  |rz.**.B000000000|
00000010  30 30 30 30 30 0d                                 |00000.
Retry 0: Timeout on pathname
                            8a 11  2a 2a 18 42 30 30 30 30         ..**.B0000|
00000020  30 30 30 30 30 30 30 30  30 30 0d                 |0000000000.
Transfer incomplete
                                            8a 11 18 18 18              .....|
00000030  18 18 18 18 18 18 18 08  08 08 08 08 08 08 08 08  |................|
00000040  08                                                |.|
00000041
```

When `rz` starts, it actively announces that it wants to receive.
Note that `rz` does not give up, so you would normally start that first.

The `<CAN>` code 0x18 is represented as `<ZDLE>` in the ZMODEM protocol.
It sends `<DC1>` with the meaning of `<XON>`.

The sequence `<ZDLE>B` starts a hexadecimal header:

```
			  Figure 4.  HEX Header

      *	* ZDLE B TYPE F3/P0 F2/P1 F1/P2	F0/P3 CRC-1 CRC-2 CR LF	XON

(TYPE, F3...F0,	CRC-1, and CRC-2 are each sent as two hex digits.)
```

So, we have:

  * An opportunistic attempt to start the `rz` command remotely
    (but remote command execution will often be blocked)
  * `TYPE` is `00` for ZRQINIT (request fast RINIT from reader)

```
shell$ rz | hd
rz waiting to receive.
00000000  2a 2a 18 42 30 31 30 30  30 30 30 30 32 33 62 65  |**.B0100000023be|
00000010  35 30 0d 8a 11                                    |50...
Retry 0: Got TIMEOUT
                         2a 2a 18  42 30 31 30 30 30 30 30        **.B0100000|
00000020  30 32 33 62 65 35 30 0d  8a 11                    |023be50...
Retry 0: Got TIMEOUT
                                         2a 2a 18 42 30 31             **.B01|

00000030  30 30 30 30 30 30 32 33  62 65 35 30 0d 8a 11     |00000023be50...
                                                        2a                  *|
Retry 0: Got TIMEOUT
00000040  2a 18 42 30 31 30 30 30  30 30 30 32 33 62 65 35  |*.B0100000023be5|
Retry 0: Got TIMEOUT
00000050  30 0d 8a 11                                       |0...
00000050              2a 2a 18 42  30 31 30 30 30 30 30 30       **.B01000000|
00000060  32 33 62 65 35 30 0d                              |23be50.
rz: caught signal 15; exiting
00000060                       8a  11 18 18 18 18 18 18 18          .........|
00000070  18 18 18 08 08 08 08 08  08 08 08 08 08           |.............|
0000007d
```

From this information, we have:

  * `TYPE` is `01` for ZRINIT
  * `F0` sets the flags `CANFDX`, `CANOVIO`, `CANLZW` (full duplex, not blocked
    by disk I/O, LZW compression)


## How to Quit

Neither quits on receiving Ctrl-D, presumably because it is prepared
to receive raw binary input.

Both quit immediately on `< /dev/null` with messages like incomplete
input and (immediate) timeout.  They are clearly not made to handle
empty input (which is a suggested improvement).


## Sockets

It's fathomable to make sockets (or named pipes) available for either,
in a Z-MODEM pool directory possibly for the user or even for a group,
and post the message there.  Terminals running zesTTY clients could
connect to the pool instead of running `sz` or `rz` locally.

Future versions of this idea could use other connectivity, including
over the Internet via a (protected) TCP/IP socket.  This would allow
one member of a group to run `rz` while the other issues `sz` to
transfer a file to their group mate.  This would be for instant
exchange, which in itself is a wonderful tool.  It does start to
sound like FTP a lot.  Or HTTP, or AMQP 1.0 for example.  It could
integrate with each, where `rz` and `sz` handle the terminal side
of an interaction.


## Pool

Imagine a variable `ZPOOL` defaulting to `~/.zpool` which would be
tried at the user, as interpretation of the tilde.  This directory
holds a socket in both directions, and tries hard not to miss the
other.  The socket closes when no more traffic is posisble.

This may be overridden by an explicit pool setting parameter.  To
salTTY though, not to `sz` or `rz`, because those are endpoints and
the pool is about connection those.

Future setups may use SIP to connect to another party via ZMODEM.
And it may use KIP to pass an encryption key to such remotes.
That's another project, though!
