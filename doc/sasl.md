# Terminal Support for SASL

> *Due to streams in mulTTY, with names to indicate their
> interpretation, we can now work on better authentication
> exchanges.  We explain how we can work SASL into a
> multiplexed terminal flow.*

Have you ever seen how login is automated in old
scripts?  It grabs strings like `ogin:` and
`assword:` and inserts characters in response to it.
The missing first character is to avoid case sensitivity.
And TTY technology still has no support for doing this
with more structure.  One result is that we are still
facing the challenge to enter a literal password, and
to do this over and over again.  Let's get over that.

Having automation-friendly support for authentication
means that we can pop up a question for credentials if
and when we need it, of which the start of a session
is just an example.  We might offer to change identity,
confirm an identity to match a more stringent ACL rule,
and so on.  The TTY can use more flexibility, certainly
to accommodate modern concepts like containers.

Furthermore, the program multiplexing in mulTTY reveals
with complete clarity where any challenge tokens come
from and where the response tokens should be sent.  It
is the hierarchical structure of program multiplexing
that supports a stepwise reduction along a path of trust
about the remote party in the SASL exchange; each parent
is trusted to properly incorporate a child that in turn
receives a certain level of trust to act responsibly
with SASL tokens.


## Adding Streams for SASL

This is an enhancement to the interpretation of a stream
of textual data on `sasl` from client to server and back,
using control codes that are not otherwise used.  The
same name is used in both directions because the codes
in the two directions are distinct; this allows even
bidirectional authentication over `sasl` streams.

The codes defined below might have arisen on the normal
`stdin` and `stdout` flows, and it is still possible to
capture them from those for passing on as `sasl` streams.
At this time however, we are interpreting and possibly
causing conflicts of opinion on meaning.  Though we
would not know which, in practice, it is impossible to
know for sure, so we shall not take that view.


## SASL in Secure Contexts

Note that SASL is not designed to be passed in the
clear, for certain reasons:

  * Backward-compatible SASL mechanisms can be weak,
    supporting even the replay of captured traffic

  * SASL mechanisms may leak identity information and
    so destroy privacy and perhaps even security

  * There might be man-in-the-middle attacks that modify
    authentication attempts while in transit

  * SASL Authentication and the actual traffic using
    the gathered access rights must be tightly bound
    together

One way of avoiding these problems is with local system
control, if the SASL exchange falls inside one machine.
A popular alternative in most protocols is to have SASL
run inside a TLS wrapper.  Yet another manner that could
make sense for terminal sessions is to use it under the
OpenSSH protocol.  (OpenSSH might even be extended with
an authentication mechanism for SASL.)

Certain SASL mechanisms are capable of relaying their
own identity information.  A computer account with its
controlling domain, an authenticating TLS client and an
authenticating OpenSSH client all offer identity, though
it is not enforced in all cases.  The general answer to
use that in SASL is the `EXTERNAL` mechanism, defined to
look at whatever identity information the context can
provide.  Servers able to make such a lookup could offer
it and clients interested in saving themselves the extra
work (or desiring to separate identity concerns to an
outer layer) could then choose it.


## Client probing: Asking the server for SASL

The client may probe the server by sending an empty
message to its `sasl` stream.  This empty client
message is just `<SYN><ETB>`.  If the server can
understand this stream, it may respond to the client
over the `sasl` stream.

The probe as well as a possible `<SOH>` name prefix
are just hints to the server.  It may deal with it
in any way it wants, including ignoring it or
answering with a different kind of SASL setup.  The
client must therefore not assume that one probe
having failed implies that all other probes would
also fail.  It should feel free to try any probes
that it deems likely.


## Setup phase: Stop/Start SASL Operation

The server usually requests authentication via SASL,
and it does this by sending three fields with <US>
separation: (1) the host name to log into, (2) the service
to address on the host and (3) a list of mechanism names
that it supports.  Mechanism names are uppercase and short,
and they do not contain a space.  Many protocols send
the mechanism list with space to separate the elements,
and so does mulTTY.  The mechanism list is terminated
with `<ETB>`.

The mechanism list needs to be introduced.  This is
done by prefixing the list with `<ENQ><CAN>` which can
be interpreted as a reset of any prior security status
and a fresh start of a SASL session with an equiry into
the client's ability to authenticate.

To completely ban a client, the server may send the
form `<ENQ><CAN><ETB>`, which explicitly lists no SASL
mechanism names for client SASL authentication.


## Teardown phase: Cancel SASL Operation

The client may cancel any SASL exchange in progress,
either before or during negotiation or afterwards,
by sending `<SYN><CAN><ETB>`.  If it is done afterwards,
then it may be considered a logout; rights will be
abolished and if a security layer was active it
will be closed.  New login attempts are possible.

The server may cancel any SASL exchange in progress,
either before or during negotiation or afterwards,
by sending `<ENQ><CAN><ETB>`.  Again, the client is
logged out after this exchange but may retry login.

When a session does not exist, a `<CAN>` has probably
been sent superfluously and it must be silently ignored.


## Passing SASL Tokens over ASCII

SASL Tokens are general binary strings, and may include
control codes that are meaningful to ASCII.  One of these
is `<NUL>` that may be added or removed ad libitum
according to generic ASCII definitions.  There are also
control codes used in mulTTY, and 0xff that me be confused
with the Telnet command prefix.

Any such codes can be protected with `<DLE>` escaping.
The fact that this alters the number of bytes being
passed hardly matters, as token sizes are variable in
SASL.  When reading back SASL sequences, any inserted
`<NUL>` must not be considered part of the SASL Token
and must be removed.  Once `<DLE>` escaping is removed,
binary content has been recovered and meaningful
occurrences of `<NUL>` must be kept as they are.

When a SASL Token is sent, we precede it with `<US>`.  If
no token is sent, which is distinct from sending an empty
SASL token, we send nothing and continue with the context,
described per direction below, which appends something
that is never `<US>`.

Note that the `<US>` character preceding a SASL Token
is not subjected to `<DLE>` escaping, which is only
used for the internals of the SASL Token.


## Server Challenges

A server challenge occurs after mechanism selection,
described below, has taken place.  The actual server
challenge starts with the `<ENQ>` character.  Unlike
the mechanism list, the next character is not `<CAN>`.

It may then pass a SASL Token if the SASL mechanism
requires one at that point.  When a token is present,
it is preceded with a `<US>` character and it holds
none of the characters defined as follow-ups below.

If the server knows the outcome it may also report
`<ACK>` or `<NAK>` to relay the SASL exchange result
to have succeeded or failed.  As always, this is
preceded with an `<ENQ>` character to signify a
server message and finished with an `<ETB>` code.

An optional extra token is possible according to
SASL, though it does not seem to be used at all
in practice.  But an extra token might now be
inserted, which according to SASL may only be done
after `<ACK>` but not `<NAK>` or when no status
is ready to be relayed yet.

The server challenge ends with an `<ETB>` code.


## Client Response

The first client response differs from any further
ones by selecting a mechanism from the list offered
by the server.

Client messages always start with `<SYN>`
character, which makes it easy to distinguish the
client and server messages.  As a result, it is
even possible to use bidirectional SASL or, as a
practical consequence, the same stream name `sasl`
can be used in both directions.

During the initial response, the client must choose
a mechanism from the list sent by the server.
This is sent as a `<CAN>` control followed by the
selected mechanism name and a space.  This prefix
must not be included in any client message but the
initial response.

The client may send a SASL Token if the mechanism
needs this.  For instance, a server-initiated
mechanism may not need it in the initial client
message.   The customary `<US>` control is inserted
only if a token is sent.

The client response ends with a `<ETB>` code.


## Authorisation follows SASL Authentication

SASL authenticates a user, possibly modifies it to
an authorisation identity, but all that the server
then knows is who it is dealing with.  Though a
specific context could provide hard-wired access
rights, there is a lot to be said for an ACL to
specify more flexible facilities:

  * Groups may share resources, but not all.
  * Roles may assign workers the access to certain
    resources, for the duration of their work.
  * You may be using aliases to separate types of
    work and to control the access required per task.
  * External parties may not have local accounts,
    but still need to be granted access.
  * Temporary sessions may be shared for the duration
    of a project or even just a phone call.  Ever
    wanted to share a terminal session with a friend
    to see how their code was behaving?

ARPA2 facilitates these things with a general
[arpa2access](https://gitlab.com/arpa2/arpa2common)
library, including central control through either
an `a2rule` command, distributed to a Policy Rules
database.  This is a small endpoint for the rich
design of an
[InternetWide Architecture](http://internetwide.org/blog/2016/06/24/iwo-phases.html)
intended for shared identities that we can use
with realm crossover to gain access elsewhere.
One of the crossover mechanisms proposed is based
on SASL, and named `SXOVER-PLUS`.


## Working Phase: Authorisation Feedback

When a server wants to indicate that a client is not
authorised for a certain action, it can send `<NAK>`,
possibly prefixed with `<SOH>` naming.

When a server wants to make explicit that a client
is (now) authorised for a certain action, it can send
`<ACK>`, possible prefixed with `<SOH>` naming.  This
may be done at any time.  Clients may learn from this
that a failed attempt in the past is now possible.

The `<SOH>` naming is intended to connect such events
caused by authorisation access.
Names are intended to be humanly readable, though a
`<US>` can be used between an unreadble unique textual
value and a readable human string.

One or more of these notifications may be concatenated.
The last one should be followed with `<ETB>`.

Note that this is different from the `<ACK>` or `<NAK>`
included in the last SASL response from the server.
These indicate *authentication* success or failure,
not *authorisation* success or failure.  Syntactically,
the former only occurs between `<ENQ>` and `<ETB>` and
the latter only occurs outside it.


## Example: SASL ANONYMOUS with Authorisation Updates

The server decides it wants to ask the client to
authentication, but it does offer ANONYMOUS for a
client who is intent on using only public resources.

These examples include an `<SO>` shift to the `sasl`
stream.  When done, it sends another `<SO>` to
return to `stdin` or `stdout`.  Either or both of
these shifts may be unnecessary depending on context.

The server initiates the SASL procedure.

```
S->C: <SOH>sasl<SO><ENQ><CAN>SXOVER-PLUS ANONYMOUS<ETB><SO>
```

A client that is unaware of SASL or mulTTY streams
is likely to ignore the special characters and it
may (incorrectly) dump the ASCII content:

```
saslSXOVER-PLUS ANONYMOUS
```

This would be harmless, at best it could raise some
questions with users and make them discover the
extra option being offered.  Let's assume we have a
potent client, which chooses ANONYMOUS to send a
token that holds a tracing name which happens to be
readable as plaintext:

```
C->S: <SOH>sasl<SO><SYN><CAN>ANONYMOUS bakker@orvelte.nep<US><ETB><SO>
```

The server may explicitly accept, though this is not
always done with the ANONYMOUS method.  It does not
send a SASL Token at all, which is why there is no
`<US>` to precede an (empty) SASL Token:

```
S->C: <SOH>sasl<SO><ENQ><ACK><ETB><SO>
```

At any time later, the SASL server may discover that
this user is granted access to a particular service,
and it could report that to the SASL client:

**TODO:SOH-PREFIX-REMNANTS?NO-ENQ?**

```
S->C: <SOH>sasl<SO><SOH>webtty<ACK><ETB><SO>
```

Perhaps another resource gets withdrawn due to an
expiration or ACL change:

**TODO:SOH-PREFIX-REMNANTS?NO-ENQ?**

```
S->C: <SOH>sasl<SO><SOH>ldap<NAK><ETB><SO>
```

## Example: SASL SCRAM

We follow the example in
[Section 5 of RFC 5802](https://tools.ietf.org/html/rfc5802#section-5)
with the remark that the base64 coding used here is part of this
specific SASL mechanism, not of its ASCII embedding, but it does
simplify token handling and representation to not have to use
`<DLE>` escaping.

```
S->C: <SOH>sasl<SO><ENQ><CAN>SCRAM SXOVER ANONYMOUS<ETB><SO>
C->S: <SOH>sasl<SO><SYN><CAN><US>SCRAM n,,n=user,r=fyko+d2lbbFgONRv9qkxdawL<ETB><SO>
S->C: <SOH>sasl<SO><ENQ><US>r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,
      s=QSXCR+Q6sek8bf92, i=4096<ETB><SO>
C->S: <SOH>sasl<SO><SYN><US>c=biws,r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,
      p=v0X8v3Bz2T0CJGbJQyF0X+HI4Ts=<ETB><SO>
S->C: <SOH>sasl<SO><ENQ><US>v=rmF9pqV8S7suAoZWja4dJRkFsKQ=<ACK><ETB><SO>
```

To understand the ASCII inserts:

  * `<SOH>sasl<SO>` in the beginning switches to the `sasl` stream.
  * `<SO>` at the end switches back to the default stream, `stdin` or `stdout`.
  * `<ETB>` is used to mark the end of SASL messages independently of `<SO>` to switch streams.
  * `<ENQ>` is always used as a starter by the server and never by the client.
  * `<ENQ><CAN>` is the start of a method list, always sent by the server.
  * `<SYN>` is always used as a starter by the client and never by the server.
  * `<SYN><CAN>SCRAM ` starts the initial response, before the SASL Token `n,,n=user,xxx` is sent.
  * `<US>` precedes a SASL Token and indicates that a SASL Token is to follow.
  * `<ENQ>xxx<ACK>xxx<ETB>` indicates successful authentication, but says nothing about authorisation.


## Technical Infrastructure

The SASL implementation can be any; mulTTY currently uses
[Quick-DiaSASL](https://gitlab.com/arpa2/Quick-SASL/-/blob/master/include/arpa2/quick-diasasl.h)
interface to connect to a SASL implementation in an independent program.
One suggested implementation of this protocol is a Diameter service passing
SASL tokens to a client's domain name, thus establishing Realm Crossover
for any user logging in.  The use of `SXOVER-PLUS` during Realm Crossover
also assures end-to-end encryption of the SASL tokens in transit.

