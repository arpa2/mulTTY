.TH PAM_SALTTY_DIASASL 8 "August 2022" "ARPA2.net" "System Administration"
.SH NAME
pam_saltty_diasasl \- Module for SASL login against backend
.SH SYNOPSIS
.PP
.B pam_saltty_diasasl \fIoptions\fR
.RE
.SH DESCRIPTION
The purpose of
.B pam_saltty_diasasl
is to support commandline login with SASL against a Quick-DiaSASL
backend that may support Realm Crossover.  Authentication yields
\fIusername\fB@\fIdomain\fB.\fIname\fR user names.
.PP
This module is part of the
.IR mulTTY "(7)"
framework for ASCII encoding of a separate stream for SASL,
wherein an encoding for the SASL exchange communicates with a
client-side SASL handler such as
.IR saltty "(1)."
Compared to password entry, there are several advantages:
.IP "\fBAutomation support\fR"
The practice of grabbing \fBogin:\fR and
\fBassword:\fR strings is highly localised and can never be
as flexible as explicit annotation for authentication.
.IP "\fBBetter authentication\fR"
SASL mechanisms can select a higher
level of cryptographic security, and may benefit from a
single-signon system such as Kerberos to derive credentials
from a daily central login procedure.
.IP "\fBOn demand authentication\fR"
It is possible to require (additional) authentication at
the time that a privileged operation is tried.  Good
examples are the
.IR sudo "(8)"
command and confirmation of the use of key material, such
as for decryption or signing, or for login to another server.
.PP
The use of Quick-DiaSASL is the reason that Realm Crossover
for SASL is supported.  This protocol may be handled by a
backend that runs locally or remotely, and that may work from
a simple SASL database or use Diameter to rely on a foreign
domain's identity provider.
.IP "\fBCentral Accounts\fR"
The delegation of SASL to a backend enables the use of
network-central account management.  This tends to be very
helpful with the setup or retraction of accounts in many
places at once.  (It is also possible to have a backend on
the local host and not use centralisation.)
.IP "\fBRealm Crossover\fR"
Implementations of Quick-DiaSASL may use a network-local
password database, or it may connect to Diameter, which
can rely on an identity provider in a foreign domain.  The
Diameter server would exploint DNSSEC, DANE and TLS
technologies to assure a \fIdomain\fB.\fIname\fR and ask its
identity provider declared in SRV records.  This identity
provider is then trusted to provide a \fIusername\fR under
its control in return for proxying SASL over Diameter.
The local Diameter reports \fIusername\fB@\fIdomain\fB.\fIname\fR
over Quick-DiaSASL as the trusted name for a foreign user.
.IP "\fBAccess Control\fR"
After authentication, it is possible to have flexible
Access Control rules to decide on local rights.  The present
version of the module logs in the user when their
\fIusername\fB@\fIdomain\fB.\fIname\fR occurs in \fB/etc/passwd\fR
or \fB@\fIdomain\fB.\fIname\fR or \fB@.\fIname\fR (and so on)
or \fB@.\fR which is the most general form for accounts with
Realm Crossover.  Note that all these forms are distinct from
common program accounts on the host.
.SH OPTIONS
All configuration options follow the form \fIname\fB=\fIvalue\fR.
The following \fIname\fRs ar defined:
.TP
.B hostname
The fully qualified host name for this system.  This is used in
some credentials, such as Kerberos ticket names.  When not given,
it will be requested from the system, but an error is reported
if this fails or if it lacks a domain part.
.TP
.B domain
The domain name under which SASL authenticates.  It is also known
as the security realm.  This is normally derived from the
\fBhostname\fR setting by removing the part up to the first dot.
If this is not desired, for example because the realm is really
just the host itself, then the domain may be overridden with this
option.
.TP
.B service
The name of the service under which authentication is done.  This
defaults to \fBhost\fR but may be overridden here.  Programs that
incorporate SASL authentication, such as an IMAP server, will do
that directly and not via an ASCII connection, so their settings
need not conflict with the setting in this place.
.TP
.B diasasl_ip
The IPv4 or IPv6 address of the Quick-DiaSASL backend.  When not set,
this defaults to \fB::1\fR for the local host.
.TP
.B diasasl_port
The TCP port to which the Quick-DiaSASL backend service is bound.
This value has no default, so it must always be given.
.SH MODULE TYPES PROVIDED
This is only an \fBauth\fR service module.
.SH RETURN VALUES
.TP
PAM_AUTH_ERR
Authentication failure.
.TP
PAM_BUF_ERR
Out of memory.
.TP
PAM_NO_MODULE_DATA
The configuration was wrong; check the log files for a descriptive error.
.TP
PAM_PERM_DENIED
Basic denial of access
.TP
PAM_SERVICE_ERR
Problems with the connection to the Quick-DiaSASL backend.
.TP
PAM_SUCCESS
Authentication succeeded.
.SH EXAMPLES
An example line in a PAM configuration file would be:
.sp
.RS 4
.nf
auth require pam_saltty_diasasl.so host=shell.example.net diasasl_port=3000
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/mulTTY/
project page for mulTTY.  We use its issue tracker
to track bugs and feature requests.
.SH COPYRIGHT
.PP
Copyright \(co 2022 Rick van Rein, ARPA2.net.
.PP
This work was partially funded by NLnet,
a public interest fund geared towards open source
excellence.
.SH "SEE ALSO"
.IR mulTTY "(7), " saltty "(1)."
.sp
Internet Drafts document draft-vanrein-diameter-sasl.
