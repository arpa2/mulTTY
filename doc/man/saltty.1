.TH SALTTY 1 "June 2022" "ARPA2.net" "User Commands"
.SH NAME
saltty \- Handle SASL authentication as part of a mulTTY flow
.SH SYNOPSIS
.PP
.B saltty
[\fB-c\fR] [\fB--\fR] \fIcommand\fR...
.PP
.B saltty
[\fB-n\fR \fIresourcename\fR [\fB-k\fR \fIservicekey\fR]]
[\fB-d\fR \fIhost\fB:\fIport\fR]
.RS
[\fB--\fR] [\fIcommand\fR...]
.RE
.PP
.B saltty
[\fB-u\fR \fIuser\fR[\fB@\fIdomain\fR]]
[\fB-d\fR \fIhost\fB:\fIport\fR]
[\fB--\fR] [\fIcommand\fR...]
.SH DESCRIPTION
The command
.B saltty
with option \fB-c\fR handles the client side, or otherwise the
server side of a mulTTY program run with SASL streams.  The
client listens for SASL challenges by the server.  In all
cases, it runs a command.
.PP
The general idea behind
.IR mulTTY (7)
is to use one ASCII connection to carry multiple streams
and switch between them by name.  The streams added for
.B saltty
are named
.BR sasl .
These streams are handled with respect to the command that
a client runs, or it is handled by sending those streams
over the (pseudo)terminal on which
.B saltty
runs.  Any server program that wants (further) authentication 
of a client may initiate a SASL exchange over its terminal
and expect the client to respond.  There is no reason why
the parties could not use
.IR nc (1)
or
.IR ssh (1)
as an intermediate, as long as it is a terminal connection.
Note that programs such as
.IR mosh (1)
or
tmux (1)
are no suitable transports for mulTTY flows.
.PP
SASL authentication offers a greater diversity of login
mechanisms, most of which are stronger than plain password
entry.  Automation is also greatly simplified, by making
the authentication exchange stand out explicitly, rather
than screen scraping in any automation attempt.  Finally,
.IR mulTTY (7)
streams can be separated out to different handlers, which
can benefit the separation of secrets from applications,
even under different protection levels.
.SH OPTIONS
The \fB-c\fR option explicitly selects client mode.
Server mode is explicitly selected by the other options.
It is possible to end the interpretation of options using
\fB--\fR as an explicit marker without any meaning of its own.
.TP
.BI -c
Select client mode.  The \fIcommand\fR runs as a mulTTY
program, and one of the streams used may be \fBsasl\fR,
over which a challenge/response interaction may be
started.
.TP
.BI -k " servicekey"
Apply ARPA2 Access Control using this service key in
hexadecimal notation.  A domain owner may supply such
a service key as a way to grant access using the
Rules Database managed with
.IR A2rule (8).
.IP
If this option is not given, the value can also be taken
from an environment variable \fBARPA2_SERVICEKEY\fR
which would be set to the \fIservicekey\fR value, again in
hexadecimal form.
.IP
Use \fB-k\fR with \fB-n\fR; only available when compiled in.
.TP
.BI -n " resourcename"
Apply ARPA2 Access Control usig this resource name.
Permissable forms are set for the service key, and
a lookup will be made while taking the authenticated
name (after SASL authentication) into account for
access to this resource name.  This form is flexible
enough to grant fine-grained Access Control, hence
the use of a Rules Database.
.IP
Use \fB-n\fR with either \fB-k\fR or the
\fBARPA2_SERVICEKEY\fR environment variable; only
available when compiled in.
.TP
.BI -d " host" : port
Use the indicated \fIhost\fR and \fIport\fR to locate a
SASL oracle via the Quick-DiaSASL protocol.  The \fIhost\fR may
be an IPv4 address or an IPv6 address in square brackets.
The SASL orcale can support Realm Crossover for SASL
if it connects with a Diameter SASL client.
.IP
This option can help to make authentication manageable
for even the simplest environments, including containers
and PXE-booted terminals.  These environments need no
local knowledge beyond what resolver to use, like in DNS.
.TP
.BI -u " user"
.TP
.BI -u " user" @ domain
Request access for a local \fIuser\fR or possibly a local
\fIuser\fB@\fIdomain\fR account.  In both cases, they are
resolved through the local account system, but with the
\fB@\fR part in the user name, this does not include the
customary host-local accounts, and may offer access to an
array of domain-dedicated services, possibly mounted or
created on demand.
.IP
TODO: Not yet implemented.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/mulTTY/
project page for mulTTY.  We use its issue tracker
to track bugs and feature requests.
.PP
Something that may be useful to try is the
integration of the \fBsasl\fR stream in a
.IR pam (3)
module, named
.BR pam_diasasl.so
or
.BR pam_saltty.so
and using the
.IR pam_conf (3)
mechanism to request token data after being prompted with an ASCII text.
This would make tools like
.IR sudo (1)
also offer SASL authentication in a manner that
.B saltty
could respond to.
.SH COPYRIGHT
.PP
Copyright \(co 2022 Rick van Rein, ARPA2.net.
.PP
This work was partially funded by NLnet,
a public interest fund geared towards open source
excellence.
.SH "SEE ALSO"
.IR mulTTY "(7)"
with website
.IR http://gitlab.com/arpa2/mulTTY .
.PP
Realm Crossover for SASL is based on the Diameter SASL
embedding as documented in
.BR draft-vanrein-diameter-sasl .
.PP
ARPA2 Access Control uses a Rules Database managed with
.IR a2rule (8).
This is part of the ARPA2 Common package on
.IR http://common.arpa2.net/ .
.PP
The work on Quick-DiaSASL is part of the
ARPA2 Quick-SASL package on
.IR http://quick-sasl.arpa2.net/ .
.PP
The Quick-DiaSASL protocol used to centralise
SASL authentication to a local backend service is on
.IR https://gitlab.com/arpa2/quick-der/-/blob/master/arpa2/Quick-DiaSASL.asn1 .
