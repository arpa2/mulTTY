.TH PRETTY 7 "June 2022" "ARPA2.net" "Programmer's Manual"
.SH NAME
mulTTY \- ASCII annotations to distinguish programs and I/O streams.
.SH SYNOPSIS
\fR[\fB<SOH>\fIstream\fR[\fB<US>\fIdescription\fR]]\fB<SI\fR>|\fB<SO>
.PP
\fR[\fB<SOH>\fIreason\fR[\fB<US>\fIdescription\fR]]\fB<EM>\fR
.PP
\fR[\fB<SOH>\fIprogram\fR]\fB<ETX>\fR|\fB<EOT>\fR|\fB<STX>\fR|\fB<SUB>\fR
.PP
\fB<DLE>\fIescaped\fR
.PP
.SH DESCRIPTION
The
.IR ASCII (7)
table offers more control codes than used in an everyday program.
Using the extra codes in a systematic manner,
.IR mulTTY (7)
can annotate a flow of ASCII characters in such a manner that
streams such as \fBstdout\fR and \fBstderr\fR can be combined
and later taken apart.
.PP
One level up, it allows the same approach
for programs, which are considered to be part of a hierarchy
managed by what
.IR mulTTY (7)
calls a multiplxer.
Multiplexers are programs that start multiple other programs,
such as a shell or an
.IR init (1)
program.  They can be nested and this structure is retained by
.IR mulTTY (7)
flows.
.PP
Streams and programs are identified by small strings that are
inserted into an ASCII flow before their data flow.  There are
control characters to introduce such context switches, which can
be parsed upon reception, and used for purposes such as
.IR pretty (1)
printing with a colour per program and bold error output.
.PP
On top of the standard stream names for \fBstdin\fR, \fBstdout\fR
and \fBstderr\fR, mulTTY reserves \fBstdctl\fR as an input
flow over which special commands can be issued that influence
how the program is run, such as sending signals, wrapping a
debugger around it or capturing
.IR syslog (3)
output.  Such control can be helpful in shells or the
.IR init (1)
environment of a container, and defining it as a general
facility enables generic Access Control with tools like
.IR a2rule (1).
.PP
The naming of streams for particular purposes can be a
generally useful tool in recognising a particular behaviour,
without resorting to hijacking a special sequence.  A
good example would be a separate \fBzmodem\fR stream
that runs the Z-modem protocol without interference with
other, perhaps newer, uses of ASCII codes.  Z-modem can
be surprisingly useful in modern settings, either to
attach a remote session to a desktop's clipboard, or to
send a file from one remote session to another via a
desktop coupling for their \fBzmodem\fR streams.
Without the explicit
.IR mulTTY (7)
annotation as a separate stream such a facility could
easily become a security risk.
.PP
.SH STREAMS AND PROGRAM MULTIPLEXING
The ASCII codes used for streams and the hierarchy of
program multiplexes use an optional identifier prefix
.BI <SOH> name
which may optionally be followed by
.BI <US> description
for human consumption.  This form is used throughout
.IR mulTTY (7)
as a precursor to an instruction.  When not found,
a default value will be used, as defined for each
code.
.PP
The codes used for general stream and program management are:
.TP
.BR <SI> " or " <SO>
These two codes are equivalent but the general practice is to
use the last one seen, or \fB<SO>\fR if none.  The
original meaning of these codes is Shift In and Shift
Out, for uses such as changing to another colour of
printer ribbon.  Linux consoles use the codes to
switch to an alternative character set.
.IP
The meaning of this code in mulTTY is to set the
output stream name to the part after \fB<SOH>\fR,
or default to \fBstdin\fR for input streams and to
\fBstdout\fR for output streams.
.IP
The current stream is local to a program, and it is
a task for any multiplexer to track the current
stream of each controlled program separately.
.TP
.BI <EM>
This is the End Of Medium code.  It is used by
mulTTY to mark the end of the current stream of
the current program.  If no \fB<SOH>\fR prefix is
used, this expresses no special reason for ending
the stream.  Otherwise, an \fB<SOH>\fIreason\fR
prefix indicates a machine-readable reason for the
termination.  The \fIreason\fR must not be a numeric
code from
.IR errno (3)
because those are OS-specific; the POSIX names like
\fBEACCESS\fR for the same events are usable, and so
are other numeric codes generated with a
.IR com_err (3)
compiler.  When a reason is given, it is possible
to add \fB<US>\fIdescription\fR before \fB<EM>\fR for
human consumption.
.TP
.BI <STX>
Start of Text is used by mulTTY to move down in the
hierarchy of multiplexed programs.  There is a current
program at every level, which is used here by default,
but \fB<SOH>\fIprogram\fR can specify a program to
search at this level.
.TP
.BI <ETX>
End of Text is used by mulTTY to change the
current program by moving up in the program hierarchy.
When prefixed by \fB<SOH>\fIprogram\fR it will search
for the program named \fIprogram\fR at that level.
.TP
.BI <SUB>
Substitute is used by mulTTY to move to another
program at the same hierarchical level.  By default this
toggles control with the last visited program, but an
explicit \fB<SOH>\fIprogram\fR may be prefixed to name
the \fIprogram\fR to switch to.
.TP
.BI <EOT>
End of Transmission is used by mulTTY to end the current
program, possibly after switching to the \fIprogram\fR
mentioned in an optional \fB<SOH>\fIprogram\fR prefix.
.IP
After ending the program, there is no current program
and no current stream.  In this special situation the
\fB<EM>\fR code can be used to give a reasonn for
the program shutdown.  This can be used by multiplexers
to report program execution errors in a manner that is
suited for machine processing.
.IP
When the closed-down program has children, either
directly or indirectly, then these are closed down too.
This is silently done, without reporting facility.
.PP
When a search for a \fIstream\fR or \fIprogram\fR fails,
the respective entry is opened on the spot.  This helps
to land in a minimum of error conditions while processing
ASCII codes with mulTTY.  Multiplexers are urged to
report the shutdown of any named resource that they
implicitly create.  The recursive operation of the
\fB<EOT>\fR control can make this a light operation,
and one where a controlling program cleans up for lower
layers of control.
.PP
.SH ESCAPING, UTF-8, BINARY CODES
ASCII flows may hold various kinds of data, including
UTF-8 and even binary flows.  To
.IR mulTTY (7)
they are sequences of bytes, some of which happen to be
codes subject to interpretation.
.PP
There is an escape mechanism to conceal codes from
.IR mulTTY (7),
namely the \fB<DLE>\fIcode\fR sequence.  The byte value
of \fIcode\fR will be XOR-red with 0x40 and then inserted
into the byte stream without processing.
.PP
.IR mulTTY (7)
has been carefully designed to escape only once, and in
a very particular place.  At each point during the merge
and split of the flow, it is precisely known which
characters may pass, and which need to be escaped.  This
means that escapes are also removed once, and there is no
risk for a byte \fB\\x10\fR in binary content to ever be
mistaken for a \fB<DLE>\fR escape prefix.
.PP
UTF-8 code points may spread over multiple bytes, each
of which is considered a separate byte, but the UTF-8
notation can never spontaneously form control codes.
It is also not possible for the codes \fB\\x8F\fR or
\fB\\xFF\fR to occur in an UTF-8 character sequence.
So there are no additional concerns for UTF-8 escaping
as long as a good regimen is available for ASCII.
.PP
Binary codes are more general than UTF-8, but they too
can be passed thanks to \fB<DLE>\fR escaping.  One
values should certainly be escaped; the \fB<NUL>\fR
byte 0x00 is a filler that may be inserted or removed
at will in any ASCII flow, but \fB<DLE>@\fR would be an
escaped form to pass the value through.
.PP
The escape mechanism operates in a number of levels,
namely:
.IP "\fBBidirectional Flows \"over the wire\"\fR"
The lowest level of concern is what flows over a wire.  The
requirements for
.IR mulTTY (7)
are very close to an 8-but clean channel, but there are
a few places where it is lenient as a result of the
.IR ASCII (7)
definitions.  The \fB<NUL>\fR character may be inserted
and removed at will, and the \fB<DC1>\fR through \fB<DC4>\fR
codes are reserved for controlling hardware.  The best-known
example of these codes are the XON/XOFF controls for software
flow control, which coincide with \fB<DC1>\fR and \fB<DC3>\fR,
respectively.  Finally, the similarity with
.IR telnet (1)
makes the character \fB\\xFF\fR subject to escaping, also
because it does not occur in normal text including UTF-8.
The encoding is such that these codes must not be generated
at the wire flow level, unless for their intended purposes.
.IP
Any higher
layer that generates these codes should escape these bytes
unless they deliberately want to use them as defined.
.IP "\fBPrograms and Streams\fR"
The reason-of-existence for
.IR mulTTY (7)
is merging and splitting bytes from a hierarchy of programs,
and within that, from a number of streams.  To distinguish
between these sources and sinks of bytes, a few characters
are reserved and must therefore be escaped when used inside
inside programs of streams, namely
.BR <SI> ", " <SO> ", " <EM> ", " <STX> "," <ETX> ", "
.BR <SUB> " and " <EOT> .
.IP
Switches between programs and streams are written atomically;
that is, a program switch will not interrupt a stream switch
or vice-versa.  This property does not hold for byte content,
because
.IR mulTTY (7)
makes no assumption about its interpretation as binary or
UTF-8 or yet other form; this means that multi-byte codes
in UTF-8 can be split by a program or stream switch.  Once
back to the same program and stream, the character continues
in full ignorance of the side-step caused by switching.
.IP "\fBProgram Streams\fR"
Within programs, there can be any number of streams.  Each
of these can be dedicated to a purpose, such as standard
error, shell control, SASL or the ZMODEM protocol.  It does
not matter, as long as the escaping rules are used.  This
enables the transport of binary content.
.IP
It is worth noting that plain ASCII controls require no
escaping to be transported safely; this includes
.BR <CR> ", " <LF> ", " <HT> ", " <BS> ", " <ESC> ", "
.BR <BEL> ", " <BS> " and " <VT> "."
For the majority of applications, this implies that
textual data can pass unmodified, and that selective
escaping is needed for binary content.
.SH SASL IN TERMINALS
Authentication in terminals is traditionally weak.  Due
to the unstructured text designed for human processing, it is
difficult to scrape and process automatically, and this
has isolated it from more advanced authentication mechanisms.
Other protocols embed SASL in a specific format to allow
authentication with potential connections to external
tools for handling it, sometimes under single sign-on.
Even if
.IR SSH (1)
brings better security, it does not introduce options
for re-authentication during a session while trying to
use privileged functions.
.PP
There is a stream name \fBsasl\fR reserved in
.IR mulTTY (7)
to carry SASL to a program's input and output flow.
The codes are generally used as follows.
.TP
.BR <ENQ> ... <ETB>
This is the general form of a server-to-client message.
.TP
.BR <SYN> ... <ETB>
This is the general form fo a client-to-server message.
.TP
.BR <CAN>
This code is used in forms at the beginning or end of the authentication exchange.
.BR <US>
This code is used before optional parts of the protocol.
.BR <ACK>
This code is used to approve authentication.
.BR <NAK>
This code is used to reject authentication.
.TP
.BI <US> token
This code is used to mark that an optional token was filled;
without \fB<US>\fR there would be not token.  In SASL, there
is a difference between an empty token and an absent token.
Since the \fItokenfR is a general binary string, it may have
\fB<DLE>\fIescaped\fR sequences in it.  In practice, optional
tokens are followed by \fB<ETB>\fR so that shows the importance
of escaping to avoid interpretation as an ASCII control.
.PP
Specific sequences to capture SASL are defined as follows.
.TP
.BI <SYN><ETB>
Probe from client to server, to test for SASL support
and possibly trigger a login.
.TP
.BI <ENQ><CAN> hostname <US> service <US> "mech\fR..." <ETB>
Initiation of authentication, resetting any prior state,
in server-to-client direction.  This starts a SASL login
procedure.  The \fImech\fR... is a space-separated list of
SASL mechanism names for the client to choose from.  The
\fIhostname\fR represents the service host and the
\fIservice\fR is the name of the service protocol for which
authentication is attempted.
.TP
.BI <SYN><CAN> mech <ETB>
Client selection of SASL mechanism \fImech\fR with no
client-first token.
.TP
.BI <SYN><CAN> mech <US> token <ETB>
Client selection of SASL mechanism \fImech\fR with the
client-first token in the \fItoken\fR byte sequence.
Note the use of a space between \fImech\fR and \fItoken\fR
which is also transmitted for an empty token; this is how
an empty token (with space) is distinct from an absent
token (without space).
.TP
.BI <ENQ><ETB>
Challenge from server to client, with no token data.
.TP
.BI <SYN><ETB>
Response from client to server, with no token data.
.TP
.BI <ENQ><US> token <ETB>
Challenge from server to client, with \fItoken\fR as a
binary challenge token.
.TP
.BI <SYN><US> token <ETB>
Response from client to server, with no token data.
.TP
.BI <ENQ><NAK><ETB>
Rejection of authentication from server to client.
It is up to the server if another authentication round is offered.
.TP
.BI <ENQ><ACK><ETB>
Final approval from server to client, with no additional data.
.TP
.BI <ENQ><ACK><US> token <ETB>
Final approval from server to client, with additional data.
.TP
.BI <ENQ><CAN><ETB>
Reset of authentication in server-to-client direction.
This signals a SASL logout, usually with revocation of any
rights.
.TP
.BI <SYN><CAN><ETB>
Reset of authentication in client-to-server direction.
This signals a SASL logout, and requests to erect a return
to minimal access rights.
.PP
.SH SHELL INTEGRATION
There is an option for more automated interactions with shells,
based on the
.IR mulTTY (7)
encoding mechanisms.   It could be used in a separete
\fBstdopt\fR stream, subjected to the interpretation that
follows.  This can be beneficial for such things as
remote control of containers or other multiplexer places.  It can
even help to make terminal operations more accessible to novice
users, because their interface can provide better guidance.
.PP
The following ASCII controls are considered for this purpose.
It not only describes commands in a more automation-friendly
manner, but also how menus and tables can be structured with
plain ASCII controls to make them more presentable due to
explicit annotation, without screen scraping practices.
.TP
.BI <US>
To separate words, and to avoid special interpretation of spaces.
Spaces are easy to type, but make far less sense for automation.
.TP
.BI <RS>
Undetermined.
.TP
.BI <GS>
Placed between a definition (such as a function or environment
variable) and its use.
.TP
.BI <FS>
Separates commands.
.TP
.BI <ENQ>
Requests substitition, usually as a number of options.
.PP
All this is experimental and untested.  It is here to demonstrate
the potential powers of a
.IR mulTTY (7)
approach to terminals and program multiplexing.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/mulTTY/
project page for mulTTY.  We use its issue tracker
to track bugs and feature requests.
.SH COPYRIGHT
.PP
Copyright \(co 2022 Rick van Rein, ARPA2.net.
.PP
This work was partially funded by NLnet,
a public interest fund geared towards open source
excellence.
.SH "SEE ALSO"
.IR mulTTY "(7)"
with website
.IR http://gitlab.com/arpa2/mulTTY
and with utilities
.IR ctrl2ascii "(1), " ascii2ctrl "(1), " gotty "(1), "
pretty "(1), " saltty "(1), " netty "(8),"
.PP
Related work is the testing tool \fBPypeline\fR on
.IR http://pypeline.arpa2.net/

