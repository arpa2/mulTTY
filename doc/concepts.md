# Concepts in mulTTY

> *Streams, flows and programs...*

**Stream** is a sequence of bytes, for instance UTF-8 or ASCII characters.
Streams may carry binary bytes, but as ASCII does not prefix with length
there will be a need to escape certain codes.  Noteworthy is that bytes are
sequentially ordered within a stream.  Every stream has a name, but it may
be empty to signify the default stream; on input, that would be `stdin` and
on output, that would be `stdout`.

**Programs** represent a collection of streams, usually produced by one
process running one executable, but possibly by a group of collaborating
processes and/or executables.  Every program has a name.

**Program sets** are (possibly empty) sets of programs.  Every program is
part of one program set.  Program sets may have a parent program set, thus
forming a hierarchy of program sets.  Every program set has a current and
previous program to which it may switch.

**Multiplexers** are programs that are combine programs and program sets
and insert ASCII controls to keep them separated.  Unlike basic programs
that only handle streams, a multiplexer defines a child program set under
its own program name, thus adding a deepening layer to the hierarchy of
program sets.  Each of these children define the current program's program
set as their parent.  Multiplexers can be parents to multiplexers.

**Flow** is a transport medium for one or more streams, with `<SO>` and
`<SI>` codes to separate them.  The default stream is `stdin` on input
and `stdout` on output.  Additional streams such as `stderr` can be merged
without annotation under default shells, but will be annotated with `<SO>`
and `<SI>` codes under mulTTY.  As a result, the flow can be taken apart
before being displayed.  It is also possible to redirect certain streams
from within a flow.


## What mulTTY adds to POSIX

The added value of mulTTY is the distinction between a stream and flow.
This allows a manner of merging streams into a single flow in such a
way that they can be taken apart again.

In addition, nesting of programs can be represented with multiplexing,
so that programs can additionally be merged and split.

The use of active identification of streams is that they can hold
special kinds of content, such as SASL authentication exchanges, and
benefit from additional semantics.  Any cases where "screen scraping"
would otherwise be required due to the exchange of human-readable
text and interactions, or where a hard-coded ASCII sequence is
used for such things as login or menus, can now be exchanged in a
manner that separates the data from its representation.  Again, SASL
is a good example of something that can be handled elengantly, so it is
not dumbed down to password exchange as a result of terminals that
cannot do much more than conceal typed content.

