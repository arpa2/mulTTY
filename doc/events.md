# Events from mulTTY

> *The mulTTY parser can be applied to arbitrary chunks of ASCII, and
> it will trigger callback events.  The caller chooses between direct
> handling or buffering of content.*

All callbacks from mulTTY are made to the same routine, with an event
type to mark the reason of the callback.  Textual events provide a
text pointer and a length in bytes from that pointer.  It also passes
along a flag to indicate whether there may be more coming.


## Callback Types and Mechanism

```
#include <stdint.h>
#include <stdbool.h>
#include <arpa2/multty.h>

typedef void (*multty_callback_t) (
		multty_flow *mty, multty_event_t evt,
		char *txt, uint16_t len, bool end);
```

Callbacks with `end == false` usually need more text fed into the parser,
coming from the arrival of more input.  The additional text will cause
another `mty_callback()` invocation with the same `mty_event` value,
all the way up to `end == true`.  If it happens that new text arrives
that starts with the end marker, then the callback is made with `len == 0`
and `txt != NULL` so basically a consistent case that happens to be empty.

  * Be mindful about partial delivery during event callbacks.
    Store any data you might need, or otherwise assure that the
    input to the parser is not moved or overwritten.

  * Be mindful about possible empty delivery to event callbacks.
    You may expect to be handed a `txt` pointer even then.


## Error Callbacks

There is some grammar to mulTTY flows, so it is necessary to handle
unexpected characters.  The common responses maintain the flow by
either removing the character or passing it, possibly after a
modification.

The error callback allows you to choose what to do.  The problem
is always a single byte at a given pointer position.  The context
in which it occurs is the parser state into which it occurs.  An
error callback returns `true` to accept the character or `false`
to discard it from the flow.

```
#include <stdbool.h>
#include <arpa2/multty.h>

typedef bool (*multty_error_callback_t) (
		multty_flow *mty, multty_event_t evt,
		char *bad);
```


## Hacking with Callbacks

Note that the callback function does not use `const char *txt`.  It does
come with a few rules though; the part that triggers the event has been
completely handled, but you cannot assume the same for what follows.

  * You can count on the data at `txt` to be where you provided it
    to the parser.  Moving that data, or overwriting it, will change
    the data pointed to by `txt`.  Similar things are done internally
    by the parser to remove `<DLE>` escaping from a delivered binary
    string before processing the `txt` and (lowered) `len` values.
    But if the `parse_cb` is set in the `curin` stream structure, for
    instance because you want to further parse a stream's content, then
    you may want to do this yourself.

  * Your event callback may change the data at `txt`, but only over at
    most `len` bytes.  Take note that `len == 0` is a valid situation.

  * For error callbacks, you can change the single character before
    accepting it by returning `true`, but you must not assume that
    the next character is part of the input flow.

  * You must not assume that multiple event callbacks with partial
    delivery of text are from a contiguous region of memory; they are
    delivered as they occur in the mulTTY flow.  Multiple callbacks
    occur when the parser hit the end of its input buffer before a
    structure was fully parsed.  Furthermore, the parser removes
    `<DLE>` escaping and so reading adjacent to the previous buffer
    may end up elsewhere.

  * Multiplexing codes may break off a stream and later continue
    parsing that.  In this more advanced class of programs, it may
    even happen that stream changing commands from mulTTY get
    broken into pieces.

  * Non-multiplexing programs have a somewhat lighter task.  They
    can assume only one parser for a given text flow.  Switching
    between streams for example, would deliver the stream names
    without further interruptions, though it may still happen that
    they hit upon the end of a parsed block of text.  With some
    care, crude optimisations may be possible to avoid storing
    characters.  Since these are the simplest basic programs, this
    may be helpful.

  * Take note that stream switching occurs at byte boundaries, not
    at character boundaries.  It is possible that UTF-8 codes get
    broken into fragments.  This neither happens to `<DLE>` escapes
    nor with stream-modifying commands, so the mulTTY grammar has
    some nesting to its structure.  Multiplexing programs should
    be careful about not breaking off any partial commands from
    streams.


## Interactions between Flows, Streams and Multiplexing

First, let's define what variants of input and output are handled
by mulTTY:

  * Flows represent the input or output as they occur over a file
    handle of some kind, while communicating with other programs,
    sockets, files or devices that implement mulTTY.  They encode
    multiple streams and possibly also multiplexes with the ASCII
    codes as defined by mulTTY.

  * Streams are a separate sequence of bytes or characters, as
    separated out from a flow on input, and as combined with
    other streams on output.  We call one such entity that
    uses one or more flows with one or more streams a program.

  * Multiplexing combines the flows of a number of programs
    into one flow, which uses a more advanced mulTTY grammar.
    Not all programs need to be aware of multiplexing, but it
    is very  useful to have a few generic tools that are.
    Think of shells, `init` programs, `xinetd` and so on.

Generally, a parser is attached to an input flow, and a driver
with optional buffering transmits an output flow.  The program
using these handlers will setup a flow with one or more streams,
one of which is elected as the default flow.  Every flow has a
current stream, which initially is its default flow.  Changes
can be made throughout the program.

Multiplexers order programs in a hierarchical order, represented
as a pointer structure.  This too can be changed while the
multiplexer runs.  Every multiplexed program has its own streams
with the related current and default stream.

When a program exhibits multiple flows, they appear separated to
a multiplexer because the multiplexer works on file descriptors
such as sockets or pipelines or devices and cannot tell who is
on the other end.  Also, there is usually a reason to keep them
separate, which a multiplexer then respects.

  * Your parser calls you back with a `multty_flow` pointer.
    This represents the input flow that is being processed
    by the parser.  It has `multty_stream` pointers to the
    current and default stream, which you can modify during
    a callback, and your program can do this too.  You are
    responsible for concurrency aspects of all this (which
    can be quite simple in an event-driven program).  It
    is possible for even the current flow to be NULL when
    no program exists for it.

  * Every `multty_stream` has a name by which it appears
    in the flow, with an empty string only for the default
    stream.  The 32 distinct characters, filled with 0x00
    if it is shorter (but not necessarily terminated!) are
    part of the structure.  You can also store a `extname`
    for more characters, and a `descr` string to be sent
    after `<US>`.  If you need to search for an entry by
    name, then use your own code; there are `prev` and
    `next` pointers.

  * Any `multty_stream` may point to the `multty_program`
    that contains it.  Programs unaware of multiplexing
    will set this to NULL.  Programs aware of it may also
    find the NULL program when the current program ended.

  * Any `multty_program` has pointers to the hierarchical
    `parent`, its `prev` and `next` siblings, its direct
    `children` and, among that, its `current_child`.  It
    also has a `last` subling to allow switching back
    and forth more easily.

The mulTTY library can just be a parser for ASCII, or a wrapper
around it that either is or is not aware of multiplexing.
What such a wrapper does is filter events and higher-level
callbacks instead.


## Higher Level Callbacks

TODO
