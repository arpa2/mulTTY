/* sasl-server.c -- Server-side mulTTY SASL authentication.
 *
 * This API requests SASL authentication from a client.  It makes
 * callbacks for printing strings, which the caller then produces
 * as part of the "sasl" stream in its own manner.  The input of
 * new SASL content is passed via mulTTY's callback system.
 *
 * This API formats mulTTY messages that adhere to the SASL stream
 * protocol.  Any contained binary content is <DLE> escaped before
 * it is sent, so the requirements of SASL tokens are met.
 *
 * The API talks to a backend using Quick-DiaSASL.  It expects to
 * receive a diasasl_node structure that has already been setup.
 * It creates a session under that node and communicates until it
 * has been satisfied.
 *
 * The caller is always in control.  This concerns polling of the
 * Quick-DiaSASL backend as well as the user interaction.  There
 * is a return code indicating what the API expects to happen next.
 * This setup allows synchronous, forked, threaded, event-driven 
 * and perhaps even other structures in the API caller.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <termios.h>
#include <unistd.h>

#include <security/pam_modules.h>
#include <security/pam_ext.h>

#include <arpa2/socket.h>
#include <arpa2/quick-mem.h>
#include <arpa2/quick-diasasl.h>
#include <arpa2/except.h>

#include <arpa2/multtysasl.h>




static void sasltext_cb (struct multty *mty, multty_event_t evt,
                        char *txt, unsigned len,
                        bool first, bool last);

static void saslevent_cb (struct multty *mty, multty_event_t evt,
			bool named, bool described);


#ifdef PAM_MODULE_INSPIRATION
//
// Configuration options on the PAM commandline
enum config_symbols {
	CFG_HOSTNAME = 0,
	CFG_DOMAIN,
	CFG_SERVICE,
	CFG_DIASASL_IP,
	CFG_DIASASL_PORT,
	CFG_COUNT
};
char *config_keys [CFG_COUNT] = {
	"host",
	"domain",
	"service",
	"diasasl_ip",
	"diasasl_port",
};
int config_keylen [CFG_COUNT] = {
	4,
	6,
	7,
	10,
	12,
};
#if 0
char *config_defaults [CFG_COUNT] = {
	NULL,
	"host",
};
#endif
#endif



//
// A good manual on writing a PAM module:
// https://web.archive.org/web/20190523222819/https://fedetask.com/write-linux-pam-module/
//



/* Escape all control characters 0x00..0x1f with <DLE> prefix.
 * Do the same with special characters 0x7f <DEL> and 0xff <IAC>.
 * Take input from token and write it to the (large enough)
 * send buffer.  Append a '\0' byte to the latter.
 */
static uint32_t bin_escape_DLE (char *send, char *token, uint32_t tokenlen) {
	uint32_t sendlen = 0;
	for (int i = 0; i < tokenlen; i++) {
		char c = token [i];
		if (((c & 0xe0) == 0x00) || ((c & 0x7f) == 0x7f)) {
			send [sendlen++] = c_DLE;
			send [sendlen++] = 0x40 ^ c;
		} else {
			send [sendlen++] =        c;
		}
	}
	send [sendlen] = '\0';
	return sendlen;
}


/* Reset the TTY if it was altered.
 */
static void reset_tty_multtysasl (struct multtysasl_server *sasl) {
	if (sasl->_termios_changed) {
		tcsetattr (0, TCSADRAIN, &sasl->_termios_original);
		sasl->_termios_changed = false;
	}
}


/* Cleanup the mulTTY SASL structures.
 */
static void teardown_multtysasl (struct multtysasl_server *sasl) {
	if (sasl->_tty_sasl_active) {
		/* We sent on the TTY, so we should tear it down */
		(void) sasl->send2client_cb (sasl, s_ENQ s_NAK s_ETB s_ENQ s_CAN s_ETB);
		sasl->_tty_sasl_active = false;
	}
	reset_tty_multtysasl (sasl);
}


/* Callbacks from Quick-DiaSASL due to a response.
 *
 * This relays protocol information (success or failure+reason)
 * and an optional mechlist and optional s2c token.  The latter
 * can be printed directly on the client stream.  If there is
 * no final response, the state than changes to wait4client.
 */
static void diasasl_cb (struct diasasl_session *session,
		const int32_t *com_errno,
		char *opt_mechanism_list,
		uint8_t *s2c_token, uint32_t s2c_token_len) {
	struct multtysasl_server *sasl = (struct multtysasl_server *) session;
	assert (sasl->state == MSS_WAIT4BACKEND);
#ifdef OLD_AND_INDIRECT
	assert (!sasl->_success);
	assert (!sasl->_failure);
	sasl->_s2c_token.bufptr = NULL;
	sasl->_s2c_token.buflen = 0;
#endif
	if (com_errno != NULL) {
		if (*com_errno == 0) {
#ifdef OLD_AND_INDIRECT
			sasl->_success = true;
#endif
			sasl->state = MSS_AUTHENTICATED;
		} else {
#ifdef OLD_AND_INDIRECT
			sasl->_failure = true;
#endif
			sasl->state = MSS_REJECTED;
		}
#ifndef OLD_AND_INDIRECT
	} else {
		sasl->state = MSS_WAIT4CLIENT;
		sasl->_tty_sasl_active = true;
#endif
	}
#ifdef OLD_AND_INDIRECT
	if ((com_errno == NULL) || (*com_errno == 0)) {
		if (opt_mechanism_list != NULL) {
			assert (mem_isnull (&sasl->_mechlist));
			assert (mem_isnull (&sasl->_s2c_token));
			sasl->_mechlist.buflen = strlen (opt_mechanism_list);
			sasl->_mechlist.bufptr = NULL;
			if (!mem_alloc (sasl, sasl->_mechlist.buflen, (void **) &sasl->_mechlist.bufptr)) {
				sasl->_failure = true;
				return;
			}
			memcpy (sasl->_mechlist.bufptr, opt_mechanism_list, sasl->_mechlist.buflen);
		} else if (s2c_token != NULL) {
			sasl->_s2c_token.bufptr = NULL;
			if (!mem_alloc (sasl, s2c_token_len, (void **) &sasl->_s2c_token.bufptr)) {
				sasl->_failure = true;
				return;
			}
			memcpy (sasl->_s2c_token.bufptr, s2c_token, s2c_token_len);
			sasl->_s2c_token.buflen = s2c_token_len;
		}
	}
#endif
	unsigned buflen = 5;
	switch (sasl->state) {
	case MSS_AUTHENTICATED:
		buflen += 2;
		if (s2c_token != NULL) {
			buflen += 1 + s2c_token_len;
		}
		break;
	case MSS_REJECTED:
		buflen += 5;
		break;
	case MSS_WAIT4CLIENT:
		if (opt_mechanism_list != NULL) {
			buflen += 0 + strlen (sasl->config_hostname);
			buflen += 1 + strlen (sasl->config_service);
			buflen += 1 + strlen (opt_mechanism_list);
		} else if (s2c_token != NULL) {
			buflen += 1 + s2c_token_len;
		}
	}
	char asciibuf [buflen];
	asciibuf [0] = c_ENQ;
	switch (sasl->state) {
	case MSS_AUTHENTICATED:
		asciibuf [1] = c_ACK;
		if (s2c_token != NULL) {
			asciibuf [2] = c_US;
			bin_escape_DLE (asciibuf + 3, s2c_token, s2c_token_len);
		}
		strcat (asciibuf, s_ETB);
		(void) sasl->send2client_cb (sasl, asciibuf);
		break;
	case MSS_REJECTED:
		asciibuf [1] = c_NAK;
		asciibuf [2] = c_ETB;
		asciibuf [3] = c_ENQ;
		asciibuf [4] = c_CAN;
		asciibuf [5] = c_ETB;
		asciibuf [6] = '\0';
		break;
	case MSS_WAIT4CLIENT:
		if (opt_mechanism_list != NULL) {
			sprintf (asciibuf + 1, "%c%s%c%s%c%s%c",
				c_CAN,
				sasl->config_hostname,
				c_US,
				sasl->config_service,
				c_US,
				opt_mechanism_list,
				c_ETB);
		} else if (s2c_token != NULL) {
			asciibuf [1] = c_US;
			memcpy (asciibuf + 2, s2c_token, s2c_token_len);
			asciibuf [2 + s2c_token_len + 0] = c_ETB;
			asciibuf [2 + s2c_token_len + 1] = '\0';
		} else {
			asciibuf [1] = c_ETB;
			asciibuf [2] = '\0';
		}
		break;
	}
	if (!sasl->send2client_cb (sasl, asciibuf)) {
		/* Failed; will try to tell the client below */
		sasl->state = MSS_REJECTED;
	}
	if (sasl->state != MSS_WAIT4CLIENT) {
		/* Done interacting, recover the original terminal setup */
		teardown_multtysasl (sasl);
		sasl->finalstate_cb (sasl);
	}
}


/* Process an incoming text fragment.  Basically collect what we can get.
 * Use fragbuf as a fragment buffer and fraglen as the collected length.
 */
static void sasltext_init (struct multty_ext4sasl *ext) {
	ext->c2s_mechchoice = NULL;
#ifdef OLD_SIGNAL_MODE
	ext->c2s_tokenbuf = NULL;
	ext->c2s_tokenlen = 0;
	ext->c2s_send = false;
	ext->c2s_teardown = true;
#endif
	//TODO// May piggyback on the existing memory pool
#ifdef MEMPOOL_SEPARATE
	if (mem_open (0, &ext->fragpool)) {
#endif
		if (mem_buffer_open (ext->fragpool, 1000, &ext->fragbuf)) {
#ifdef OLD_SIGNAL_MODE
			ext->c2s_teardown = false;
#endif
		} else {
			mem_close (&ext->fragpool);
#ifndef OLD_SIGNAL_MODE
			struct multtysasl_server *sasl = (struct multtysasl_server *) ext->fragpool;
			teardown_multtysasl (sasl);
#endif
		}
#ifdef MEMPOOL_SEPARATE
	}
#endif
}
//
static void sasltext_fini (struct multty_ext4sasl *ext) {
	mem_buffer_delete (ext->fragpool, &ext->fragbuf);
#ifdef MEMPOOL_SEPARATE
	mem_close (&ext->fragpool);
#endif
}
//
static void sasltext_cb (struct multty *mty, multty_event_t evt,
			char *txt, unsigned len,
			bool first, bool last) {
	struct multty_ext4sasl *ext = (struct multty_ext4sasl *) mty;
	struct multtysasl_server *sasl = (struct multtysasl_server *) ext->fragpool;
	log_debug ("Processing SASL text callback type %d on \"%.*s\"", evt, len, txt);
	//
	// Do not keep state if the parser makes unexpected callbacks
#ifdef OLD_SIGNAL_MODE
	ext->c2s_tokenbuf = NULL;
	ext->c2s_tokenlen = 0;
	ext->c2s_send = false;
#endif
	//
	// Start a new token with no bytes in the buffer
	if (first) {
		ext->fraglen = 0;
	}
	//
	// Have enough space in the buffer for the token (extension)
	if (!mem_buffer_resize (ext->fragpool, ext->fraglen + len, &ext->fragbuf)) {
#ifdef OLD_SIGNAL_MODE
		ext->c2s_teardown = true;
#else
		teardown_multtysasl (sasl);
#endif
		return;
	}
	//
	// Clone token bytes into the buffer
	if (len > 0) {
		memcpy (((uint8_t *) ext->fragbuf.bufptr) + ext->fraglen, txt, len);
		ext->fraglen += len;
	}
	//
	// If this ends the text, see if it calls for immediate processing
	if (last) {
		if (evt == MULTTY_SASL_C2S_TOKEN) {
			//
			// The client sent a token, which is now complete;
			// store its temporary coordinates and have it sent
			log_debug ("Got a c2s token size %d", ext->fraglen);
#ifdef OLD_SIGNAL_MODE
			ext->c2s_tokenbuf = ext->fragbuf.bufptr;
			ext->c2s_tokenlen = ext->fraglen;
			ext->c2s_send = true;
#else
			/* The fragbuf/len holds the C2S token to be sent */
			diasasl_send (sasl->dianode, &sasl->diasasl, ext->c2s_mechchoice, ext->fragbuf.bufptr, ext->fraglen);
			ext->c2s_mechchoice        = NULL;
			sasl->diasasl.chanbind     = NULL;
			sasl->diasasl.chanbind_len = 0;
#endif
		} else if (evt == MULTTY_SASL_C2S_MECHNAME) {
			//
			// The client selected a mechanism; store the choice
			// and open a new buffer for the token that may follow
			log_debug ("Got a c2s mechanism choice %.*s", ext->fraglen, ext->fragbuf.bufptr);
			membuf mechbuf;
			mechbuf.bufptr = ext->fragbuf.bufptr;
			mechbuf.buflen = ext->fraglen;
			if (!mem_buffer_resize (ext->fragpool, ext->fraglen + 1, &ext->fragbuf)) {
#ifdef OLD_SIGNAL_MODE
				ext->c2s_teardown = true;
#else
				teardown_multtysasl (sasl);
#endif
				return;
			}
			ext->fragbuf.bufptr [ext->fraglen] = '\0';
			mem_buffer_close (ext->fragpool, ext->fraglen + 1, &ext->fragbuf);
			ext->c2s_mechchoice = ext->fragbuf.bufptr;
			if (!mem_buffer_open (ext->fragpool, 1000, &ext->fragbuf)) {
#ifdef OLD_SIGNAL_MODE
				ext->c2s_teardown = true;
#else
				teardown_multtysasl (sasl);
#endif
				return;
			}
			/* Do not send yet; there may be a client-first token */
		} else {
			/* Do not close by default, but reuse the buffer */
		}
	}
}
//
void saslevent_cb (struct multty *mty, multty_event_t evt,
			bool named, bool described) {
	struct multty_ext4sasl *ext = (struct multty_ext4sasl *) mty;
	struct multtysasl_server *sasl = (struct multtysasl_server *) ext->fragpool;
	log_debug ("Processing SASL event callback type %d", evt);
	if (evt == MULTTY_SASL_C2S_NO_TOKEN) {
		//
		// Explicit indication that the client will not send a token
#ifdef OLD_SIGNAL_MODE
		ext->c2s_tokenbuf = NULL;
		ext->c2s_tokenlen = 0;
		ext->c2s_send = true;
#else
		diasasl_send (sasl->dianode, &sasl->diasasl, ext->c2s_mechchoice, NULL, 0);
		ext->c2s_mechchoice        = NULL;
		sasl->diasasl.chanbind     = NULL;
		sasl->diasasl.chanbind_len = 0;
#endif
	} else if (evt == MULTTY_SASL_C2S_TEARDOWN) {
		//
		// Explicit indication that the client tears down the DiaSASL attempt
#ifdef OLD_SIGNAL_MODE
		ext->c2s_teardown = true;
#else
		teardown_multtysasl (sasl);
#endif
	}
}


/* Open a mulTTY SASL session for one authentication exchange.  Return success.
 * When successful, this needs to be reversed with multtysasl_close().
 *
 * The tty_in handle is optional.  If it is not used, set it <0.  When provided
 * and if it is a tty, echo will be suppressed and similar sanity will be used.
 * When later closing the server, this is reversed to the original state.
 *
 * The printing callback is used to print <DLE>-escaped binary content, as
 * it may occur in the "sasl" stream.  The callback asks for the current
 * stream to be set to "sasl" and then print the string without further
 * escaping.
 *
 * The node is the Quick-DiaSASL node against which this session runs.
 * It will be setup with the server_domain and svcproto and communication
 * starts immediately (reply traffic will not be processed before the caller
 * invokes diasasl_process() on the dianode, but errors may fire immediately).
 *
 * The newserver pointer must be initialised to NULL before the call.
 *
 * When a program and/or flow is provided, then their values are setup for
 * the lifetime of this object.
 *
 * After this call, it is possible to setup channel binding information,
 *	(*newserver)->diasasl.chanbind     = ...
 *	(*newserver)->diasasl.chanbind_len = ...
 * This is new territory for ASCII connections! and unknown how.
 * Ideas: 1. Use the (EC)DH key used in SSH
 *        2. Derive a key from a TLS or SSH session key
 *        3. Use the inode of the terminal
 *
 */
bool multtysasl_open (struct multtysasl_server **newserver,
				int tty_in,
				bool (*send2client_cb) (struct multtysasl_server *this, const char *multtysasl_string),
				void (*finalstate_cb) (struct multtysasl_server *this),
				struct diasasl_node *dianode,
				char *hostname,
				char *server_domain,
				char *svcproto,
				struct multtyprog *prog, struct multtyflow *flow) {
	//
	// Allocate a server with zeroes, then initialise variables
	if (!mem_open (sizeof (*newserver), (void **) newserver)) {
		return false;
	}
	struct multtysasl_server *sasl = *newserver;
	//
	// Temporarily set the MSS_ERROR state
	bool ok = true;
	sasl->state = MSS_ERROR;
	//
	// Set configuration strings
	sasl->config_hostname = hostname;
	sasl->config_service  = svcproto;
	//
	// Setup the final-state and printing callbacks
	sasl->finalstate_cb  =  finalstate_cb;
	sasl->send2client_cb = send2client_cb;
	//
	// Setup the terminal, aiming for raw mode
	sasl->_tty_in = tty_in;
	sasl->_termios_changed = (tty_in >= 0) &&
			isatty (tty_in) &&
			(0 == tcgetattr (tty_in, &sasl->_termios_original));
	if (sasl->_termios_changed) {
		struct termios rawmode;
		memcpy (&rawmode, &sasl->_termios_original, sizeof (rawmode));
		rawmode.c_iflag &= ~( INLCR | ICRNL | INPCK | ISTRIP | IXON | IXANY | IUTF8 );
		rawmode.c_iflag |=    IGNBRK | IGNPAR | IXOFF;
		rawmode.c_oflag &= ~( OPOST | OLCUC | ONLCR | OCRNL | ONOCR | ONLRET | OFILL );
		rawmode.c_oflag |=    0;
		rawmode.c_lflag &= ~( ISIG | ICANON | ECHO | IEXTEN );
		rawmode.c_lflag |=    0;
		tcsetattr (0, TCSADRAIN, &rawmode);
	}
	//
	// Setup for mulTTY
	struct multty_ext4sasl *ms = &sasl->mtysasl;
	ms->mty.flow = flow;
	ms->mty.text_cb = sasltext_cb;
	ms->mty.event_cb = saslevent_cb;
	ms->mty.parse_cb = multtysasl_parse_cb;
	ms->fragpool = (void *) sasl;
	sasltext_init (ms);
	strcpy (ms->mty.name, "sasl");
	if (prog != NULL) {
		multtyprog_addstream (prog, &ms->mty);
	}
	ms->mty.flow = flow;
	//
	// Start communicating with the backend
	sasl->dianode = dianode;
	sasl->diasasl.callback = diasasl_cb;
	diasasl_open (sasl->dianode, &sasl->diasasl, server_domain, svcproto);
	//
	// Enter the state ready for processing
	sasl->state = MSS_WAIT4BACKEND;
	//
	// Success, deliver a session for mulTTY SASL authentication
	return true;
}


/* Close a mulTTY SASL session after to cleanup an authentication exchange
 * that was opened with a successful call to multtysasl_open().  This is good
 * to do as soon as possible, but any data should first be taken out of it.
 * It is a good aim to close before the next remote communication.
 *
 * The server pointer will be reset to NULL and all memory is cleared
 * (inasfar as compiler optimisation does not zealously damage that code).
 */
void multtysasl_close (struct multtysasl_server **oldserver) {
	struct multtysasl_server *sasl = *oldserver;
	if (sasl->mtysasl.mty.program != NULL) {
		multtyprog_delstream (&sasl->mtysasl.mty);
	}
	diasasl_close (sasl->dianode, &sasl->diasasl);
	sasltext_fini (&(*oldserver)->mtysasl);
	if (sasl->_tty_in >= 0) {
		reset_tty_multtysasl (sasl);
	}
	mem_close ((void **) oldserver);
}




#if PAM_MODULE_INSPIRATION

PAM_EXTERN int pam_sm_authenticate (pam_handle_t *pamh, int flags, int argc, const char *argv []) {
log_debug ("pam_saltty_diasasl: pam_saltty_diasasl enters pam_sm_authenticate()");


	//NO//
	//
	// Tend towards failure, but stay open to be taught otherwise
	int pamrc = PAM_PERM_DENIED;
	//
	// When the TTY starts SASL, it should also send a failure/retraction
#ifdef OLD_PAM_CODE
	bool tty_sasl = false;
#endif
	//
	// Parse arguments
	char *config_values [CFG_COUNT];
	memset (config_values, 0, sizeof (config_values));
	for (int argi = 0; argi < argc; argi++) {
		for (int keyi = 0; keyi < CFG_COUNT; keyi++) {
			if (strncmp (argv [argi], config_keys [keyi], config_keylen [keyi]) != 0) {
				continue;
			}
			char *possval = (char *) (argv [argi] + config_keylen [keyi]);
			if (*possval++ != '=') {
				continue;
			}
			if (config_values [keyi] != NULL) {
				log_error ("Doubly defined key \"%s\"", config_keylen [keyi]);
				return PAM_NO_MODULE_DATA;
			}
			config_values [keyi] = possval;
		}
	}
	char althostname [256];
	if (gethostname (althostname, sizeof (althostname)) < 0) {
		althostname [0] = '\0';
	} else {
		althostname [sizeof (althostname)-1] = '\0';
	}
	if (config_values [CFG_HOSTNAME] == NULL) {
		if (althostname [0] == '\0') {
			log_error ("Failed to extract fully qualified host name, please configure host=...");
			return PAM_NO_MODULE_DATA;
		} else if (strchr (althostname, '.') == NULL) {
			log_error ("Please add the domain in the host=... configuration");
			return PAM_NO_MODULE_DATA;
		}
		config_values [CFG_HOSTNAME] = althostname;
	}
	char *altdomain = strchr (config_values [CFG_HOSTNAME], '.');
	if (altdomain != NULL) {
		altdomain++;
	}
	if (config_values [CFG_DOMAIN] == NULL) {
		if (altdomain == NULL) {
			log_error ("Failed to extract domain name, please configure domain=...");
			return PAM_NO_MODULE_DATA;
		}
		config_values [CFG_DOMAIN] = altdomain;
	}
	if (strchr (config_values [CFG_DOMAIN], '.') == NULL) {
		log_error ("Domain is not fully qualified (no dots)");
		return PAM_NO_MODULE_DATA;
	}
	if (config_values [CFG_SERVICE] == NULL) {
		config_values [CFG_SERVICE] = "host";
	}
	if (config_values [CFG_DIASASL_IP] == NULL) {
		config_values [CFG_DIASASL_IP] = "::1";
	}
	if (config_values [CFG_DIASASL_PORT] == NULL) {
		log_error ("No default Quick-DiaSASL port, please configure diasasl_port=... (and maybe also diasasl_ip=...)");
		return PAM_NO_MODULE_DATA;
	}


	//YES//DONE//
	//
	// Setup for raw mode (mostly clearing ICANON)
	struct termios tty_orig, tty_mode;
	bool tty_changed = isatty (0) && (0 == tcgetattr (0, &tty_orig));
	if (tty_changed) {
		memcpy (&tty_mode, &tty_orig, sizeof (tty_mode));
		tty_mode.c_iflag &= ~( INLCR | ICRNL | INPCK | ISTRIP | IXON | IXANY | IUTF8 );
		tty_mode.c_iflag |=    IGNBRK | IGNPAR | IXOFF;
		tty_mode.c_oflag &= ~( OPOST | OLCUC | ONLCR | OCRNL | ONOCR | ONLRET | OFILL );
		tty_mode.c_oflag |=    0;
		tty_mode.c_lflag &= ~( ISIG | ICANON | ECHO | IEXTEN );
		tty_mode.c_lflag |=    0;
		tcsetattr (0, TCSADRAIN, &tty_mode);
	}


	//NO//
	//
	// Setup for Quick-DiaSASL
	struct diasasl_node dianode;
	memset (&dianode, 0, sizeof (dianode));
	diasasl_node_open (&dianode);


	//YES//DONE//
	//
	// Setup for mulTTY
	struct multtyflow mufl;
	multtyflow_init (&mufl, false);
	multty_t *stream_out = multty_stdout;
	multty_t *stream_in  = multty_stdin;
	struct multty_ext4sasl multty_sasl = {
		.mty.next = NULL /* Filled later */,
		.mty.prev = NULL /* Filled later */,
		.mty.flow = &mufl,
		.mty.program = multtyprog_main,
		.mty.name = { 's', 'a', 's', 'l', 0 },
		.mty.description = NULL,
		.mty.description_fresh = false,
		.mty.text_cb = sasltext_cb,
		.mty.event_cb = saslevent_cb,
		.mty.parse_cb = multtysasl_parse_cb,
	};
	multtyprog_addstream (multtyprog_main, &multty_sasl.mty);
	multty_sasl.mty.flow = &mufl;
#ifdef MEMPOOL_SEPARATE
	//
	// Setup for SASL parsing
	sasltext_init (&multty_sasl);		//YES//FRAGMEM//
#endif


	//NO//
	//
	// Connect a socket
	struct sockaddr_storage diass;
	if (!socket_parse (config_values [CFG_DIASASL_IP], config_values [CFG_DIASASL_PORT], &diass)) {
log_debug ("pam_saltty_diasasl: Failed to parse socket IP %s and/or port %s",
				config_values [CFG_DIASASL_IP], config_values [CFG_DIASASL_PORT]);
		pamrc = PAM_SERVICE_ERR;
		goto diafail;
	}
	if (!socket_client (&diass, SOCK_STREAM, &dianode.socket)) {
log_debug ("pam_saltty_diasasl: Failed to make client socket");
		pamrc = PAM_SERVICE_ERR;
		goto diafail;
	}
	//
	// Start the SASL session as a client
	struct multtysasl_server *sasl = NULL;
	if (!mem_open (sizeof (*sasl), (void **) &sasl)) {
log_debug ("pam_saltty_diasasl: Failed to create memory pool");
		pamrc = PAM_BUF_ERR;
		goto memsoxfail;
	}
#ifdef MEMPOOL_SEPARATE
	//
	// Setup for SASL parsing
	sasltext_init (&multty_sasl);		//YES//FRAGMEM//
	multty_sasl.fragpool = (void *) sasl;
#endif


	//YES//DONE//
	sasl->diasasl.callback = diasasl_cb;
	diasasl_open (&dianode, &sasl->diasasl, config_values [CFG_DOMAIN], NULL);


	//NO//
	if (diasasl_process (&dianode, true) != 0) {
log_debug ("pam_saltty_diasasl: Failed to process DiaSASL open request for domain %s", config_values [CFG_DOMAIN]);
		pamrc = PAM_SERVICE_ERR;
		goto memsoxfail;
	}
	if (mem_isnull (&sasl->_mechlist) || mem_isempty (&sasl->_mechlist)) {
log_debug ("pam_saltty_diasasl: Silly mechanism list from Quick-DiaSASL server, NULL or empty");
		pamrc = PAM_SERVICE_ERR;
		goto memsoxfail;
	}


	//YES//DONE//
	//
	// Setup channel binding information
	// This is new territory for ASCII connections!
	// Ideas: 1. Use the (EC)DH key used in SSH
	//        2. Derive a key from a TLS or SSH session key
	//        3. Use the inode of the terminal
	//TODO// sasl->diasasl.chanbind = ...
	//TODO// sasl->diasasl.chanbind_len = ...
	//
	// We should have received a mechanism list, and this
	// will be the initial SASL message to the client;
	// this carries a _mechlist, which needs no <DLE> escaping
log_debug ("pam_saltty_diasasl: Prompting the user with the mechanism list \"%s\"", sasl->_mechlist);
	//
	// Since pam_prompt() sends to stderr, we produce our own prommpt
#ifdef OLD_PAM_CODE
	tty_sasl = true;
#else
	sasl->_tty_sasl_acive = true;
#endif


	//NO//
	printf (s_SOH "sasl" s_SO s_ENQ s_CAN "%s" s_US "%s" s_US "%s" s_ETB s_SO,
			config_values [CFG_HOSTNAME],
			config_values [CFG_SERVICE],
			sasl->_mechlist);
	fflush (stdout);


	//NO//
	char rdbuf [4096];
	ssize_t rdlen = read (0, rdbuf, sizeof (rdbuf)-1);
log_debug ("read() returned %zd at %s:%d", rdlen, __FILE__, __LINE__);
	if (rdlen <= 0) {
		pamrc = PAM_CONV_ERR;
		goto memsoxfail;
	} else {
		rdbuf [rdlen] = '\0';
		pamrc = PAM_SUCCESS;
	}


	//NO// Loop moves into event handler loop; parsing is external;
	//
	// Enter a process/prompt loop to process client responses
	while ((!sasl->_success) && (!sasl->_failure)) {
		//
		// Reduce the response to just the mulTTY "sasl" stream
		int resplen = 0;
		uint8_t *respbin = rdbuf;
		char *resptxt = rdbuf;
		log_debug ("pam_saltty_diasasl: Response address is %p #%d, start", respbin, resplen);
		log_data ("pam_saltty_diasasl: Response in text", resptxt, strlen (resptxt), 0);
		while  (resptxt = strstr (resptxt, s_SOH "sasl"),
					resptxt != NULL) {
			//
			// Move beyond "<SOH>sasl<SI|SO>" or repeat if neither
			// but be open for "<SOH>sasl<US>...<SI|SO>" too
			resptxt += 5;
			if (*resptxt == c_US) {
				char c;
				do {
					c = * ++resptxt;
				} while (((c >= ' ') && (c != c_DEL)) ||
				         ((c >= 0x07) && (c <= 0x0d)) ||
				         (c == 0x1b));
			}
			if ((*resptxt != c_SO) && (*resptxt != c_SI)) {
				continue;
			}
			resptxt++;
			//
#ifdef PARSER_JOB_DONE_HERE
			// Remove any <DLE> escapes from the response token
#else
			// Move content to the response start, minding <DLE> escapes
#endif
			while ((*resptxt != '\0') && (*resptxt != c_SO) && (*resptxt != c_SI) && (*resptxt != c_SOH)) {
				if (*resptxt == c_DLE) {
#ifdef PARSER_JOB_DONE_HERE
					resptxt++;
					if (*resptxt == '\0') {
						pamrc = PAM_AUTH_ERR;
						goto memsoxfail;
					}
					*respbin++ = 0x40 ^ *resptxt++;
#else
					*respbin++ =        *resptxt++;
					resplen++;
					if (*resptxt == '\0') {
						pamrc = PAM_AUTH_ERR;
						goto memsoxfail;
					}
					*respbin++ = *resptxt++;
#endif
				} else {
					*respbin++ = *resptxt++;
				}
				resplen++;
			}
		}
		respbin -= resplen;
		log_debug ("pam_saltty_diasasl: Response address is %p #%d, done", respbin, resplen);
		log_data ("pam_saltty_diasasl: Response content", respbin, resplen, 0);
		//
		// Parse the mulTTY "sasl" stream as SASL c2s content
#ifdef OLD_SIGNAL_MODE
		multty_sasl.c2s_send = false;
		multty_sasl.c2s_tokenbuf = NULL;
		multty_sasl.c2s_tokenlen = 0;
#endif
		multtysasl_parse_cb (&multty_sasl.mty, MULTTY_CONTENT, respbin, resplen, true, true);
		//
		// Test for unexpected parser conditions
#ifdef OLD_SIGNAL_MODE
		if (multty_sasl.c2s_teardown) {
log_debug ("pam_saltty_diasasl: Client requested teardown of SASL authentication");
#ifdef OLD_SIGNAL_MODE
			multty_sasl.c2s_send = false;
#endif
#ifdef OLD_PAM_CODE
			tty_sasl = false;
#endif
		}
#endif
#ifdef OLD_SIGNAL_MODE
		if (!multty_sasl.c2s_send) {
			sasl->_failure = true;
			pamrc = PAM_SERVICE_ERR;
log_debug ("pam_saltty_diasasl: Client did not trigger token sending with its response token");
			break;
		}
		//
		// Relay the client response to the Quick-DiaSASL server
		log_debug ("About to backend-relay c2s token #%d that is NULL if %d", multty_sasl.c2s_tokenlen, multty_sasl.c2s_tokenbuf == NULL);
		diasasl_send (&dianode, &sasl->diasasl, multty_sasl.c2s_mechchoice, multty_sasl.c2s_tokenbuf, multty_sasl.c2s_tokenlen);
		log_debug ("finished backend-relay c2s token");
		//
		// Reset items meant for the first diasasl_send() only
		multty_sasl.c2s_mechchoice = NULL;
		sasl->diasasl.chanbind     = NULL;
		sasl->diasasl.chanbind_len = 0;
#endif
		//
		// Retrieve the Quick-DiaSASL server response
		if (diasasl_process (&dianode, true) != 0) {
log_debug ("pam_saltty_diasasl: Failure processing Quick-DiaSASL response message");
			pamrc = PAM_SERVICE_ERR;
			goto memsoxfail;
		}
		//
		// Do not send a new challenge when the last response was final
		if (sasl->_success || sasl->_failure) {
log_debug ("pam_saltty_diasasl: Whee, SASL is done; success=%d, failure=%d", sasl->_success, sasl->_failure);
			break;
		}
		//
		// Prompt the client for another round -- without or with token
log_debug ("pam_saltty_diasasl: Prompting the user with a challenge #%d which is NULL if %d", sasl->_s2c_token.buflen, sasl->_s2c_token.bufptr == NULL);
		if (mem_isnull (&sasl->_s2c_token)) {
			//
			// Since pam_prompt() sends to stderr, we produce our own prommpt
			printf (s_SOH "sasl" s_SO s_ENQ s_ETB s_SO);
			fflush (stdout);
			rdlen = read (0, rdbuf, sizeof (rdbuf)-1);
log_debug ("read() returned %zd at %s:%d", rdlen, __FILE__, __LINE__);
			if (rdlen <= 0) {
				pamrc = PAM_CONV_ERR;
			} else {
				rdbuf [rdlen] = '\0';
				pamrc = PAM_SUCCESS;
			}
		} else {
			char send [2 * sasl->_s2c_token.buflen];
			log_debug ("Token binary-escaping-with-DLE into buffer #%d", sizeof (send));
			int sendlen = bin_escape_DLE (send, &sasl->_s2c_token);
			log_debug ("      binary-escaped to #%d", sendlen);
			//
			// Since pam_prompt() sends to stderr, we produce our own prommpt
			printf (s_SOH "sasl" s_SO s_ENQ s_US "%.*s" s_ETB s_SO,
				sendlen, send);
			fflush (stdout);
			rdlen = read (0, rdbuf, sizeof (rdbuf)-1);
log_debug ("read() returned %zd at %s:%d", rdlen, __FILE__, __LINE__);
			if (rdlen <= 0) {
				pamrc = PAM_CONV_ERR;
			} else {
				rdbuf [rdlen] = '\0';
				pamrc = PAM_SUCCESS;
			}
		}
		if (pamrc != PAM_SUCCESS) {
log_debug ("pam_saltty_diasasl: Continued prompting of user failed");
			goto memsoxfail;
		}
		//
		// Loop back to process the prompt response
	}


	//NO//
	//
	// Send the verdict, possible with additional data upon success
	if (sasl->_failure) {
		log_debug ("Reporting failure with <ENQ><NAK><ETB> and teardown with <ENQ><CAN><ETB>");
		printf (s_SOH "sasl" s_SO s_ENQ s_NAK s_ETB s_ENQ s_CAN s_ETB s_SO);
	} else if (mem_isnull (&sasl->_s2c_token)) {
		log_debug ("Reporting success with <ENQ><ACK><ETB>");
		printf (s_SOH "sasl" s_SO s_ENQ s_ACK s_ETB s_SO);
	} else {
		char send [2 * sasl->_s2c_token.buflen];
		int sendlen = bin_escape_DLE (send, &sasl->_s2c_token);
		log_debug ("Reporting success with <ENQ><ACK><US>...token...<ETB>");
		printf (s_SOH "sasl" s_SO s_ENQ s_ACK s_US "%.*s" s_ETB s_SO,
				sendlen, send);
	}
#ifdef OLD_PAM_CODE
	tty_sasl = false;
#endif
	//
	// Setup the verdict to return to PAM
	pamrc = (sasl->_success && !sasl->_failure) ? PAM_SUCCESS : PAM_PERM_DENIED;
log_debug ("pam_saltty_diasasl: Concluding to report %d", pamrc);
	//
	// Clean up and return values
memsoxfail:
	if (sasl != NULL) {
		mem_close ((void **) &sasl);
	}


	//NO//
soxfail:
	close (dianode.socket);
diafail:
	diasasl_node_close (&dianode);
	sasltext_fini (&multty_sasl);		//YES//FRAGMEM//
	multty_sasl.mty.flow = NULL;
	multtyprog_delstream (multtyprog_main, &multty_sasl.mty);
	multtyflow_fini (&mufl);
log_debug ("pam_saltty_diasasl: pam_saltty_diasasl returns %d", pamrc);
	//
	// Possibly send SASL teardown to the TTY
#ifdef OLD_PAM_CODE
	if (tty_sasl) {
		printf (s_SOH "sasl" s_SO s_ENQ s_CAN s_ETB s_SO);
	}
#else
	//TODO// Moves elsewhere
	if (sasl->_tty_sasl_active) {
		printf (s_SOH "sasl" s_SO s_ENQ s_CAN s_ETB s_SO);
		sasl->_tty_sasl_acive = false;
	}
#endif


	//YES//ALREADY_DONE//
	//
	// Return to the original terminal mode, presumably cooked
	if (tty_changed) {
		tcsetattr (0, TCSADRAIN, &tty_orig);
	}
	//
	// Make sure that the module leaves no output pending
	fflush (stdout);


	//NO//
	//
	// Return the collected PAM error/ok code
	return pamrc;
}


/* Handle credentials.  But since authentication is delegated, there is
 * really nothing to do.  But without this function, PAM errors arise.
 */
PAM_EXTERN int pam_sm_setcred (pam_handle_t *pamh, int flags, int argc, const char **argv) {
	return PAM_SUCCESS;
}

#endif
