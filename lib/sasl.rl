/* SASL stream parsing for mulTTY, with Ragel.
 *
 * This is a parser intended to run on the contents of a mulTTY stream
 * named "sasl".  The use of this named stream is more consistent than
 * hijacking a single ASCII character for a specific function.
 *
 * Ragel generates parsers which can store all state in a few variables.
 * As a result, any stream can stop parsing at any time, and continue
 * where it left off when new data pours in.  This helps to redirect
 * characters.  Ragel can also handle binary codes, and can handle
 * DLE-escaped specials.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <assert.h>

#include <arpa2/except.h>
#include <arpa2/multty.h>


%%{

machine mulTTY_SASL;

include mulTTY "ascii.rl";


#
# SASL mechanism name and non-empty list of SASL mechanism names.
#
saslmechname = [A-Z0-9\-_]+
	;
saslmechlist = ( saslmechname .  ( ' ' . saslmechname )* )
		>{ p0 = p; first = true; }
		%{ mty->text_cb (mty, mtyf->progress, p0, (unsigned) (p-p0), first, true); }
		%{ mtyf->progress = MULTTY_RESET; }
	;

#
# Hostname and service name as used in SASL configuration.
#
hostname = [a-z0-9][a-z0-9\-]* ( '.' . [a-z0-9][a-z0-9\-]* )*
	;
service = [a-zA-Z0-9\-_.]+
	;

#
# SASL initiation and teardown by client or server.
#
sasl_s2c_initiate = CAN
	. hostname
		>{ mtyf->progress = MULTTY_SASL_S2C_INITIATE_HOSTNAME; p0 = p; first = true; }
		%{ mty->text_cb (mty, mtyf->progress, p0, (unsigned) (p-p0), first, true); }
		%{ mtyf->progress = MULTTY_RESET; }
	US
	. service
		>{ mtyf->progress = MULTTY_SASL_S2C_INITIATE_SERVICE; p0 = p; first = true; }
		%{ mty->text_cb (mty, mtyf->progress, p0, (unsigned) (p-p0), first, true); }
		%{ mtyf->progress = MULTTY_RESET; }
	US
	. saslmechlist
		>{ mtyf->progress = MULTTY_SASL_S2C_INITIATE_MECHLIST; }
	. ETB
	;
sasl_s2c_teardown = CAN . ETB
		${ mty->event_cb (mty, MULTTY_SASL_S2C_TEARDOWN, false, false); }
	;
sasl_c2s_teardown = CAN . ETB
		${ mty->event_cb (mty, MULTTY_SASL_C2S_TEARDOWN, false, false); }
	;


#
# SASL tokens in both directions.
#
sasl_c2s_opttoken = 
	(
	US
		${ log_debug ("Found start of c2s token"); }
		${ p0 = p; first = true; mtyf->progress = MULTTY_SASL_C2S_TOKEN; }
	. bindata
	. ETB
		${ mtyf->progress = MULTTY_RESET; }
	) | (
	ETB
		${ log_debug ("Found marker of NO c2s token"); }
		${ mty->event_cb (mty, MULTTY_SASL_C2S_NO_TOKEN, false, false); }
	)
	;
sasl_c2s_mech_opttoken =
	CAN
		${ log_debug ("Found initiation of c2s SASL exchange"); }
		${ p0 = p1 = p + 1; first = true; mtyf->progress = MULTTY_SASL_C2S_MECHNAME; }
	. saslmechname
		${ p1++; }
		%{ mty->text_cb (mty, mtyf->progress, p0, (unsigned) (p1-p0), first, true); }
		%{ mtyf->progress = MULTTY_RESET; p0 = p1; }
		%{ log_debug ("Got c2s SASL mech name, now opting for token"); }
	. sasl_c2s_opttoken
		>{ if (p0 != p1) {
			mty->text_cb (mty, mtyf->progress, (p1 != p0) ? p0 : NULL, (unsigned) (p1-p0), first, true);
		   }
		 }
	;
sasl_s2c_opttoken = (
	US
		${ log_debug ("Found start of S2C token"); }
		${ mtyf->progress = MULTTY_SASL_S2C_TOKEN; }
	. bindata
 	. ETB
		${ mtyf->progress = MULTTY_RESET; }
	) | (
	ETB
		${ mty->event_cb (mty, MULTTY_SASL_S2C_NO_TOKEN, false, false); }
	)
	;

#
# SASL accept or reject by the server.
#
sasl_s2c_verdict = (
	ACK
		#TODO# The following repeats the ENQ
		#NOTE# The following ends with ETB
	. sasl_s2c_opttoken
		%{ mty->event_cb (mty, MULTTY_SASL_SUCCESS, false, false); }
	) | (
	NAK . ETB
		%{ mty->event_cb (mty, MULTTY_SASL_FAILURE, false, false); }
	)
	;


#
# SASL tokens in either direction.
#
# TODO: <SYN><ETB> may tickle the server to request authentication
#
sasl_s2c_pdu = ENQ . ( sasl_s2c_initiate | sasl_s2c_verdict | sasl_s2c_teardown | sasl_s2c_opttoken )
	;
sasl_c2s_pdu = SYN . ( sasl_c2s_teardown | sasl_c2s_mech_opttoken | sasl_c2s_opttoken )
	;


#
# SASL flows c2s, s2c and mixed.
#
# These machines may be instantiated to process specifically the
# SASL stream, independently from the machines to recognised streams
# and/or multiplexing.
#
sasl_c2s := (  sasl_c2s_pdu )+
	;
sasl_s2c := ( sasl_s2c_pdu )+
	;
sasl_x2x := ( sasl_c2s_pdu | sasl_s2c_pdu )+
	;

}%%



/* We instantiate the machine that can do mulTTY streams
 * as well as multiplexers.  Through a initialisation flag,
 * the actual machine can then be selected.
 */
%% machine mulTTY_SASL;
%% write data;


/* The parser callback for SASL, which can be hung into the stream
 * receiving it.  This will be used in preference of any other
 * callback routines, it uses the grammar defined herein and
 * triggers textual and event callbacks specific to SASL.
 */
void multtysasl_parse_cb (struct multty *mty, multty_event_t _event,
			char *txt, unsigned len,
			bool first_up, bool last_up) {
	//
	// Prepare the parser
	struct multtyflow *mtyf = mty->flow;
	int cs = first_up ? mulTTY_SASL_en_sasl_x2x: mty->ragel_cs;
	bool first = false;
	bool last = false;
	char *p = txt;
	char *pe = txt + len;
	char *eof = last_up ? pe : (pe + 1);
	char *p0 = p;
	char *p1 = p;
	//
	// Now loop until all input has been parsed
	do {
		//
		// Execute the parser
		%%{
			machine mulTTY_SASL;
			write exec;
		}%%
		//
		// Special handling if we stopped early
		if (p < pe) {
			//
			// We do not pass along syntax faults
			if (p0 != NULL) {
				//
				// Remove the characters from a current string
				mty->text_cb (mty, mtyf->progress, p0, (unsigned) (p-p0), first, false);
				first = false;
				p0 = p;
			} else {
				//
				// Remove the character without being in a string
				;
			}
			p++;
			continue;
		}
	} while (p < pe);
	//
	// Handle the end of the stream, or just the end of current input
	if (last_up) {
		//
		// The input ends here; is SASL finished too?
		if (cs < mulTTY_SASL_first_final) {
			//
			// Error Condition: SASL broken off mid-stream
			log_debug ("Signaling mulTTY SASL FAILURE because of trailing content");
			mty->event_cb (mty, MULTTY_SASL_FAILURE, false, false);
		} else {
			//
			// SASL ended and so did the stream; no loose ends
			;
		}
	} else {
		//
		// If any form of text is being delivered, deliver it now
		//  - This may be the first and following partials will not be
		//  - This is not the last, because the text is still being parsed
		if (mtyf->progress != MULTTY_RESET) {
			log_debug ("Delivering as a last-resort SASL parsing service");
			mty->text_cb (mty, mtyf->progress, p0, (unsigned) (p1-p0), first, false);
		}
	}
	//
	// Remember the parser state
	mty->ragel_cs = cs;
}

