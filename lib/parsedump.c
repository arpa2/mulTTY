/* parsedump.c -- Parse a mulTTY flow and dump in text
 *
 * This program is used for testing.  Its detailed output contains
 * the various mulTTY forms in textual form, so the compare easily.
 * This is used, among others, to debug and keep mulTTY consistent.
 *
 * TODO: Most of this moved to generic tool src/ascii-ctrl2bin.c
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa2/multty.h>



/* Callback reporting on text received, including prefix names/descriptions
 * for an upcoming mulTTY event.
 */
void text_cb (struct multtyflow *mtyf, multty_event_t evt,
		char *txt, unsigned len,
		bool first, bool last) {
	char *kind = NULL;
	if (first) {
		kind = last ? "only" : "frst";
	} else {
		kind = last ? "last" : "part";
	}
	printf ("TEXT %d/%s%3d long, \"%.*s\"\n",
			evt, kind,
			len,
			len, txt);
}


/* Callback reporting on the events received.
 */
void event_cb (struct multtyflow *mtyf, multty_event_t evt,
		bool named, bool described) {
	printf ("EVENT %d, named: %s, described: %s\n",
			evt,
			named ? "yes" : "no",
			described ? "yes" : "no");
}


/* Callback reporting parsing errors (bad characters).
 */
bool badchar_cb (struct multtyflow *mtyf, multty_event_t evt,
		char *bad) {
	printf ("ERROR: BAD CHARACTER '%c' TO SKIP IN \"%s\"\n", *bad, bad);
	return false;
}



int main (int argc, char *argv []) {
	//
	// Parse arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: %s infile.asc...\nPurpose: Parse each input file as a separate chunk.\n", *argv, *argv);
		exit (1);
	}
	//
	// Setup a flow object
	struct multtyflow mtyf;
	multtyflow_init (&mtyf, false);
	mtyf.text_cb = text_cb;
	mtyf.event_cb = event_cb;
	mtyf.badchar_cb = badchar_cb;
	//
	// Iterate over files, opening and parsing them
	for (int argi = 1; argi < argc; argi++) {
		int infile = open (argv [argi], O_RDONLY);
		if (infile < 0) {
			perror ("Failed to open input file");
			exit (1);
		}
		//
		// Process input, one chunk at a time
		char rdbuf [1024];
		ssize_t rdlen;
		while (rdlen = read (infile, rdbuf, sizeof (rdbuf)), rdlen > 0) {
			multtyflow_process_text (&mtyf, rdbuf, rdlen);
		}
		if (rdlen < 0) {
			perror ("Error reading");
			exit (1);
		}
		close (infile);
	}
	//
	// Close the parser, and test if it is happy
	if (!multtyflow_fini (&mtyf)) {
		fprintf (stderr, "The parser returned an error condition\n");
		exit (1);
	}
	return 0;
}

