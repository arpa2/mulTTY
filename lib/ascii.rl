/* ASCII parsing for mulTTY, with Ragel.
 *
 * Ragel generates parsers which can store all state in a few variables.
 * As a result, any stream can stop parsing at any time, and continue
 * where it left off when new data pours in.  This helps to redirect
 * characters.  Ragel can also handle binary codes, and can handle
 * DLE-escaped specials.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <arpa2/except.h>
#include <arpa2/multty.h>



%%{

machine mulTTY;

#
# ASCII control codes.  Not covered are those with special meaning in common
# applications, such as BEL, CR, LF, HT, VT, FF, ESC.
#
NUL = 0x00 ;
SOH = 0x01 ;
STX = 0x02 ;
ETX = 0x03 ;
EOT = 0x04 ;
ENQ = 0x05 ;
ACK = 0x06 ;
SO  = 0x0e ;
SI  = 0x0f ;
DLE = 0x10 ;
DC1 = 0x11 ;
DC2 = 0x12 ;
DC3 = 0x13 ;
DC4 = 0x14 ;
NAK = 0x15 ;
SYN = 0x16 ;
ETB = 0x17 ;
CAN = 0x18 ;
EM  = 0x19 ;
SUB = 0x1a ;
FS  = 0x1c ;
GS  = 0x1d ;
RS  = 0x1e ;
US  = 0x1f ;


#
# Plain character, that is, one that need not be escaped.
#
# Note that ASCII includes a DEL character which is special;
# it punctures all holes in a paper tape and thereby removes
# a character that once was there.
# 
# Also note that telnet uses 0xff as its escape character.
# This character does not occur in valid UTF-8 sequences but,
# like other non-plain codes, may appear in binary content if
# that is DLE-escaped.
#
plain = 0x07..0x0d | 0x1b | 0x20..0x7e | 0x80..0xfe
	;


#
# Non-plain code points (character bytes) are all that are not plain.
#
#TODO# Not used.  What about NUL?
#
non_plain = any - plain
	;

#
# Non-mulTTY code points (character bytes) are those not used below.
# In fact, US is not even in this list because it never occurs first.
# However, NUL was added because it must not be passed in the plain.
#
# We take out DC1 through DC4 because they always need to be escaped
# so their raw occurrence can be interpreted at the device driver layer.
#
non_multty = any - ( NUL | SOH | SO | SI | DLE | ETX | EOT | STX | SUB | EM | DC1 | DC2 | DC3 | DC4 );


#
# DLE-escaped sequences for characters in the range 0x00..0x1f as well as
# 0x7f and 0xff.  More precisely, the things after DLE must not be any of
# these because that defies the purpose of escaping.  Note that it is also
# possible to DLE-escape the DLE symbol.
#
# It is possible, but not required, to escape LF, CR, HT, FF, ESC, ...
# The true requirement is that the set of characters after DLE, combined
# with the codes XORed with 0x40, forms all possible byte codes; there
# is bound to be some overlap.
#
# All possible bytes can be formed from strings of escaped|plain.
#
# In cases where a parse_cb is defined, the <DLE> escapes in the
# output; without it, the escaping is removed before delivery.
#
escaped = DLE 
		${ if ((mtyf->curin != NULL) && (mtyf->curin->parse_cb != NULL)) *p1++ = *p; }
	. plain
		${ *p1++ = ((mtyf->curin != NULL) && (mtyf->curin->parse_cb != NULL)) ? *p : (0x40 ^ *p); }
	;


#
# Data is a sequence of characters or bytes that has no special meaning
# to mulTTY, and as a consequence is passed literally.
#
# Character data is passed as is, it is plain so has no DLE embedded.
#
# Binary data is passed after DLE unescaping or DLE skipping.
#
# Binary text is like binary data, but it outputs as content and is
# built from escaped and non_multty code points.
#
action setup_content {
	first = true;
	last = false;
	p0 = p1 = p;
}
action seenall_content {
	last = true;
}
action deliver_content {
	if (mty != NULL) {
		//
		// Inside a parser, so deliver content to the stream
		log_debug ("Content delivery type %d to stream callback", mtyf->progress);
		mty->text_cb
			(mty,        mtyf->progress, p0, (unsigned) (p1-p0), first, last);
	} else if ((mtyf->curin != NULL) && (mtyf->curin->parse_cb != NULL)) {
		//
		// Outside a parser, but we have one so we deliver to it
		log_debug ("Content delivery type %d to the parser for the curin stream", mtyf->progress);
		mtyf->curin->parse_cb
			(mtyf->curin, mtyf->progress, p0, (unsigned) (p1-p0), first, last);
	} else {
		//
		// Standard case, direct delivery to the callback
		log_debug ("Content delivery type %d to the flow callback", mtyf->progress);
		mtyf->text_cb
			(mtyf,        mtyf->progress, p0, (unsigned) (p1-p0), first, last);
	}
	p0 = NULL;
}
chardata = plain+
		>{ p0 = p1 = p; first = true; }
		${ p1++; }
		%{ mtyf->text_cb (mtyf, mtyf->progress, p0, (unsigned) (p1-p0), first, true); p0 = NULL; }
	;
bindata = (
	  escaped
	| plain
		${ *p1++ = *p; }
	)+
		>setup_content
		%seenall_content
		%deliver_content
	;
bintext = (
	  escaped
	| non_multty
		${ *p1++ = *p; }
	)+
		>{ mtyf->progress = MULTTY_CONTENT; }
		>setup_content
		%seenall_content
		%deliver_content
	;


#
# Name prefix, starting with SOH, and possibly with US to mark a descripton.
#
# The prename_opt is optional, and sets flags to accommodate that.
#
prename = SOH
		${ mtyf->progress = MULTTY_PREFIX_NAME; }
		%{ mtyf->named = true; }
	.  ( chardata
	   | ""
		%{ log_error ("Missing mulTTY stream name"); mtyf->named = false; }
	   ) .
	( US
		${ mtyf->progress = MULTTY_DESCRIPTION; }
		%{ mtyf->described = mtyf->named; }
	  . ( chardata
	    | ""
		%{ log_error ("Missing mulTTY stream description"); mtyf->described = false; }
	    )
	)?
	;
#
prename_opt = prename?
		>{ mtyf->named = false; mtyf->described = false; }
		%{ mtyf->progress = MULTTY_RESET; }
	;



#
# Shift to a stream.  This is either SI or SO (it does not matter which)
# and it may have an SOH naming prefix.  Without naming prefix, it uses
# the empty string to signify the default stream.
#
shift_stream = prename_opt . ( SI | SO )
		${ mtyf->siso_in = *p; }
		${ mtyf->event_cb (mtyf, MULTTY_SHIFT_STREAM, mtyf->named, mtyf->described); }
	;


#
# End of a stream.
#
end_stream = prename_opt . EM
		${ mtyf->event_cb (mtyf, MULTTY_END_STREAM, mtyf->named, mtyf->described); }
	;


#
# Non-multiplexed flow, so one program but multiple streams.
#
# This machine can be instantiated in programs that are not
# aware of multiplexing.  The state diagram of such instances
# is simpler but the multiplexing codes are not acceptable
# to the grammar.
#
program_flow := bintext? . ( ( shift_stream | end_stream ) . bintext? )*
	;

}%%



%%{

machine mulTTYplex;
include mulTTY;


#
# Program switch, for multiplexes of programs in one flow.
#
# ETX moves to the [current/named] program in the parent set.
# EOT stops    the [current/named] program.
# STX moves to the [current/named] child program.
# SUB moves to the [former /named] program in the current set.
#
switch_program = prename_opt
	( STX
		${ mtyf->event_cb (mtyf, MULTTY_PROGRAM_CHILD,   mtyf->named, mtyf->described); }
	| ETX
		${ mtyf->event_cb (mtyf, MULTTY_PROGRAM_PARENT,  mtyf->named, mtyf->described); }
	| SUB
		${ mtyf->event_cb (mtyf, MULTTY_PROGRAM_SIBLING, mtyf->named, mtyf->described); }
	| EOT
		${ mtyf->event_cb (mtyf, MULTTY_PROGRAM_STOP,    mtyf->named, mtyf->described); }
	)
	;


#
# Multiplexed flow, so any number of programs, each with streams.
#
# This machine can be instantiated in programs that aware of
# both multiplexing and streams.  The state diagram represents
# the full grammar for mulTTY.
#
multiplexed_flow := bintext? . ( ( shift_stream | end_stream | switch_program ) . bintext? )*
	;

}%%



/* We instantiate the machine that can do mulTTY streams
 * as well as multiplexers.  Through a initialisation flag,
 * the actual machine can then be selected.
 */
%% machine mulTTYplex;
%% write data;



static struct multtyflow *main_flow;


void multtyflow_init (struct multtyflow *mtyf, bool multtyplex) {
	//
	// Wipe any contents of the structure
	memset (mtyf, 0, sizeof (struct multtyflow));
	//
	// Initialise siso to the more common <SO>
	mtyf->siso_in = mtyf->siso_out = c_SO;
	//
	// Perform Ragel's one-time parser initialisation
	%%{
		machine mulTTYplex;
		write init nocs;
	}%%
	//
	// Start parsing at the beginning of an in-flow
	mtyf->ragel_cs = multtyplex
			? mulTTYplex_en_multiplexed_flow
			: mulTTYplex_en_program_flow;
	//
	// Without multtyplexing, links are not static but predetermined
	if ((!multtyplex) && (main_flow == NULL)) {
		//
		// Set this flow as the main flow
		main_flow = mtyf;
		//
		// Set the (single) flow in various fields
		multty_stdin ->flow =
		multty_stdout->flow =
		multty_stderr->flow =
		multty_stdctl->flow =
		multty_stdopt->flow =  mtyf;
		//
		// Set the current input/output program and stream
		mtyf->curoutprog =
		mtyf->curinprog  = multtyprog_main;
		mtyf->curout = multty_stdout;
		mtyf->curin  = multty_stdin ;
		//
		// If not set yet, set the real file handles to stdin/stdout
		if (mtyf->realin == NULL) {
			mtyf->realin  = stdin ;
		}
		if (mtyf->realout == NULL) {
			mtyf->realout = stdout;
		}
	}
}


bool multtyflow_fini (struct multtyflow *mtyf) {
	bool ok = true;
	//
	// Deliver any outstanding MULTTY_CONTENT as closing text
	// but prefixname and description are meaningless on their own
	//  - This may be the first, and following partials will not be
	//  - This is not the last, because the text is still being parsed
	if (mtyf->progress == MULTTY_CONTENT) {
		mtyf->text_cb (mtyf, mtyf->progress, "", 0, false, true);
	}
	//
	// Test if the parser ended in a suitable end state
	if (mtyf->ragel_cs < mulTTYplex_first_final) {
		ok = false;
	}
	//
	// Clear links for future multtyflow_init() invocations
	if (mtyf == main_flow) {
		main_flow = NULL;
		multty_stdin->flow  =
		multty_stdout->flow =
		multty_stderr->flow =
		multty_stdctl->flow =
		multty_stdopt->flow = NULL;
		mtyf->curoutprog =
		mtyf->curinprog  = NULL;
		mtyf->curout =
		mtyf->curin  = NULL;
	}
	//
	// Report whether any problems were encountered
	return ok;
}


void multtyflow_process_text (struct multtyflow *mtyf, char *txt, unsigned len) {
log_debug ("Processing text in multtyflow_process_text()");
log_data ("Text", txt, len, 0);
	//
	// Prepare the parser
	struct multty *mty = NULL;
	int cs = mtyf->ragel_cs;
	bool first = false;
	bool last = false;
	char *p = txt;
	char *pe = txt + len;
	char *eof = (len == 0) ? pe : (pe + 1);
	char *p0 = p;
	char *p1 = p;
	//
	// Now loop until all input has been parsed
	do {
		//
		// Execute the parser
		%%{
			machine mulTTYplex;
			write exec;
		}%%
		//
		// Special handling if we stopped early
		if (p < pe) {
			//
			// Test if syntax fault should be passed along
			if (mtyf->badchar_cb (mtyf, mtyf->progress, p)) {
				//
				// Pass through the character, then continue
				;
			} else if (p0 != NULL) {
				//
				// Remove the character from a current string
				//TODO// WHAT_TO_DO_HERE
				mtyf->text_cb (mtyf, mtyf->progress, p0, (unsigned) (p-p0), first, false);
				first = false;
				p0 = p;
			} else {
				//
				// Remove the character without being in a string
				;
			}
			p++;
			continue;
		}
	} while (p < pe);
	//
	// If any form of text is being delivered, deliver it now
	//  - This may be the first, and following partials will not be
	//  - This is not the last, because the text is still being parsed
	if ((mtyf->progress != MULTTY_RESET) && (p0 != NULL) && (p0 != p1)) {
log_debug ("Delivering remaining text after ASCII parsing in multtyflow_process_text() for type %d", mtyf->progress);
log_data ("Remainder", p0, (unsigned) (p1-p0), 0);
		mtyf->text_cb (mtyf, mtyf->progress, p0, (unsigned) (p1-p0), first, false);
	}
	//
	// Remember the parser state
	mtyf->ragel_cs = cs;
}

