/* multty.c -- Basic functions for mulTTY flow and streams.
 *
 * This set of functions defined mulTTY flows and streams, as well as
 * limited use of programs.  It also defines the "stdio" and "errctl"
 * streams, and mounts them into the current "main" program.  All this
 * is setup in global data, requiring no special calls.
 *
 * This does not offer multiplexer functionality, nor specialised
 * parsers such as for SASL or ASNI colours.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
// #include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <arpa2/multty.h>

#include <errno.h>


struct multty _multty_stdopt = {
	.next = NULL,
	.prev = &_multty_stdctl,
	.flow = NULL /* Filled later */,
	.program = multtyprog_main,
	.name = { 's', 't', 'd', 'o', 'p', 't', 0 },
	.description = NULL,
	.description_fresh = false,
	//TODO//ASYMM// .infd  = -1,
	//TODO//ASYMM// .outfd = -1,
	//NOCONST// .infile  = stdin,
	//NOCONST// .outfile = stdout,
};

struct multty _multty_stdctl = {
	.next = &_multty_stdopt,
	.prev = &_multty_stderr,
	.flow = NULL /* Filled later */,
	.program = multtyprog_main,
	.name = { 's', 't', 'd', 'c', 't', 'l', 0 },
	.description = NULL,
	.description_fresh = false,
	//TODO//ASYMM// .infd  = -1,
	//TODO//ASYMM// .outfd = -1,
	//NOCONST// .infile  = stdin,
	//NOCONST// .outfile = stdout,
};

struct multty _multty_stderr = {
	.next = &_multty_stdctl,
	.prev = &_multty_stdin,
	.flow = NULL /* Filled later */,
	.program = multtyprog_main,
	.name = { 's', 't', 'd', 'e', 'r', 'r', 0 },
	.description = NULL,
	.description_fresh = false,
	//TODO//ASYMM// .infd  = -1,
	//TODO//ASYMM// .outfd = 2,
	//NOCONST// .infile  = NULL /* TODO: stdctl */,
	//NOCONST// .outfile = stderr,
};

struct multty _multty_stdout = {
	.next = &_multty_stderr,
	.prev = &_multty_stdin,
	.flow = NULL /* Filled later */,
	.program = multtyprog_main,
	.name = { 's', 't', 'd', 'o', 'u', 't', 0 },
	.description = NULL,
	.description_fresh = false,
	//TODO//ASYMM// .infd  = -1,
	//TODO//ASYMM// .outfd = 1,
	//NOCONST// .infile  = stdin,
	//NOCONST// .outfile = stdout,
};

struct multty _multty_stdin = {
	.next = &_multty_stdout,
	.prev = NULL,
	.flow = NULL /* Filled later */,
	.program = multtyprog_main,
	.name = { 's', 't', 'd', 'i', 'n', 0 },
	.description = NULL,
	.description_fresh = false,
	//TODO//ASYMM// .infd  = 0,
	//TODO//ASYMM// .outfd = -1,
	//NOCONST// .infile  = NULL /* TODO: stdctl */,
	//NOCONST// .outfile = stderr,
};

#if 0
struct multty _multty_errctl = {
	.next = NULL,
	.prev = multty_stdio,
	.flow = NULL /* Filled later */,
	.program = multtyprog_main,
	.name = { 'e', 'r', 'r', 'c', 't', 'l', 0 },
	.description = NULL,
	.description_fresh = false,
	//TODO//ASYMM// .infd  = -1,
	//TODO//ASYMM// .outfd = 2,
	//NOCONST// .infile  = NULL /* TODO: stdctl */,
	//NOCONST// .outfile = stderr,
};
#endif

#if 0
struct multty _multty_stdio = {
	.next = multty_errctl,
	.prev = NULL,
	.flow = NULL /* Filled later */,
	.program = multtyprog_main,
	.name = { 's', 't', 'd', 'i', 'o', 0 },
	.description = NULL,
	.description_fresh = false,
	//TODO//ASYMM// .infd  = 0,
	//TODO//ASYMM// .outfd = 1,
	//NOCONST// .infile  = stdin,
	//NOCONST// .outfile = stdout,
};
#endif

struct multtyprog _multtyprog_main = {
	.next     = NULL,
	.prev     = NULL,
	.parent   = NULL,
	.children = NULL,
	.name = { 'm', 'a', 'i', 'n', '\0' },
#if 0
	.curin  = multty_stdio,
	.curout = multty_stdio,
#endif
	.streams = &_multty_stdin ,
	.curin   = &_multty_stdin ,
	.dflin   = &_multty_stdin ,
	.curout  = &_multty_stdout,
	.dflout  = &_multty_stdout,
};


bool multtyprog_addstream (struct multtyprog *prog, struct multty *mty) {
	assert (mty->program == NULL);
	struct multty **mp = &prog->streams;
	struct multty *here = NULL;
	while (*mp != NULL) {
		here = *mp;
		if (strncmp (mty->name, here->name, 33) == 0) {
			/* Could not add -- name already exists */
			return false;
		}
		mp = &here->next;
	}
	mty->prev = here;
	mty->next = NULL;
	*mp = mty;
	/* Successfully added */
	mty->program = prog;
	return true;
}

bool multtyprog_delstream (struct multty *mty) {
	assert (mty->program != NULL);
	struct multty **mp = &mty->program->streams;
	struct multty *prev = NULL;
	while (*mp != NULL) {
		if (*mp == mty) {
			(*mp)->next = mty->next;
			if (mty->next != NULL) {
				(mty->next)->prev = prev;
			}
			/* Successfully removed */
			mty->program = NULL;
			return true;
		}
		prev = *mp;
		mp = &prev->next;
	}
	/* Could not delete -- list entry does not exist */
	return false;
}


#if 0
//TODO// REWRITE TO STDIO USING mtyf->realin
void multtyflow_process (struct multtyflow *mtyf) {
	char buf [1030];
	ssize_t rdlen = read (mtyf->curin->infd, buf, 1024);
	if (rdlen < 0) {
		/* some kind of error occurred */
		if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
			perror ("Failed to read");
		}
	} else if (rdlen == 0) {
		/* end of file */
		close (mtyf->curin->infd);
		mtyf->curin->infd = -1;
	} else {
		/* content received, now process it */
		multtyflow_process_text (mtyf, buf, rdlen);
	}
}
#endif


ssize_t mtyescwrite (multty_t *mty, const uint8_t *bufptr, size_t buflen, uint32_t style) {
	//
	// Correctness assumptions and stream setup
	assert (mty->program == &_multtyprog_main);
	assert (mty->flow != NULL);
	mtyflow_outputstream (mty);
	//
	// Escape the string as needed, and pass to fwrite()
	ssize_t written = 0;
	while (buflen > 0) {
		//
		// Find a prefix length that needs no escape
		int noesclen = 0;
		while (noesclen < buflen) {
			if (mtyescapewish (style, bufptr [noesclen])) {
				break;
			}
			noesclen++;
		}
		//
		// Output the prefix that needs no escape
		if (noesclen > 0) {
			ssize_t wadd = fwrite (bufptr, 1, noesclen, mty->flow->realout);
			written += wadd;
			if (wadd != noesclen) {
				goto interrupt;
			}
		}
		//
		// Skip to the character that needs an escape
		bufptr += noesclen;
		buflen -= noesclen;
		//
		// If a character remains, output it as one data element
		if (buflen > 0) {
			char escbuf [2];
			escbuf [0] = c_DLE;
			escbuf [1] = 0x40 ^ *bufptr++;
			buflen--;
			ssize_t wadd = fwrite (escbuf, 2, 1, mty->flow->realout);
			written += wadd;
			if (wadd != 1) {
				goto interrupt;
			}
		}
	}
	//
	// Return the amount written
interrupt:
	return written;
}

