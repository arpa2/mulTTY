/* pam_saltty_diasasl.c -- PAM module to proxy SASL via Quick-DiaSASL.
 *
 * This PAM module allows standard applications such as sudo(1) to
 * request authentication while in an ASCII session, into which it
 * injects codes to authenticate the client across the ASCII session
 * itself.
 *
 * This uses the "sasl" stream name to switch to the specific grammar
 * defined for SASL authentication.  This grammar encodes binary tokens
 * with <DLE> escapes and marks their end clearly, and it negotiates a
 * SASL mechanism.  This makes it an ASCII embedding of SASL, which is
 * clearly distinct from any other traffic, and it can be used to spike
 * an authentication interaction in the course of any terminal session.
 *
 * Any security mechanism of more interest than password entry uses a
 * form of challenge/response interaction, and advanced ones can even
 * be sensitive to the remote server (up to mutual authentication).
 *
 * PAM is normally used for single credential entry, which does not
 * benefit from this practice.  Again, the special demarkation of text
 * with mulTTY annotations for the SASL channel, "<SOH>sasl<SO>", are
 * helpful to indicate, from the very first token entry, that it is
 * the client's intention to engage in SASL authentication.  Anything
 * else can be handled by other modules as-is, but it is only this
 * response that makes it reasonable to remove tokens and ask for new
 * ones, without dragging a client into a black hole of data entry.
 * The client in turn, is clear about the SASL offer as soon as it
 * sees the mechanisms being offered with "<SOH>sasl<SO>" followed by
 * the challenge "<ENQ><CAN>MECH MECH</ETB>" and "<SO>" switch back.
 * In other words, it takes a very clear negotiation to enter into
 * the repeated data entry.
 *
 * We implement the repeated entry of tokens with read(0,...) calls.
 * A grammar error or even an incomplete token are considered as
 * authentication failure.  Note that this can pass through plain
 * existing applications like sudo(1), login(1) and even telnetd(8)
 * because they use ASCII prompts and ASCII responses as a result of
 * representing the SASL interaction in ASCII.  It will subsequently
 * pass transparently through any network connections.  The only
 * concern is to escape all token bytes that could be considered an
 * ASCII control, especially line endings must not occur spuriously
 * so we need MULTTY_ESC_BINARY encoding for SASL tokens.  Which is
 * perfectly sensible.
 *
 * Include into a pam.conf file with a line like
 *     auth sufficient pam_saltty.so
 * or
 *     auth sufficient pam_saltty.so diasasl=[2001:db8::a2:802]:3000 TODO
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <termios.h>
#include <unistd.h>

#include <security/pam_modules.h>
#include <security/pam_ext.h>

#include <arpa2/socket.h>
#include <arpa2/quick-mem.h>
#include <arpa2/quick-diasasl.h>
#include <arpa2/except.h>

#include <arpa2/multty.h>



struct diasasl_pam {
	struct diasasl_session diasasl;
	membuf mechlist;
	membuf s2c_token;
	bool success;
	bool failure;
};


void sasltext_cb (struct multty *mty, multty_event_t evt,
                        char *txt, unsigned len,
                        bool first, bool last);

void saslevent_cb (struct multty *mty, multty_event_t evt,
			bool named, bool described);


//
// Configuration options on the PAM commandline
enum config_symbols {
	CFG_HOSTNAME = 0,
	CFG_DOMAIN,
	CFG_SERVICE,
	CFG_DIASASL_IP,
	CFG_DIASASL_PORT,
	CFG_COUNT
};
char *config_keys [CFG_COUNT] = {
	"host",
	"domain",
	"service",
	"diasasl_ip",
	"diasasl_port",
};
int config_keylen [CFG_COUNT] = {
	4,
	6,
	7,
	10,
	12,
};
#if 0
char *config_defaults [CFG_COUNT] = {
	NULL,
	"host",
};
#endif



//
// A good manual on writing a PAM module:
// https://web.archive.org/web/20190523222819/https://fedetask.com/write-linux-pam-module/
//



/* Process an incoming text fragment.  Basically collect what we can get.
 * Use fragbuf as a fragment buffer and fraglen as the collected length.
 */
struct multty_ext4sasl_pam {
	struct multty mty;
	void *fragmem;
	membuf fragbuf;
	uint32_t fraglen;
	char *c2s_mechchoice;
	char *c2s_tokenbuf;
	uint32_t c2s_tokenlen;
	bool c2s_send;
	bool c2s_teardown;
};
//
void sasltext_init (struct multty_ext4sasl_pam *ext) {
	ext->c2s_mechchoice = NULL;
	ext->c2s_tokenbuf = NULL;
	ext->c2s_tokenlen = 0;
	ext->c2s_send = false;
	ext->c2s_teardown = true;
	if (mem_open (0, &ext->fragmem)) {
		if (mem_buffer_open (ext->fragmem, 1000, &ext->fragbuf)) {
			ext->c2s_teardown = false;
		} else {
			mem_close (&ext->fragmem);
		}
	}
}
//
void sasltext_fini (struct multty_ext4sasl_pam *ext) {
	mem_buffer_delete (ext->fragmem, &ext->fragbuf);
	mem_close (&ext->fragmem);
}
//
void sasltext_cb (struct multty *mty, multty_event_t evt,
			char *txt, unsigned len,
			bool first, bool last) {
	struct multty_ext4sasl_pam *ext = (struct multty_ext4sasl_pam *) mty;
	log_debug ("Processing SASL text callback type %d on \"%.*s\"", evt, len, txt);
	//
	// Do not keep state if the parser makes unexpected callbacks
	ext->c2s_tokenbuf = NULL;
	ext->c2s_tokenlen = 0;
	ext->c2s_send = false;
	//
	// Start a new token with no bytes in the buffer
	if (first) {
		ext->fraglen = 0;
	}
	//
	// Have enough space in the buffer for the token (extension)
	if (!mem_buffer_resize (ext->fragmem, ext->fraglen + len, &ext->fragbuf)) {
		ext->c2s_teardown = true;
	}
	//
	// Clone token bytes into the buffer
	if (len > 0) {
		memcpy (((uint8_t *) ext->fragbuf.bufptr) + ext->fraglen, txt, len);
		ext->fraglen += len;
	}
	//
	// If this ends the text, see if it calls for immediate processing
	if (last) {
		if (evt == MULTTY_SASL_C2S_TOKEN) {
			//
			// The client sent a token, which is now complete;
			// store its temporary coordinates and have it sent
			log_debug ("Got a c2s token size %d", ext->fraglen);
			ext->c2s_tokenbuf = ext->fragbuf.bufptr;
			ext->c2s_tokenlen = ext->fraglen;
			ext->c2s_send = true;
		} else if (evt == MULTTY_SASL_C2S_MECHNAME) {
			//
			// The client selected a mechanism; store the choice
			// and open a new buffer for the token that may follow
			log_debug ("Got a c2s mechanism choice %.*s", ext->fraglen, ext->fragbuf.bufptr);
			membuf mechbuf;
			mechbuf.bufptr = ext->fragbuf.bufptr;
			mechbuf.buflen = ext->fraglen;
			if (!mem_buffer_resize (ext->fragmem, ext->fraglen + 1, &ext->fragbuf)) {
				ext->c2s_teardown = true;
				return;
			}
			ext->fragbuf.bufptr [ext->fraglen] = '\0';
			mem_buffer_close (ext->fragmem, ext->fraglen + 1, &ext->fragbuf);
			ext->c2s_mechchoice = ext->fragbuf.bufptr;
			if (!mem_buffer_open (ext->fragmem, 1000, &ext->fragbuf)) {
				ext->c2s_teardown = true;
				return;
			}
			/* Do not send yet; there may be a client-first token */
		} else {
			/* Do not close by default, but reuse the buffer */
		}
	}
}
//
void saslevent_cb (struct multty *mty, multty_event_t evt,
			bool named, bool described) {
	struct multty_ext4sasl_pam *ext = (struct multty_ext4sasl_pam *) mty;
	log_debug ("Processing SASL event callback type %d", evt);
	if (evt == MULTTY_SASL_C2S_NO_TOKEN) {
		//
		// Explicit indication that the client will not send a token
		ext->c2s_tokenbuf = NULL;
		ext->c2s_tokenlen = 0;
		ext->c2s_send = true;
	} else if (evt == MULTTY_SASL_C2S_TEARDOWN) {
		//
		// Explicit indication that the client tears down the DiaSASL attempt
		ext->c2s_teardown = true;
	}
}


/* Escape all control characters 0x00..0x1f with <DLE> prefix.
 * Do the same with special characters 0x7f <DEL> and 0xff <IAC>.
 *
 * TODO: Exemption may be useful for <DLE> itself.
 */
static int bin_escape_DLE (char *send, struct membuf *token) {
	int sendlen = 0;
	for (int i = 0; i < token->buflen; i++) {
		char c = token->bufptr [i];
		if (((c & 0xe0) == 0x00) || ((c & 0x7f) == 0x7f)) {
			send [sendlen++] = c_DLE;
			send [sendlen++] = 0x40 ^ c;
		} else {
			send [sendlen++] =        c;
		}
	}
	return sendlen;
}


/* Callbacks from Quick-DiaSASL due to a response.
 */
static void diasasl_cb (struct diasasl_session *session,
		const int32_t *com_errno,
		char *opt_mechanism_list,
		uint8_t *s2c_token, uint32_t s2c_token_len) {
	struct diasasl_pam *sasl = (struct diasasl_pam *) session;
	assert (!sasl->success);
	assert (!sasl->failure);
	sasl->s2c_token.bufptr = NULL;
	sasl->s2c_token.buflen = 0;
	if (com_errno != NULL) {
		if (*com_errno == 0) {
			sasl->success = true;
		} else {
			sasl->failure = true;
		}
	}
	if ((com_errno == NULL) || (*com_errno == 0)) {
		if (opt_mechanism_list != NULL) {
			assert (mem_isnull (&sasl->mechlist));
			assert (mem_isnull (&sasl->s2c_token));
			sasl->mechlist.buflen = strlen (opt_mechanism_list);
			sasl->mechlist.bufptr = NULL;
			if (!mem_alloc (sasl, sasl->mechlist.buflen, (void **) &sasl->mechlist.bufptr)) {
				sasl->failure = true;
				return;
			}
			memcpy (sasl->mechlist.bufptr, opt_mechanism_list, sasl->mechlist.buflen);
		} else if (s2c_token != NULL) {
			sasl->s2c_token.bufptr = NULL;
			if (!mem_alloc (sasl, s2c_token_len, (void **) &sasl->s2c_token.bufptr)) {
				sasl->failure = true;
				return;
			}
			memcpy (sasl->s2c_token.bufptr, s2c_token, s2c_token_len);
			sasl->s2c_token.buflen = s2c_token_len;
		}
	}
}


PAM_EXTERN int pam_sm_authenticate (pam_handle_t *pamh, int flags, int argc, const char *argv []) {
log_debug ("pam_saltty_diasasl: pam_saltty_diasasl enters pam_sm_authenticate()");
	//
	// Tend towards failure, but stay open to be taught otherwise
	int pamrc = PAM_PERM_DENIED;
	//
	// When the TTY starts SASL, it should also send a failure/retraction
	bool tty_sasl = false;
	//
	// Parse arguments
	char *config_values [CFG_COUNT];
	memset (config_values, 0, sizeof (config_values));
	for (int argi = 0; argi < argc; argi++) {
		for (int keyi = 0; keyi < CFG_COUNT; keyi++) {
			if (strncmp (argv [argi], config_keys [keyi], config_keylen [keyi]) != 0) {
				continue;
			}
			char *possval = (char *) (argv [argi] + config_keylen [keyi]);
			if (*possval++ != '=') {
				continue;
			}
			if (config_values [keyi] != NULL) {
				log_error ("Doubly defined key \"%s\"", config_keylen [keyi]);
				return PAM_NO_MODULE_DATA;
			}
			config_values [keyi] = possval;
		}
	}
	char althostname [256];
	if (gethostname (althostname, sizeof (althostname)) < 0) {
		althostname [0] = '\0';
	} else {
		althostname [sizeof (althostname)-1] = '\0';
	}
	if (config_values [CFG_HOSTNAME] == NULL) {
		if (althostname [0] == '\0') {
			log_error ("Failed to extract fully qualified host name, please configure host=...");
			return PAM_NO_MODULE_DATA;
		} else if (strchr (althostname, '.') == NULL) {
			log_error ("Please add the domain in the host=... configuration");
			return PAM_NO_MODULE_DATA;
		}
		config_values [CFG_HOSTNAME] = althostname;
	}
	char *altdomain = strchr (config_values [CFG_HOSTNAME], '.');
	if (altdomain != NULL) {
		altdomain++;
	}
	if (config_values [CFG_DOMAIN] == NULL) {
		if (altdomain == NULL) {
			log_error ("Failed to extract domain name, please configure domain=...");
			return PAM_NO_MODULE_DATA;
		}
		config_values [CFG_DOMAIN] = altdomain;
	}
	if (strchr (config_values [CFG_DOMAIN], '.') == NULL) {
		log_error ("Domain is not fully qualified (no dots)");
		return PAM_NO_MODULE_DATA;
	}
	if (config_values [CFG_SERVICE] == NULL) {
		config_values [CFG_SERVICE] = "host";
	}
	if (config_values [CFG_DIASASL_IP] == NULL) {
		config_values [CFG_DIASASL_IP] = "::1";
	}
	if (config_values [CFG_DIASASL_PORT] == NULL) {
		log_error ("No default Quick-DiaSASL port, please configure diasasl_port=... (and maybe also diasasl_ip=...)");
		return PAM_NO_MODULE_DATA;
	}
	//
	// Setup for raw mode (mostly clearing ICANON)
	struct termios tty_orig, tty_mode;
	bool tty_changed = isatty (0) && (0 == tcgetattr (0, &tty_orig));
	if (tty_changed) {
		memcpy (&tty_mode, &tty_orig, sizeof (tty_mode));
		tty_mode.c_iflag &= ~( INLCR | ICRNL | INPCK | ISTRIP | IXON | IXANY | IUTF8 );
		tty_mode.c_iflag |=    IGNBRK | IGNPAR | IXOFF;
		tty_mode.c_oflag &= ~( OPOST | OLCUC | ONLCR | OCRNL | ONOCR | ONLRET | OFILL );
		tty_mode.c_oflag |=    0;
		tty_mode.c_lflag &= ~( ISIG | ICANON | ECHO | IEXTEN );
		tty_mode.c_lflag |=    0;
		tcsetattr (0, TCSADRAIN, &tty_mode);
	}
	//
	// Setup for Quick-DiaSASL
	struct diasasl_node dianode;
	memset (&dianode, 0, sizeof (dianode));
	diasasl_node_open (&dianode);
	//
	// Setup for mulTTY
	struct multtyflow mufl;
	multtyflow_init (&mufl, false);
	multty_t *stream_out = multty_stdout;
	multty_t *stream_in  = multty_stdin;
	struct multty_ext4sasl_pam multty_sasl = {
		.mty.next = NULL /* Filled later */,
		.mty.prev = NULL /* Filled later */,
		.mty.flow = &mufl,
		.mty.name = { 's', 'a', 's', 'l', 0 },
		.mty.description = NULL,
		.mty.description_fresh = false,
		.mty.text_cb = sasltext_cb,
		.mty.event_cb = saslevent_cb,
		.mty.parse_cb = multtysasl_parse_cb,
	};
	multtyprog_addstream (multtyprog_main, &multty_sasl.mty);
	multty_sasl.mty.flow = &mufl;
	//
	// Setup for SASL parsing
	sasltext_init (&multty_sasl);
	//
	// Connect a socket
	struct sockaddr_storage diass;
	if (!socket_parse (config_values [CFG_DIASASL_IP], config_values [CFG_DIASASL_PORT], &diass)) {
log_debug ("pam_saltty_diasasl: Failed to parse socket IP %s and/or port %s",
				config_values [CFG_DIASASL_IP], config_values [CFG_DIASASL_PORT]);
		pamrc = PAM_SERVICE_ERR;
		goto diafail;
	}
	if (!socket_client (&diass, SOCK_STREAM, &dianode.socket)) {
log_debug ("pam_saltty_diasasl: Failed to make client socket");
		pamrc = PAM_SERVICE_ERR;
		goto diafail;
	}
	//
	// Start the SASL session as a client
	struct diasasl_pam *sasl = NULL;
	if (!mem_open (sizeof (*sasl), (void **) &sasl)) {
log_debug ("pam_saltty_diasasl: Failed to create memory pool");
		pamrc = PAM_BUF_ERR;
		goto memsoxfail;
	}
	sasl->diasasl.callback = diasasl_cb;
	diasasl_open (&dianode, &sasl->diasasl, config_values [CFG_DOMAIN], NULL);
	if (diasasl_process (&dianode, true) != 0) {
log_debug ("pam_saltty_diasasl: Failed to process DiaSASL open request for domain %s", config_values [CFG_DOMAIN]);
		pamrc = PAM_SERVICE_ERR;
		goto memsoxfail;
	}
	if (mem_isnull (&sasl->mechlist) || mem_isempty (&sasl->mechlist)) {
log_debug ("pam_saltty_diasasl: Silly mechanism list from Quick-DiaSASL server, NULL or empty");
		pamrc = PAM_SERVICE_ERR;
		goto memsoxfail;
	}
	//
	// Setup channel binding information
	// This is new territory for ASCII connections!
	// Ideas: 1. Use the (EC)DH key used in SSH
	//        2. Derive a key from a TLS or SSH session key
	//        3. Use the inode of the terminal
	//TODO// sasl->diasasl.chanbind = ...
	//TODO// sasl->diasasl.chanbind_len = ...
	//
	// We should have received a mechanism list, and this
	// will be the initial SASL message to the client;
	// this carries a mechlist, which needs no <DLE> escaping
log_debug ("pam_saltty_diasasl: Prompting the user with the mechanism list \"%s\"", sasl->mechlist);
	//
	// Since pam_prompt() sends to stderr, we produce our own prommpt
	tty_sasl = true;
	printf (s_SOH "sasl" s_SO s_ENQ s_CAN "%s" s_US "%s" s_US "%s" s_ETB s_SO,
			config_values [CFG_HOSTNAME],
			config_values [CFG_SERVICE],
			sasl->mechlist);
	fflush (stdout);
	char rdbuf [4096];
	ssize_t rdlen = read (0, rdbuf, sizeof (rdbuf)-1);
log_debug ("read() returned %zd at %s:%d", rdlen, __FILE__, __LINE__);
	if (rdlen <= 0) {
		pamrc = PAM_CONV_ERR;
		goto memsoxfail;
	} else {
		rdbuf [rdlen] = '\0';
		pamrc = PAM_SUCCESS;
	}
	//
	// Enter a process/prompt loop to process client responses
	while ((!sasl->success) && (!sasl->failure)) {
		//
		// Reduce the response to just the mulTTY "sasl" stream
		int resplen = 0;
		uint8_t *respbin = rdbuf;
		char *resptxt = rdbuf;
		log_debug ("pam_saltty_diasasl: Response address is %p #%d, start", respbin, resplen);
		log_data ("pam_saltty_diasasl: Response in text", resptxt, strlen (resptxt), 0);
		while  (resptxt = strstr (resptxt, s_SOH "sasl"),
					resptxt != NULL) {
			//
			// Move beyond "<SOH>sasl<SI|SO>" or repeat if neither
			// but be open for "<SOH>sasl<US>...<SI|SO>" too
			resptxt += 5;
			if (*resptxt == c_US) {
				char c;
				do {
					c = * ++resptxt;
				} while (((c >= ' ') && (c != c_DEL)) ||
				         ((c >= 0x07) && (c <= 0x0d)) ||
				         (c == 0x1b));
			}
			if ((*resptxt != c_SO) && (*resptxt != c_SI)) {
				continue;
			}
			resptxt++;
			//
#ifdef PARSER_JOB_DONE_HERE
			// Remove any <DLE> escapes from the response token
#else
			// Move content to the response start, minding <DLE> escapes
#endif
			while ((*resptxt != '\0') && (*resptxt != c_SO) && (*resptxt != c_SI) && (*resptxt != c_SOH)) {
				if (*resptxt == c_DLE) {
#ifdef PARSER_JOB_DONE_HERE
					resptxt++;
					if (*resptxt == '\0') {
						pamrc = PAM_AUTH_ERR;
						goto memsoxfail;
					}
					*respbin++ = 0x40 ^ *resptxt++;
#else
					*respbin++ =        *resptxt++;
					resplen++;
					if (*resptxt == '\0') {
						pamrc = PAM_AUTH_ERR;
						goto memsoxfail;
					}
					*respbin++ = *resptxt++;
#endif
				} else {
					*respbin++ = *resptxt++;
				}
				resplen++;
			}
		}
		respbin -= resplen;
		log_debug ("pam_saltty_diasasl: Response address is %p #%d, done", respbin, resplen);
		log_data ("pam_saltty_diasasl: Response content", respbin, resplen, 0);
		//
		// Parse the mulTTY "sasl" stream as SASL c2s content
		multty_sasl.c2s_send = false;
		multty_sasl.c2s_tokenbuf = NULL;
		multty_sasl.c2s_tokenlen = 0;
		multtysasl_parse_cb (&multty_sasl.mty, MULTTY_CONTENT, respbin, resplen, true, true);
		//
		// Test for unexpected parser conditions
		if (multty_sasl.c2s_teardown) {
log_debug ("pam_saltty_diasasl: Client requested teardown of SASL authentication");
			multty_sasl.c2s_send = false;
			tty_sasl = false;
		}
		if (!multty_sasl.c2s_send) {
			sasl->failure = true;
			pamrc = PAM_SERVICE_ERR;
log_debug ("pam_saltty_diasasl: Client did not trigger token sending with its response token");
			break;
		}
		//
		// Relay the client response to the Quick-DiaSASL server
		log_debug ("About to backend-relay c2s token #%d that is NULL if %d", multty_sasl.c2s_tokenlen, multty_sasl.c2s_tokenbuf == NULL);
		diasasl_send (&dianode, &sasl->diasasl, multty_sasl.c2s_mechchoice, multty_sasl.c2s_tokenbuf, multty_sasl.c2s_tokenlen);
		log_debug ("finished backend-relay c2s token");
		//
		// Reset items meant for the first diasasl_send() only
		multty_sasl.c2s_mechchoice = NULL;
		sasl->diasasl.chanbind     = NULL;
		sasl->diasasl.chanbind_len = 0;
		//
		// Retrieve the Quick-DiaSASL server response
		if (diasasl_process (&dianode, true) != 0) {
log_debug ("pam_saltty_diasasl: Failure processing Quick-DiaSASL response message");
			pamrc = PAM_SERVICE_ERR;
			goto memsoxfail;
		}
		//
		// Do not send a new challenge when the last response was final
		if (sasl->success || sasl->failure) {
log_debug ("pam_saltty_diasasl: Whee, SASL is done; success=%d, failure=%d", sasl->success, sasl->failure);
			break;
		}
		//
		// Prompt the client for another round -- without or with token
log_debug ("pam_saltty_diasasl: Prompting the user with a challenge #%d which is NULL if %d", sasl->s2c_token.buflen, sasl->s2c_token.bufptr == NULL);
		if (mem_isnull (&sasl->s2c_token)) {
			//
			// Since pam_prompt() sends to stderr, we produce our own prommpt
			printf (s_SOH "sasl" s_SO s_ENQ s_ETB s_SO);
			fflush (stdout);
			rdlen = read (0, rdbuf, sizeof (rdbuf)-1);
log_debug ("read() returned %zd at %s:%d", rdlen, __FILE__, __LINE__);
			if (rdlen <= 0) {
				pamrc = PAM_CONV_ERR;
			} else {
				rdbuf [rdlen] = '\0';
				pamrc = PAM_SUCCESS;
			}
		} else {
			char send [2 * sasl->s2c_token.buflen];
			log_debug ("Token binary-escaping-with-DLE into buffer #%d", sizeof (send));
			int sendlen = bin_escape_DLE (send, &sasl->s2c_token);
			log_debug ("      binary-escaped to #%d", sendlen);
			//
			// Since pam_prompt() sends to stderr, we produce our own prommpt
			printf (s_SOH "sasl" s_SO s_ENQ s_US "%.*s" s_ETB s_SO,
				sendlen, send);
			fflush (stdout);
			rdlen = read (0, rdbuf, sizeof (rdbuf)-1);
log_debug ("read() returned %zd at %s:%d", rdlen, __FILE__, __LINE__);
			if (rdlen <= 0) {
				pamrc = PAM_CONV_ERR;
			} else {
				rdbuf [rdlen] = '\0';
				pamrc = PAM_SUCCESS;
			}
		}
		if (pamrc != PAM_SUCCESS) {
log_debug ("pam_saltty_diasasl: Continued prompting of user failed");
			goto memsoxfail;
		}
		//
		// Loop back to process the prompt response
	}
	//
	// Send the verdict, possible with additional data upon success
	if (sasl->failure) {
		log_debug ("Reporting failure with <ENQ><NAK><ETB> and teardown with <ENQ><CAN><ETB>");
		printf (s_SOH "sasl" s_SO s_ENQ s_NAK s_ETB s_ENQ s_CAN s_ETB s_SO);
	} else if (mem_isnull (&sasl->s2c_token)) {
		log_debug ("Reporting success with <ENQ><ACK><ETB>");
		printf (s_SOH "sasl" s_SO s_ENQ s_ACK s_ETB s_SO);
	} else {
		char send [2 * sasl->s2c_token.buflen];
		int sendlen = bin_escape_DLE (send, &sasl->s2c_token);
		log_debug ("Reporting success with <ENQ><ACK><US>...token...<ETB>");
		printf (s_SOH "sasl" s_SO s_ENQ s_ACK s_US "%.*s" s_ETB s_SO,
				sendlen, send);
	}
	tty_sasl = false;
	//
	// Setup the verdict to return to PAM
	pamrc = (sasl->success && !sasl->failure) ? PAM_SUCCESS : PAM_PERM_DENIED;
log_debug ("pam_saltty_diasasl: Concluding to report %d", pamrc);
	//
	// Clean up and return values
memsoxfail:
	if (sasl != NULL) {
		mem_close ((void **) &sasl);
	}
soxfail:
	close (dianode.socket);
diafail:
	diasasl_node_close (&dianode);
	sasltext_fini (&multty_sasl);
	multty_sasl.mty.flow = NULL;
	multtyprog_delstream (&multty_sasl.mty);
	multtyflow_fini (&mufl);
log_debug ("pam_saltty_diasasl: pam_saltty_diasasl returns %d", pamrc);
	//
	// Possibly send SASL teardown to the TTY
	if (tty_sasl) {
		printf (s_SOH "sasl" s_SO s_ENQ s_CAN s_ETB s_SO);
	}
	//
	// Return to the original terminal mode, presumably cooked
	if (tty_changed) {
		tcsetattr (0, TCSADRAIN, &tty_orig);
	}
	//
	// Make sure that the module leaves no output pending
	fflush (stdout);
	//
	// Return the collected PAM error/ok code
	return pamrc;
}


/* Handle credentials.  But since authentication is delegated, there is
 * really nothing to do.  But without this function, PAM errors arise.
 */
PAM_EXTERN int pam_sm_setcred (pam_handle_t *pamh, int flags, int argc, const char **argv) {
	return PAM_SUCCESS;
}
